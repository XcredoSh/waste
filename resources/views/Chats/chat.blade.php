@extends('layouts.base')

@section('content')
    <div class="container box_1170">
        <div class="section-top-border">

            <div class="row">


                @foreach($equipments as $equipment)
                    {{--    <h1>{{$equipment->id}}</h1>--}}
                    <div class="col-lg-12">
                        <blockquote class="generic-blockquote">
                            <div class="row">
                                <div class="col-lg-3">
                                    <i style="color: green;">{{$equipment->shop_name}}/ {{$equipment->as_app_type}}</i><br>
                                    <i><p class="date">{{date('d M y, h:i a', strtotime($equipment->created_at))}}</p></i><br><br>

                                </div>
                                <div class="col-lg-9">
                                    <a href="/chat" class="set_id_a" id="{{$equipment->id}}">
                                        <p style="margin-bottom: 0px;">Оборудование- {{$equipment->monufacture_model}}</p>
                                        <p>№1234 - <b>{{$equipment->message}}</b></p>
                                    </a>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                @endforeach
                @foreach($special_machineries as $special_machinerie)
                        <div class="col-lg-12">
                            <blockquote class="generic-blockquote">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <i style="color: green;">{{$special_machinerie->spec_shop_name}}/ {{$special_machinerie->spec_app_type}}</i><br>
                                        <i><p class="date">{{date('d M y, h:i a', strtotime($special_machinerie->created_at))}}</p></i><br><br>

                                    </div>
                                    <div class="col-lg-9">
                                        <a href="/chat" class="set_id_a" id="{{$special_machinerie->id}}">
                                            <p style="margin-bottom: 0px;">Оборудование- {{$special_machinerie->ts_group}}</p>
                                            <p>№1234 - <b>{{$special_machinerie->message}}</b></p>
                                        </a>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                @endforeach
            </div>
        </div>
    </div>

<br>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="user-wraper">

                    <ul class="users ul_marg">
                        @foreach($users as $user)
                          {{--  @foreach($equipments as $equipment)
                            @if($user->email == $equipment->email)--}}
                        <li class="user li_none" id="{{$user->id}}">
                            @if($user->unread)
                                     <span class="pending">{{$user->unread}}</span>
                            @endif
                            <div class="media">
                                <div class="media-left">
                                    <img src="https://via.placeholder.com/150" alt="" class="media-object">
                                </div>
                                <div class="media-body">
                                    <p class="name">{{$user->name}}</p>
                                    <p class="email">{{$user->email}}</p>
                                </div>
                            </div>
                        </li>
                           {{-- @endif
                          @endforeach--}}
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md-8" id="messages">

            </div>

        </div>
    </div>


<script type="text/javascript">

    var receiver_id = '';
    var my_id = "{{ Auth::id() }}";

    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('e4fc667daba51d5358ed', {
            cluster: 'ap2',
            forceTLS: true
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function(data) {
            /*alert(JSON.stringify(data));*/
            if (my_id == data.from){
                $('#' + data.to).click();
            } else if(my_id == data.to){
                if (receiver_id == data.from){
                    $('#' + data.from).click();
                }else{
                    var pending = parseInt( $('#' + data.from).find('.pending').html() );
                    if (pending){
                        $('#' + data.from).find('.pending').html(pending + 1)
                    }else{
                        $('#' + data.from).append('<span class="pending">1</span>');
                    }
                }
            }
        });


        $('.user').click( function () {
            $('.user').removeClass('active');
            $(this).addClass('active');
            $(this).find('.pending').remove();
            receiver_id = $(this).attr('id');
            $.ajax({
                type: "get",
                url: "message/" + receiver_id,
                date: "",
                cache: false,
                success: function (data) {
                    $('#messages').html(data);
                    scrollToBottomFunc();
                }
            })
        });


        $(document).on('keyup', '.input-text input', function (e) {

            var message = $(this).val();


            if (e.keyCode == 13 && message !='' && receiver_id != '')
            {
                /*alert(message);*/
                $(this).val('');

                var datastr = "receiver_id=" + receiver_id + "$message=" + message;

                $.ajax({
                    type: "post",
                    url: "message",
                    data: {
                        receiver_id : receiver_id,
                        message : message
                    },
                    cache: false,
                    success: function (data) {

                    },
                    error: function (jqXHR, status, err) {
                        console.log(err);
                        console.log(status);
                        console.log(jqXHR);
                    },
                    complete: function() {
                        scrollToBottomFunc();
                    }
                })
            }
        });
    });
    
    
    function scrollToBottomFunc() {
        $('.message-wrapper').animate({
            scrollTop: $('.message-wrapper').get(0).scrollHeight }, 50);
    }

</script>


@endsection

