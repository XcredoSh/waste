<!doctype html>
<html lang="{{app()->getLocale()}}">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>WASTEUZ</title>
    <link rel="icon" href="{{ asset('assets') }}/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/bootstrap.min.css">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
    <!-- animate CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/owl.carousel.min.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/all.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/flaticon.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/themify-icons.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/nice-select.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/style.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/input/style.css" media="screen" title="no title" charset="utf-8">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{ asset('assets') }}/js/jquery-1.12.1.min.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=<ваш API-ключ>" type="text/javascript"></script>

</head>
<style>
    .block_items:hover{
        background-color: green;
        color: #ffffff;
    }
</style>

<body>


<header class=" home_menu">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="{{route('indexOne')}}"> <img src="{{ asset('assets') }}/img/last.png" alt="logo" style="width: 200px"> </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigatio">
                        <span class="menu_icon"><i class="fas fa-bars"></i></span>
                    </button>

                    <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                        <ul class="navbar-nav">

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('news')}}">Новости</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('services')}}">Норматив {{trans('msg.services')}}</a>
                            </li>


                            <li class="nav-item">
                                <a class="nav-link" href="https://cleancity.uz/startpage;jsessionid=6863FE8714C8DC31FAE2F2E8F941EC21.thweb3">{{trans('msg.to_subscribers')}}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('contact')}}">{{trans('msg.contact')}}</a>
                            </li>


                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#signup">{{trans('msg.check_in')}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  href="#" data-toggle="modal" data-target="#signin">{{trans('msg.kirish')}}</a>
                            </li>


                        </ul>
                    </div>
                    {{-- <a id="search_1" href="javascript:void(0)"><i class="ti-search"></i></a>--}}
                </nav>
            </div>
        </div>
    </div>
    <div class="search_input" id="search_input_box">
        <div class="container ">
            <form class="d-flex justify-content-between search-inner">
                <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                <button type="submit" class="btn"></button>
                <span class="ti-close" id="close_search" title="Close Search"></span>
            </form>
        </div>
    </div>
</header>




<br><br><br><br>
<div class="container">
    <div class="row">
        <p style="float: left">МЕСТО ОСМОТРА OINT</p><br>
        <div id="map5" style="width: 1365px; height: 600px"></div>
    </div>

    <script>
        ymaps.ready(function () {
            var myMap = new ymaps.Map('map5', {
                    center: [41.311081, 69.240562],
                    zoom: 5
                }, {
                    searchControlProvider: 'yandex#search'
                }),


                // Создаём макет содержимого.
                MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                )


                @foreach($mapdatesPoints as $mapdate)
            var marker_name = new ymaps.Placemark([{{$mapdate->lan}}, {{$mapdate->lng}}], {
                    hintContent: '{{$mapdate->hintContent}}',
                    balloonContent: '{{$mapdate->balloonContent}}',

                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#imageWithContent',
                    // Своё изображение иконки метки.
                    iconImageHref: 'assets/img/icon/marker3.png',
                    // Размеры метки.
                    iconImageSize: [48, 48],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-24, -24],
                    // Смещение слоя с содержимым относительно слоя с картинкой.
                    iconContentOffset: [15, 15],
                    // Макет содержимого.
                    iconContentLayout: MyIconContentLayout
                });

            myMap.geoObjects
                .add(marker_name)

            @endforeach

        });

    </script>

</div>

<br><br><br><br><br>


<script src="{{ asset('assets') }}/js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="{{ asset('assets') }}/js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="{{ asset('assets') }}/js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
<script src="{{ asset('assets') }}/js/swiper.min.js"></script>
<!-- swiper js -->
<script src="{{ asset('assets') }}/js/masonry.pkgd.js"></script>
<!-- particles js -->
<script src="{{ asset('assets') }}/js/owl.carousel.min.js"></script>
<script src="{{ asset('assets') }}/js/jquery.nice-select.min.js"></script>
<!-- slick js -->
<script src="{{ asset('assets') }}/js/slick.min.js"></script>
<script src="{{ asset('assets') }}/js/jquery.counterup.min.js"></script>
<script src="{{ asset('assets') }}/js/waypoints.min.js"></script>
<script src="{{ asset('assets') }}/js/contact.js"></script>
<script src="{{ asset('assets') }}/js/jquery.ajaxchimp.min.js"></script>
<script src="{{ asset('assets') }}/js/jquery.form.js"></script>
<script src="{{ asset('assets') }}/js/jquery.validate.min.js"></script>
<script src="{{ asset('assets') }}/js/mail-script.js"></script>
<!-- custom js -->
<script src="{{ asset('assets') }}/js/custom.js"></script>
<script src="{{ asset('assets') }}/mapdata.js"></script>
<script src="{{ asset('assets') }}/countrymap.js"></script>


{{--<script src="https://api-maps.yandex.ru/2.1/?apikey=<ваш API-ключ>&lang=ru_RU" type="text/javascript">
</script>--}}



</body>

</html>
