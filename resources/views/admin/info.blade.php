<section id="">
    <div class="row">
        <div class="col-12 mt-3 mb-1">
            <div class="content-header">
                Добавить информация</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="form-actions">
                        <button type="button" id="tx" onclick="tx()" class="btn btn-raised btn-primary">
                            <i class="ft-folder"></i>
                            Техт
                        </button>
                        <button type="button" id="pd" onclick="pd()"  class="btn btn-raised btn-primary">
                            <i class="ft-folder"></i>
                            PDF
                        </button>
                        <button type="button" id="lk" onclick="lk()"  class="btn btn-raised btn-primary">
                            <i class="ft-folder"></i>
                            Линк
                        </button>
                        <button type="button" id="ph" onclick="ph()" class="btn btn-raised btn-primary">
                            <i class="ft-folder"></i>
                            Текст изображения
                        </button>
                    </div>
                </div>

                <div class="card-body" id="for_tx" style="height: 1050px;">
                    <div class="card-block">
                        {!! Form::open(['url' => 'informations', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="px-3" style="padding-top: 10px;">
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput6">Interested in</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select id="projectinput6" name="about_us_id" class="form-control">

                                                                <option value="0" selected disabled>Меню</option>
                                                                <option value="1"  >О нас</option>
                                                                <option value="2" >Открытые данные</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select id="projectinput6" name="category_id" class="form-control">
                                                                <option value="0" selected disabled>Категория</option>
                                                                @foreach($infos as $item)
                                                                    <option value="{{$item->id}}"  >{{$item->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">About Project</label>
                                                    <div class="col-md-9">
                                                        <textarea id="projectinput9" rows="15" class="form-control" name="text" placeholder="Text UZ" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">About Project</label>
                                                    <div class="col-md-9">
                                                        <textarea id="projectinput9" rows="15" class="form-control" name="text_ru" placeholder="Text RU" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-raised btn-primary">
                                                        <i class="ft-check"></i>
                                                        Сохранить
                                                    </button>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>



                        </div>


                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="card-body" id="for_pd" style="height: 550px;">
                    <div class="card-block">
                        {!! Form::open(['url' => 'informations', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="px-3" style="padding-top: 10px;">
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput6">Interested in</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select id="projectinput6" name="about_us_id" class="form-control">

                                                                <option value="0" selected disabled>Меню</option>
                                                                <option value="1"  >О нас</option>
                                                                <option value="2" >Открытые данные</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select id="projectinput6" name="category_id" class="form-control">
                                                                <option value="0" selected disabled>Категория</option>
                                                                @foreach($infos as $item)
                                                                    <option value="{{$item->id}}"  >{{$item->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">PDF</label>
                                                    <div class="col-md-9" >
                                                        <input type="text"  id="projectinput1" class="form-control" placeholder="PDF Name UZ" name="pdf_name_uz" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">PDF</label>
                                                    <div class="col-md-9" >
                                                        <input type="text" id="projectinput1" class="form-control" placeholder="PDF Name ru" name="pdf_name_ru" required>
                                                    </div>
                                                </div>



                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">PDF</label>
                                                    <div class="col-md-9">
                                                        <label id="projectinput8" class="file center-block">
                                                            <input type="file" id="file" name="pdf">
                                                            <span class="file-custom"></span>
                                                        </label>
                                                    </div>
                                                </div>


                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-raised btn-primary">
                                                        <i class="ft-check"></i>
                                                        Сохранить
                                                    </button>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>



                        </div>


                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="card-body" id="for_lk" style="height: 550px;">
                    <div class="card-block">
                        {!! Form::open(['url' => 'informations', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="px-3" style="padding-top: 10px;">
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput6">Interested in</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select id="projectinput6" name="about_us_id" class="form-control">

                                                                <option value="0" selected disabled>Меню</option>
                                                                <option value="1"  >О нас</option>
                                                                <option value="2" >Открытые данные</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select id="projectinput6" name="category_id" class="form-control">
                                                                <option value="0" selected disabled>Категория</option>
                                                                @foreach($infos as $item)
                                                                    <option value="{{$item->id}}"  >{{$item->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">LINK</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="projectinput1" class="form-control" placeholder="Name UZ" name="link_name_uz" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">LINK</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="projectinput1" class="form-control" placeholder="Name RU" name="link_name_ru" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">LINK</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="projectinput1" class="form-control" placeholder="http://" name="link" required>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-raised btn-primary">
                                                        <i class="ft-check"></i>
                                                        Сохранить
                                                    </button>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>



                        </div>


                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="card-body" id="for_ph" style="height: 550px;">
                    <div class="card-block">
                        {!! Form::open(['url' => 'informations', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="px-3" style="padding-top: 10px;">
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput6">Interested in</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select id="projectinput6" name="about_us_id" class="form-control">

                                                                <option value="0" selected disabled>Меню</option>
                                                                <option value="1"  >О нас</option>
                                                                <option value="2" >Открытые данные</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select id="projectinput6" name="category_id" class="form-control">
                                                                <option value="0" selected disabled>Категория</option>
                                                                @foreach($infos as $item)
                                                                    <option value="{{$item->id}}"  >{{$item->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row last">
                                                    <label class="col-md-3 label-control" for="projectinput9">PHOTO</label>
                                                    <div class="col-md-9">
                                                        <label id="projectinput8" class="file center-block">
                                                            <input type="file" id="file" name="photo" required>
                                                            <span class="file-custom"></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group row last">
                                                            <label class="col-md-3 label-control" for="projectinput9">PHOTO TEXT UZ</label>
                                                            <div class="col-md-9">
                                                                <textarea id="projectinput9" rows="5" class="form-control" name="text_photo" placeholder="Text" required></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group row last">
                                                            <label class="col-md-3 label-control" for="projectinput9">PHOTO TEXT RU</label>
                                                            <div class="col-md-9">
                                                                <textarea id="projectinput9" rows="5" class="form-control" name="text_photo_ru" placeholder="Text" required></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-raised btn-primary">
                                                        <i class="ft-check"></i>
                                                        Сохранить
                                                    </button>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>






