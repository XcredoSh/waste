
<section id="">
    <div class="row">
        <div class="col-12 mt-3 mb-1">
            <div class="content-header">
                Добавление изображений</div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">

                </div>
                <div class="card-body">
                    <div class="card-block">
                        <form action="{{route('addImgsPost')}}" method="POST" class="form">
                            {{csrf_field()}}
                            <div class="form-body">

                                <div class="row">
                                    <div class=" col-md-6">
                                        <div class="form-group">
                                            <label for="companyName" class="sr-only"></label>
                                            <input type="text" name="img_title"  class="form-control" placeholder="Название изображения" required>
                                        </div>

                                       {{-- <div class="form-group  ">
                                            <label for="companyName" class="sr-only">Area Name RU</label>
                                            <input type="text" name="area_name_ru"  class="form-control" placeholder="Area Name RU">
                                        </div>--}}
                                    </div>

                                    <div class="form-group  col-md-6">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="sr-only"></label>

                                                <input type="file" name="in_img" id="in_img" class="form-control" placeholder="Img" required>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                            </div>

                            <div class="form-actions">

                                <button type="submit" class="btn btn-raised btn-primary">
                                    <i class="ft-check"></i> Сохранить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Удаленные изображения</h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
                        <form action="{{route('addImgsPost')}}" method="POST" class="form">
                            {{csrf_field()}}
                            <div class="form-body">

                                <div class="row">
                                    <section id="shopping-cart">

                                            <div class="col-sm-12">
                                                <div class="card col-md-12">
                                                    <div class="card-header">

                                                    </div>
                                                    <div class="card-body">
                                                        <div class="card-block col-md-12">
                                                            <table class="table table-responsive-md text-center ">
                                                                <thead>
                                                                <tr>
                                                                    <th>ID</th>
                                                                    <th>Img</th>
                                                                    <th>Title</th>
                                                                    <th></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @forelse($imgs as $key => $img)
                                                                    <tr>
                                                                        <td>{{$key}}</td>
                                                                        <td><img class="media-object round-media" src="{{ asset('assets') }}{{$img->in_img}}" alt="Generic placeholder image" style="height: 75px;" /></td>
                                                                        <td>{{$img->img_title}}</td>
                                                                        <td>


                                                                                <a class="danger" href="/dashboard/img/{{$img->id}}" data-item-id="{{ $img->id }}" >
                                                                                    <i class="ft-trash font-medium-3"></i>
                                                                                </a>

                                                                        </td>
                                                                    </tr>
                                                                @endforeach

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                    </section>
                                </div>



                            </div>

                            <div class="form-actions">

                                <button type="submit" class="btn btn-raised btn-primary">
                                    <i class="ft-check"></i> Удалять
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




</section>


