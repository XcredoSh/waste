<section id="">
    <div class="row">
        <div class="col-12 mt-3 mb-1">
            <div class="content-header">
                Добавить информация</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body">
                    <div class="card-block">
                        {!! Form::open(['url' => 'information', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="row">
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <input type="text" name="text_uz"  class="form-control" placeholder="Добавить информация uz" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="sr-only"></label>
                                        <input type="text" name="text_ru"  class="form-control" placeholder="Добавить информация ru" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="companyName" class="sr-only"></label>
                                        <input type="file" name="information"  class="form-control" placeholder="Добавить информация" required>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-raised btn-primary">
                                <i class="ft-check"></i>
                                Сохранить
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>


<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">

                <div class="card-body">
                    <div class="card-block">

                        <table class="table table-responsive-md-md text-center table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>№</th>

                                <th>Хужжат номи</th>
                                <th>Date</th>
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($informations as $information)

                                    <tr>
                                        <td>{{$information->id}}</td>

                                        <td>
                                            {{$information->text_ru}}
                                        </td>

                                        <td>
                                            {{$information->created_at}}
                                        </td>

                                        <td>
                                            <a href="/information/deleated/{{$information->id}}">
                                                <i class="ft-trash font-medium-3"></i>
                                            </a>
                                        </td>

                                    </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


