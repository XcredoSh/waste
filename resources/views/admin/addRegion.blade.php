
<section id="">
    <div class="row">
        <div class="col-12 mt-3 mb-1">
            <div class="content-header">
                Добавление региона</div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">

                </div>
                <div class="card-body">
                    <div class="card-block">
                        <form action="{{route('addRegionPost')}}" method="POST" class="form">
                            {{csrf_field()}}
                            <div class="form-body">

                                <div class="row">
                                    <div class="form-group  col-md-6">
                                        <label for="companyName" class="sr-only">Region Name UZ</label>
                                        <input type="text" name="region_name_uz" id="name" class="form-control" placeholder="Region Name UZ" required>
                                    </div>
                                    <div class="form-group  col-md-6">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="sr-only">Area ID</label>
                                                <select name="areas_id" class="form-control">
                                                        <option value="1">Тошкент</option>
                                                        <option value="2">Тошкент вилояти</option>
                                                        <option value="3">Андижон вилояти</option>
                                                        <option value="4">Намнган  вилояти</option>
                                                        <option value="5">Қўқон  вилояти</option>
                                                        <option value="6">Жиззах вилояти</option>
                                                        <option value="7">Бухоро вилояти</option>
                                                        <option value="8">Самарқанд вилояти</option>
                                                        <option value="9">Навоий вилояти</option>
                                                        <option value="10">Стрдарё вилояти</option>
                                                        <option value="11">Қашқадарё вилояти</option>
                                                        <option value="12">Сурхондарё вилояти</option>
                                                        <option value="13">Қорақолпоқистон вилояти</option>
                                                </select>
                                               {{-- <input type="number" name="number" id="" class="form-control" placeholder="Area ID">--}}
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group  ">
                                            <label for="companyName" class="sr-only">Region Name RU</label>
                                            <input type="text" name="region_name_ru" id="name" class="form-control" placeholder="Region Name RU" required>
                                        </div>
                                    </div>
                                </div>





                            </div>

                            <div class="form-actions">

                                <button type="submit" class="btn btn-raised btn-primary">
                                    <i class="ft-check"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>

