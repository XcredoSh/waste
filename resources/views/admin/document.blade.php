

<section id="">
    <div class="row">
        <div class="col-12 mt-3 mb-1">
            <div class="content-header">
                Добавить документ</div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">ИНФОРМАЦИЯ О ПРОЕКТЕ</h4>
                </div>
                <div class="card-body">
                    <div class="card-block">


                        {!! Form::open(['url' => 'document_save', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}

                            @csrf
                            <div class="form-body">

                               <div class="row">
                                   <div class="form-group  col-md-6">
                                       <label for="companyName" class="sr-only">Document Name</label>
                                       <input type="text" name="name" id="name" class="form-control" placeholder="Document Name" required>
                                   </div>
                                   <div class="form-group  col-md-6">
                                       <div class="row">
                                           <div class="form-group col-md-6">
                                               <input type="number" name="number" id="" class="form-control" placeholder="Registrated Number" required>
                                           </div>
                                           <div class="form-group col-md-6">
                                               <input type="date" name="date" id="date" class="form-control" placeholder="Registrated Date" required>
                                           </div>
                                       </div>
                                   </div>
                               </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput5" class="sr-only">Document Type</label>
                                            <select id="projectinput5" name="document_types_id" class="form-control">
                                                @foreach($document_types as $doc)
                                                    <option value="{{$doc->id}}">{{$doc->name_ru}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group pb-1">
                                    <label for="switchery1" class="font-medium-2 text-bold-600 ml-1">Adding Link</label>
                                    <input type="checkbox" name="is_locale" onclick="check()" id="myCheck" class="js-switch" required />
                                </div>

                                <div class="row" id="rowCheck">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="companyName" class="sr-only">Document Link</label>
                                            <input type="text" name="url" id="companyName" class="form-control" placeholder="Document Link" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="inputCheck">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="companyName" class="sr-only">Document file</label>
                                            <input type="file" id="file" class="form-control" placeholder="Document file" name="file" required>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions">

                                <button type="submit" class="btn btn-raised btn-primary">
                                    <i class="ft-check"></i> Сохранить
                                </button>
                            </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

