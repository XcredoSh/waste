
    <?php
    $toshkent = Session::get('toshkent');
    $toshkentVil = Session::get('toshkentVil');
    $andijonVil = Session::get('andijonVil');
    $namanganVil = Session::get('namanganVil');
    $qoqonVil = Session::get('qoqonVil');
    $jizzaxVil = Session::get('jizzaxVil');
    $buxoroVil = Session::get('buxoroVil');
    $samarqandVil = Session::get('samarqandVil');
    $navoiyVil = Session::get('navoiyVil');
    $sirdaryoVil = Session::get('sirdaryoVil');
    $qashqadaryoVil = Session::get('qashqadaryoVil');
    $surhondaryoVil = Session::get('surhondaryoVil');
    $qoraqolpoqistonVil = Session::get('qoraqolpoqistonVil');
    $newser = Session::get('newser');
    ?>
    @if($toshkent)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_1)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_1)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($toshkentVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_2)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_2)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($andijonVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_3)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_3)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($namanganVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_4)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_4)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($qoqonVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_5)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_5)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($jizzaxVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_6)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_6)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($buxoroVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_7)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_7)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($samarqandVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_8)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_8)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($navoiyVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_9)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_9)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($sirdaryoVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_10)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_10)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($qashqadaryoVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_11)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_11)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($surhondaryoVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_12)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_12)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($qoraqolpoqistonVil)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($app_inspector_show_13)
                                        <h3 class="mb-1 warning">{{count($app_inspector_show_13)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($newser)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">

                                    <span>News</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif(0<1)
        <div class="row" matchHeight="card">
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">

                                    @if($applicate)
                                        <h3 class="mb-1 danger">{{count($applicate)}}</h3>
                                    @endif
                                    <span>New Apps</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-rocket danger font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($users)
                                        <h3 class="mb-1 success">{{count($users)}}</h3>
                                    @endif
                                    <span>New Clients</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-user success font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="px-3 py-3">
                            <div class="media">
                                <div class="media-body text-left">
                                    @if($docx)
                                        <h3 class="mb-1 warning">{{count($docx)}}</h3>
                                    @endif
                                    <span>Documents</span>
                                </div>
                                <div class="media-right align-self-center">
                                    <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @endif




















