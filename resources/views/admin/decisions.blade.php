<div class="content-wrapper"><!--Extended Table starts-->
    <div class="row">
        <div class="col-12">
            <div class="content-header col-md-6">
                <h5 style="margin-left: 70px; position: absolute; margin-top: 10px">
                    Решения</h5>
                <a href="" class="btn btn-social-icon mr-2 mb-2 btn-facebook">
                    <span class="fa fa-home"></span>
                </a>
            </div>

        </div>
    </div>

    <section id="extended">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="form-group" style="padding-left: 25px; padding-top: 25px;">

                    </div>

                    <div class="card-body">
                        <div class="card-block">

                            <table class="table table-responsive-md-md text-center table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>№</th>

                                    <th>Хужжат номи</th>
                                    <th>Рўйхатга олинган тартиб рақфми</th>
                                    <th>Рўйхатга олинган санаси</th>
                                    <th>Линк</th>
                                    <th>Delete</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($documentВecisions as $documentВecision)
                                    @if($documentВecision->document_types_id == 3)
                                        <tr>
                                            <td>{{$documentВecision->id}}</td>

                                            <td>
                                                {{$documentВecision->name}}
                                            </td>
                                            <td>{{$documentВecision->number}}</td>
                                            <td>{{$documentВecision->date}}</td>
                                            <td>{{$documentВecision->url}}</td>
                                            <td>
                                                <a href="/dashboard/codes/delete/{{$documentВecision->id}}">
                                                    <i class="ft-trash font-medium-3"></i>
                                                </a>
                                            </td>

                                        </tr>

                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>




<script  type="text/javascript">
    var deleted = document.getElementById("delete-area");
    var btnBtnDelete = document.getElementById("myBtnDelete");
    //var span = document.getElementsByClassName("close")[0];

    btnBtnDelete.onclick = function() {
        deleted.style.display = "block";
    }
    btnBtnDelete.onclick = function() {
        deleted.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == deleted) {
            deleted.style.display = "none";
        }
    }
</script>
<script  type="text/javascript">
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];

    btn.onclick = function() {
        modal.style.display = "block";
    }
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>






