<!-- Start Align Area -->
<div class="whole-wrap">
    <div class="container box_1170">



        <div class="section-top-border">
            <h3 class="mb-30"></h3>
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <table class="table table-bordered mb-0">
                            <tbody>

                            @foreach($resident_details_ids as $resident_details_id)
                            <tr>
                                <td><strong>ID</strong> {{$resident_details_id->id}}</td>
                            </tr>
                            <tr>
                                @foreach($applications as $app)
                                    @if($app->id == $resident_details_id->region_id)
                                        @foreach($regions_id as $regions_ids)
                                            @if($app->region_id == $regions_ids->id)
                                                <td><strong>Region</strong>
                                                    {{$regions_ids->region_name_uz}}
                                                </td>

                                            @endif
                                        @endforeach

                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <td><strong>Area</strong> {{$resident_details_id->area}}</td>
                            </tr>
                            <tr>
                                <td><strong>Name</strong> {{$resident_details_id->first_name}} {{$resident_details_id->last_name}}</td>
                            </tr>
                            <tr>
                                <td><strong>Phone</strong> {{$resident_details_id->phone}}</td>
                            </tr>
                            <tr>
                                @foreach($applications as $app)
                                   {{-- @dd($app->id)--}}
                                    @if($app->id == $resident_details_id->id)
                                               <td><strong>Email</strong> {{$resident_details_id->email}}</td>
                                    @endif
                                @endforeach

                            </tr>
                            <tr>
                                <td><strong>Passport</strong> {{$resident_details_id->passport_number}}</td>
                            </tr>
                            <tr>
                                <td><strong>Appealed Date</strong> {{$resident_details_id->date}}</td>
                            </tr>
                            </tbody>
@endforeach
                        </table>
                    </div>

                </div>
                <div class="col-md-8 mt-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Media</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner" role="listbox">
                                    @foreach($resident_details_ids as $resident_details_id)
                                        @foreach($applications as $app)
                                        @if($app->id == $resident_details_id->id)
                                             <div class="carousel-item active">
                                                 <img src="../../{{$app->media}}" alt="First slide">
                                             </div>
                                         @endif
                                        @endforeach
                                    @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carousel-example-generic" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-example-generic" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <section id="blockquotes-with-avatar">
            <div class="row match-height">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Письмо-обращение </h4>
                        @foreach($resident_details_ids as $resident_details_id)
                            <p>{{$resident_details_id->message}}</p>
                        @endforeach

                    </div>
                </div>
            </div>
        </section>

    </div>

</div>

<div class="form-group  col-md-12">
    <div class="row">
        <div class="form-group col-md-6">
            <button type="button" id="answerId" onclick="myAnswer()" class="btn btn-raised btn-success col-md-12">
Ответить <i class="icon-share-alt"></i></button>
        </div>
        @foreach($resident_details_ids as $resident_details_id)
            <div class="form-group col-md-6">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <form action="{{route('newActionThis', $resident_details_id->id)}}" method="POST">
                                @csrf
                                @method('PATCH')
                                <input type = "hidden" name = "app_status_id" value = "2" >

                                <button type="input" id="answerDen" class="btn btn-raised btn-danger col-md-12">В процессе <i class="icon-settings"></i></button>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form action="{{route('newActionThis', $resident_details_id->id)}}" method="POST">
                                @method('PATCH')
                                @csrf
                                <input type = "hidden" name = "app_status_id" value = "3">

                                <button type="input" id="answerYes" class="btn btn-raised btn-success col-md-12">Сделано <i class="icon-check"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>










<section id="sizing">
    <div class="row">
        <div class="col-12 mt-3 mb-1">
            <div class="content-header">
                Добавить документ</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Adding date</h4>
                </div>
                <div class="card-body">
                    <div class="card-block">


                        {!! Form::open(['url' => '/doshboard/resident_details', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                        {{--<form action="{{route('answerName')}}" method="POST" class="form">--}}
                            {{csrf_field()}}
                            <div class="form-body">
                                <h4 class="form-section mt-3"><i class="ft-check-circle"></i> Answers</h4>
                                <div class="row">
                                   {{-- <div class="form-group  col-md-6">
                                        <label for="companyName" class="sr-only">Document Name</label>
                                        <input type="n" name="name" id="name" class="form-control" placeholder="Document Name">
                                    </div>--}}
                                    <div class="form-group  col-md-6">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <input type="number" name="userID" id="" class="form-control" placeholder="Adding ID">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="number" name="answerID" id="date" class="form-control" placeholder="Adding answer ID">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <textarea name="toUserMessage" class="form-control" id="placeTextarea" rows="3" placeholder="Textarea for answer" style="margin-top: 0px; margin-bottom: 0px; height: 100px;"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="file" name="toUserFile" id="date" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group">
                                <button type="submit" class="btn btn-raised btn-primary col-md-12">
                                    SEND <i class="icon-paper-plane"></i>
                                </button>
                                </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
