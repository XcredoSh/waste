<section id="">
    <div class="row">
        <div class="col-12 mt-3 mb-1">
            <div class="content-header">
                Добавить статьи</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body">
                    <div class="card-block">
                        {!! Form::open(['url' => 'admin-articles', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="row">
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <label for="companyName" class="sr-only">Название</label>
                                        <textarea type="text" name="art_name_uz"  class="form-control" placeholder="Название UZ" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="companyName" class="sr-only">Название</label>
                                        <textarea type="text" name="art_name_ru"  class="form-control" placeholder="Название RU" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="companyName" class="sr-only"></label>
                                        <input type="file" name="art_image"  class="form-control" placeholder="" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="companyName" class="sr-only"></label>
                                        <input type="date" name="created"  class="form-control" placeholder="" required>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-raised btn-primary">
                                <i class="ft-check"></i>
                                Сохранить
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>


<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">

                <div class="card-body">
                    <div class="card-block">

                        <table class="table table-responsive-md-md text-center table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>№</th>

                                <th>Articles name</th>
                                <th>Images</th>
                                <th>Date</th>
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($articels as $item)

                                <tr>
                                    <td>{{$item->id}}</td>

                                    <td>
                                        {{$item->art_name_ru}}
                                    </td>
                                    <td>
                                        <img src=" {{$item->art_image	}}" alt="" width="60">

                                    </td>

                                    <td>
                                        {{$item->created}}
                                    </td>

                                    <td>
                                        <a href="/admin-articles/delete/{{$item->id}}">
                                            <i class="ft-trash font-medium-3"></i>
                                        </a>
                                    </td>

                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>



