<div class="content-wrapper"><!--Extended Table starts-->
    <div class="row">
        <div class="col-12">
            <div class="content-header col-md-6">
                <h5 style="margin-left: 70px; position: absolute; margin-top: 10px">
                    </h5>
                <a href="" class="btn btn-social-icon mr-2 mb-2 btn-facebook">
                    <span class="fa fa-home"></span>
                </a>
            </div>

        </div>
    </div>

    <section id="extended">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="form-group" style="padding-left: 25px; padding-top: 25px;">



                    <div class="card-body">
                        <div class="card-block">

                            <table class="table table-responsive-md-md text-center table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>№</th>

                                    <th>Хужжат номи</th>
                                    <th>Рўйхатга олинган тартиб рақфми</th>
                                    <th>Рўйхатга олинган санаси</th>
                                    <th>Линк</th>
                                    <th>Delete</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($documentShows as $documentShow)
                                    @if($documentShow->document_types_id == 1)
                                        <tr>
                                      <td>{{$documentShow->id}}</td>

                                      <td>
                                          {{$documentShow->name}}
                                      </td>
                                      <td>{{$documentShow->number}}</td>
                                      <td>{{$documentShow->date}}</td>
                                      <td>{{$documentShow->url}}</td>
                                      <td>
                                          <a href="/dashboard/codes/delete/{{$documentShow->id}}">
                                              <i class="ft-trash font-medium-3"></i>
                                          </a>
                                      </td>

                                  </tr>

                                    @endif

                                @endforeach


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
