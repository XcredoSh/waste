
<?php
$admin = Session::get('admin');
?>
@if($admin)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 1)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Люди приложения</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Название</th>
                                            <th>Фамилия</th>
                                            <th>Телефон</th>
                                            <th>Номер паспорта</th>
                                            <th>Название региона</th>
                                            <th>Площадь</th>
                                            <th>Сообщение</th>
                                            <th>Дата</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)

                                            <tr>
                                                <td>{{$app->id}}</td>
                                                <td>{{$app->first_name}}</td>
                                                <td>{{$app->last_name}}</td>
                                                <td>{{$app->phone}}</td>
                                                <td>{{$app->passport_number}}</td>
                                                <td>
                                                @if($app->region_id == 1)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 2)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 3)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 4)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 5)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 6)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 7)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 8)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 9)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 10)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 11)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                    @endforeach
                                                @elseif($app->region_id == 12)
                                                    @foreach($regions_id as $regions_ids)
                                                        @if($app->region_id == $regions_ids->id)
                                                            <td>{{$regions_ids->region_name_uz}}</td>
                                                        @endif
                                                     @endforeach
                                                @endif

                                                            <td>{{$app->message}}</td>
                                                            <td>{{$app->date}}</td>

                                                            <td></td>


                                            </tr>



                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif








<?php
$toshkent = Session::get('toshkent');
?>
@if($toshkent)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 1)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>ID Application</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 1)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>


                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>

                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif



<?php
$toshkentVil = Session::get('toshkentVil');
?>
@if($toshkentVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 2)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 2)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>


                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

<?php
$andijonVil = Session::get('andijonVil');
?>
@if($andijonVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 3)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 3)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>


                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

<?php
$namanganVil = Session::get('namanganVil');
?>
@if($namanganVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 4)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 4)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>


                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

<?php
$qoqonVil= Session::get('qoqonVil');
?>
@if($qoqonVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 5)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 5)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>


                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

<?php
$jizzaxVil = Session::get('jizzaxVil');
?>
@if($jizzaxVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 6)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 6)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>


                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

<?php
$buxoroVil = Session::get('buxoroVil');
?>
@if($buxoroVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 7)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 7)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>

                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

<?php
$samarqandVil = Session::get('samarqandVil');
?>
@if($samarqandVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 8)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 8)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>


                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

<?php
$navoiyVil = Session::get('navoiyVil');
?>
@if($navoiyVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 9)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 9)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>



                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif


<?php
$sirdaryoVil = Session::get('sirdaryoVil');
?>
@if($sirdaryoVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 9)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 9)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>


                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

<?php
$qashqadaryoVil = Session::get('qashqadaryoVil');
?>
@if($qashqadaryoVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 11)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 11)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>

                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

<?php
$surhondaryoVil = Session::get('surhondaryoVil');
?>
@if($surhondaryoVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 12)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 12)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>


                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif

<?php
$qoraqolpoqistonVil = Session::get('qoraqolpoqistonVil');
?>
@if($qoraqolpoqistonVil)
    @foreach($inspectors as $inspector)
        @if($inspector->region_inspector_id == 13)
            <section id="horizontal">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">People applications</h4>
                            </div>
                            <div class="card-body collapse show">
                                <div class="card-block card-dashboard">
                                    <p class="card-text">www</p>
                                    <table class="table display nowrap table-striped table-bordered scroll-horizontal">
                                        <thead>
                                        <tr>
                                            <th>ID</th>

                                            <th>Name</th>
                                            <th>Last Name</th>
                                            <th>Phone</th>
                                            <th>Passport Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                            <th>eye</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($applications as $app)
                                            @if($app->region_id == 13)
                                                <tr>
                                                    <td>{{$app->id}}</td>
                                                    {{-- <td>
                                                         <div class="custom-control custom-checkbox m-0">
                                                             <input type="checkbox" class="custom-control-input" id="item1">
                                                             <label class="custom-control-label" for="item1"></label>
                                                         </div>
                                                     </td>--}}
                                                    <td>{{$app->first_name}}</td>
                                                    <td>{{$app->last_name}}</td>
                                                    <td>{{$app->phone}}</td>
                                                    <td>{{$app->passport_number}}</td>


                                                    <td>{{$app->date}}</td>

                                                    <td>
                                                        @if($app->app_status_id == 1)
                                                            <a class="btn white btn-round btn-danger">New</a>
                                                        @elseif($app->app_status_id == 2)
                                                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">Прият к исполнению <i class="far fa-clock"></i></button>
                                                            {{--<a class="btn white btn-round btn-yellow">New</a>--}}
                                                        @elseif($app->app_status_id == 3)
                                                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">Исполненю <i class="icon-check"></i></button>
                                                        @endif

                                                    </td>
                                                    <form action="{{route('show_by_population')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <td>
                                                            <input type="text" value="{{$app->id }}"  name="region_id" class="btn btn-raised btn-secondary "></input>
                                                        </td>
                                                        <td>
                                                            <button type="submit"  class="btn btn-raised btn-info">More <i class="icon-arrow-right"></i></button>
                                                        </td>
                                                    </form>
                                                </tr>

                                            @endif

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif









