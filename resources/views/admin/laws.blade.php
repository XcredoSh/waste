<div class="content-wrapper"><!--Extended Table starts-->
    <div class="row">
        <div class="col-12">
            <div class="content-header col-md-6">
                <h5 style="margin-left: 70px; position: absolute; margin-top: 10px">
                    Законы</h5>
                <a href="" class="btn btn-social-icon mr-2 mb-2 btn-facebook">
                    <span class="fa fa-home"></span>
                </a>
            </div>

        </div>
    </div>

    <section id="extended">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="form-group" style="padding-left: 25px; padding-top: 25px;">


                    </div>


                    <div class="card-body">
                        <div class="card-block">

                            <table class="table table-responsive-md-md text-center table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>№</th>

                                    <th>Хужжат номи</th>
                                    <th>Рўйхатга олинган тартиб рақфми</th>
                                    <th>Рўйхатга олинган санаси</th>
                                    <th>Линк</th>
                                    <th>Delete</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($documentLaws as $documentLaw)
                                    @if($documentLaw->document_types_id == 2)
                                        <tr>
                                            <td>{{$documentLaw->id}}</td>

                                            <td>
                                                {{$documentLaw->name}}
                                            </td>
                                            <td>{{$documentLaw->number}}</td>
                                            <td>{{$documentLaw->date}}</td>
                                            <td>{{$documentLaw->url}}</td>
                                            <td>
                                                <a href="/dashboard/codes/delete/{{$documentLaw->id}}">
                                                    <i class="ft-trash font-medium-3"></i>
                                                </a>
                                            </td>

                                        </tr>

                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script  type="text/javascript">
    var deleted = document.getElementById("delete-area");
    var btnBtnDelete = document.getElementById("myBtnDelete");
    //var span = document.getElementsByClassName("close")[0];

    btnBtnDelete.onclick = function() {
        deleted.style.display = "block";
    }
    btnBtnDelete.onclick = function() {
        deleted.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == deleted) {
            deleted.style.display = "none";
        }
    }
</script>
<script  type="text/javascript">
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];

    btn.onclick = function() {
        modal.style.display = "block";
    }
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>

{{--Delete modal end--}}
<script type="text/javascript">
    $(document).ready(function () {
        $('#delete-area').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('item-id');

            $('#deleteArea').attr("action", "{{ url('/brands') }}" + "/" + id);
        })

        $(document).on('click', "#edit-item", function () {
            $(this).addClass('edit-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

            var options = {
                'backdrop': 'static'
            };
            $('#edit-area').modal(options)
        })

        // on modal show
        $('#edit-area').on('show.bs.modal', function (event) {
            // var el = $(".edit-item-trigger-clicked"); // See how its usefull right here?
            var el = $(event.relatedTarget); // See how its usefull right here?
            var row = el.closest(".data-row");

            // get the data
            var id = el.data('item-id');
            var name = row.children(".name").text();
            // var description = row.children(".description").text();

            // fill the data in the input fields
            $("#modal-id").val(id);
            $("#modal-name").val(name);

            $('#edit-form').attr("action", "{{ url('/brands') }}" + "/" + id + "/edit");

        })

        // on modal hide
        $('#edit-brand').on('hide.bs.modal', function () {
            $('.edit-item-trigger-clicked').removeClass('edit-item-trigger-clicked')
            $("#edit-form").trigger("reset");
        })
    });


</script>



