
<section id="">
    <div class="row">
        <div class="col-12 mt-3 mb-1">
            <div class="content-header">
                Добавление области</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">

                </div>
                <div class="card-body">
                    <div class="card-block">
                        <form action="{{route('addAreaPost')}}" method="POST" class="form">
                            {{csrf_field()}}
                            <div class="form-body">
                                <div class="row">
                                    <div class=" col-md-6">
                                        <div class="form-group">
                                            <label for="companyName" class="sr-only">Название области UZ</label>
                                            <input type="text" name="area_name_uz"  class="form-control" placeholder="Название области UZ" required>
                                        </div>

                                        <div class="form-group  ">
                                            <label for="companyName" class="sr-only">Название областиRU</label>
                                            <input type="text" name="area_name_ru"  class="form-control" placeholder="Название области RU" required>
                                        </div>
                                    </div>
                                    <div class="form-group  col-md-6">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="sr-only">Регион СТАТУС</label>

                                               <input type="number" name="numberArea" id="" class="form-control" placeholder="ID области" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <h5>Region STATUS</h5>
                                                <table class="table table-responsive-md-md text-center table-striped table-bordered">
                                                    <thead>
                                                    <tr>

                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Тошкент - 1</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Тошкент вилояти - 2</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Андижон вилояти - 3</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Наманган  вилояти - 4</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Қўқон  вилояти - 5</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Жиззах вилояти - 6</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Бухоро вилояти - 7</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Самарқанд вилояти - 8</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Навоий вилояти - 9</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Сирдарё вилояти - 10</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Қашқадарё вилояти - 11</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Сурхондарё вилояти - 12</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Қорақолпоқистон вилояти - 13</p></tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                            </div>

                            <div class="form-actions">

                                <button type="submit" class="btn btn-raised btn-primary">
                                    <i class="ft-check"></i>
                                    Сохранить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>


