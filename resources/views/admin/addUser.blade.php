
<section id="">
    <div class="row">
        <div class="col-12 mt-3 mb-1">
            <div class="content-header">
                Добавление пользователя для логина и пароля</div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">

                </div>
                <div class="card-body">
                    <div class="card-block">
                        <form action="{{route('addUserPost')}}" method="POST" class="form">
                            {{csrf_field()}}
                            <div class="form-body">

                                <div class="row">
                                    <div class=" col-md-6">
                                        <div class="form-group">
                                            <label for="companyName" class="sr-only">Area Name UZ</label>
                                            <input type="text" name="name"  class="form-control" placeholder="Name" required>
                                        </div>

                                        <div class="form-group  ">
                                            <label for="companyName" class="sr-only">Area Name RU</label>
                                            <input type="email" name="email"  class="form-control" placeholder="Email" required>
                                        </div>

                                        <div class="form-group  ">
                                            <label for="companyName" class="sr-only">Area Name RU</label>
                                            <input type="password" name="password"  class="form-control" placeholder="Password" required>
                                        </div>
                                    </div>

                                    <div class="form-group  col-md-6">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="sr-only">Inspector ID</label>

                                                <input type="number" name="region_inspector_id" id="" class="form-control" placeholder="Inspector ID" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <h5>Inspector ID</h5>
                                                <table class="table table-responsive-md-md text-center table-striped table-bordered">
                                                    <thead>
                                                    <tr>

                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Админ - 0</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Тошкент - 1</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Тошкент вилояти - 2</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Андижон вилояти - 3</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Наманган  вилояти - 4</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Қўқон  вилояти - 5</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Жиззах вилояти - 6</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Бухоро вилояти - 7</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Самарқанд вилояти - 8</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Навоий вилояти - 9</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Сирдарё вилояти - 10</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Қашқадарё вилояти - 11</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Сурхондарё вилояти - 12</p></tr>
                                                    <tr><p style="font-size: 12px; padding: 0px; margin-bottom: 3px">Қорақолпоқистон вилояти - 13</p></tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                            </div>

                            <div class="form-actions">

                                <button type="submit" class="btn btn-raised btn-primary">
                                    <i class="ft-check"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>


