<section id="">
    <div class="row">
        <div class="col-12 mt-3 mb-1">
            <div class="content-header">
                Добавить videos</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body">
                    <div class="card-block">
                        {!! Form::open(['url' => 'videos', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="row">
                                <div class=" col-md-6">

                                    <div class="form-group">
                                        <label for="companyName" class="sr-only"></label>
                                        <input type="file" name="videos"  class="form-control" placeholder="" required>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-raised btn-primary">
                                <i class="ft-check"></i>
                                Сохранить
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>

<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">

                <div class="card-body">
                    <div class="card-block">

                        <table class="table table-responsive-md-md text-center table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>№</th>

                                <th>Video</th>
                                <th>Date</th>
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <tbody>
                              @foreach($vodeos as $item)
                                  @if($item->type == 2)

                                     <tr>
                                         <td>{{$item->id}}</td>

                                         <td>

                                             <video src=" {{$item->url}}" width="150"></video>
                                         </td>

                                         <td>
                                             {{$item->created_at}}
                                         </td>

                                         <td>
                                             <a href="/videos/deleted/{{$item->id}}">
                                                 <i class="ft-trash font-medium-3"></i>
                                             </a>
                                         </td>

                                     </tr>
                                     @endif

                                 @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>




