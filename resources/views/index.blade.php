@extends('layouts.base')

@section('header')
    @include('layouts.header')
@endsection
@section('content')
    @include('pages.'.$page)
@endsection
@section('footer')
    @include('layouts.footer')
@endsection

