<footer class="footer_part">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="single_footer_part">
                    <a href="" class="footer_logo_iner"> <img src="{{ asset('assets') }}/img/last.png" alt="#" width="60%"> </a>
                    <p>{{trans('msg.qilish')}}
                    </p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-6">
                <div class="single_footer_part">
                    <h4>{{trans('msg.aloqa')}}</h4>
                    <p>{{trans('msg.address')}}
                    </p>
                    <p>Факс: (99871) 236-33-31</p>
                    <p>Тел : 207-11-03</p>
                    <p>Email : <b>info@uznature.uz</b></p>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="single_footer_part">
                    <h4>{{trans('msg.sayt')}}</h4>

                        <ul>
                        <li><a href="http://eco.gov.uz">{{trans('msg.fot')}} </a></li>
                        <li><a href="http://man.uz/ru/">JV MAN Auto-Uzbekistan</a></li>
                        <li><a href="http://krantas.uz/">ЗАВОД ООО «KRANTAS»</a></li>
                        <li><a href="http://www.uzxcmg.uz/">Совместное предприятие ООО «UzXCMG» Хорезмская обл. г. Ургенч ул. Помышленная №1</a></li>
                        <li><a href="http://howo.uz/">КАРЬЕРНЫЕ САМОСВАЛЫ HOWO</a></li>
                        <li><a href="http://www.samauto.uz/uz/">Самаркандский автомобильный завод</a></li>
                        </ul>


                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-8">
                <div class="copyright_text">
                    <p>
                        {{trans('msg.all')}}
                    </p>

                </div>
            </div>
            <div class="col-lg-4">
                <div class="footer_icon social_icon">
                    <ul class="list-unstyled list-inline">
                        <li><a href="https://www.facebook.com/Uzecology/" class="single_social_icon" ><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://twitter.com/UzWaste" class="single_social_icon"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="http://eco.gov.uz" class="single_social_icon"><i class="fas fa-globe"></i></a></li>
                        <li><a href="https://t.me/wasteuz1" class="single_social_icon"><i class="fab fa-telegram"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCfU_xlpaE_xbRpjRhN2Prog" class="single_social_icon"><i class="fab fa-youtube"></i></a></li>
                        <li><a href="https://www.instagram.com/ekologiyadavlatqomitasi/" class="single_social_icon"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
{{--<script>
    $(document).ready(function(){
        $('[data-toggle="popover"]').popover();
    });
</script>--}}
