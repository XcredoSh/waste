
<header class="main_menu home_menu">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="{{route('indexOne')}}"> <img src="{{ asset('assets') }}/img/last.png" alt="logo" style="width: 200px" class="header_logo"> </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigatio">
                        <span class="menu_icon"><i class="fas fa-bars"></i></span>
                    </button>

                    <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                        <ul class="navbar-nav">

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('news')}}">{{trans('msg.newss')}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('articles')}}">{{trans('msg.maq')}}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('services')}}" style="padding-right: 5px; padding-left: 5px;"> {{trans('msg.services')}}</a>
                            </li>


                            <li class="nav-item">
                                <a class="nav-link" href="https://cleancity.uz/startpage;jsessionid=6863FE8714C8DC31FAE2F2E8F941EC21.thweb3">{{trans('msg.to_subscribers')}}</a>
                            </li>



                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="" id="navbarDropdown">
                                   {{trans('msg.infor')}}
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item"  href="{{route('about')}}" style="color: #ffffff" >{{trans('msg.my')}}</a>
                                    <a class="dropdown-item"  href="{{route('contact')}}" style="color: #ffffff" >{{trans('msg.contact')}}</a>
                                    <a class="dropdown-item"  href="{{route('opendata')}}" style="color: #ffffff" >{{trans('msg.open')}}</a>
                                </div>
                            </li>



                            @if(!Auth::id())

                            @endif
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="{{route('cabinetLogin')}}" id="navbarDropdown">
                                    {{trans('msg.kirish')}}
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item"  href="/register" style="color: #ffffff" >{{trans('msg.check_in')}}</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="" id="navbarDropdown"
                                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(session()->get('locale') == "uz")
                                        Ўзбекча
                                    @elseif(session()->get('locale') == "ru" or session()->get('locale') == "")
                                        Русский
                                    @endif
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    @if(session()->get('locale') == "uz" )
                                        <a class="dropdown-item" href="locale/ru">Русский</a>
                                    @elseif(session()->get('locale') == "ru" or session()->get('locale') == "")
                                        <a class="dropdown-item" href="locale/uz">Ўзбекча</a>
                                    @endif
                                </div>
                            </li>


                            @if(!Auth::id() == 0)
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    logout<span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Выйти') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endif

                        </ul>
                    </div>
                   {{-- <a id="search_1" href="javascript:void(0)"><i class="ti-search"></i></a>--}}
                </nav>
            </div>
        </div>
    </div>
    <div class="search_input" id="search_input_box">
        <div class="container ">
            <form class="d-flex justify-content-between search-inner">
                <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                <button type="submit" class="btn"></button>
                <span class="ti-close" id="close_search" title="Close Search"></span>
            </form>
        </div>
    </div>
</header>



