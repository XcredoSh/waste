<!DOCTYPE html>
<html lang="en" class="loading">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Apex admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Apex admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Admin</title>
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets') }}/app-assets/img/ico/apple-icon-60.html">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets') }}/app-assets/img/ico/apple-icon-76.html">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets') }}/app-assets/img/ico/apple-icon-120.html">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets') }}/app-assets/img/ico/apple-icon-152.html">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets') }}/app-assets/img/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets') }}/app-assets/img/ico/favicon-32.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    {{--<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">--}}
    <!-- BEGIN VENDOR CSS-->
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/vendors/css/prism.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/vendors/css/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/vendors/css/switchery.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN APEX CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/vendors/css/sweetalert2.min.css">
{{--<img src="{{ asset('assets') }}{{$menu->in_img}}" alt="Los Angeles" width="1365" height="500">
               <div class="carousel-caption">
                   <h3>{{$menu->img_title}}</h3>

               </div>--}}


    <!-- END APEX CSS-->
    <!-- BEGIN Page Level CSS-->
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <!-- END Custom CSS-->

    <style>

        .carousel-inner img {
            width: 100%;
            height: 100%;
        }




    </style>
</head>
<body data-col="2-columns" class=" 2-columns ">
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper">


    <!-- main menu-->
    <!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
    <div data-active-color="white" data-background-color="man-of-steel" data-image="app-assets/img/sidebar-bg/01.jpg" class="app-sidebar">
        <!-- main menu header-->
        <!-- Sidebar Header starts-->
        <div class="sidebar-header">
            <div class="logo clearfix"><a href="{{route('dashboard')}}" class="logo-text float-left">
                    <div class="logo-img"><img src="{{ asset('assets') }}/app-assets/img/logo.png"/></div><span class="text align-middle">WASTEUZ</span></a><a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block"><i data-toggle="expanded" class="ft-toggle-right toggle-icon"></i></a><a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a></div>
        </div>
        <!-- Sidebar Header Ends-->
        <!-- / main menu header-->
        <!-- main menu content-->
        <div class="sidebar-content">
            <div class="nav-container">
                <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">


                    <?php
                    $toshkent = Session::get('toshkent');
                    $toshkentVil = Session::get('toshkentVil');
                    $andijonVil = Session::get('andijonVil');
                    $namanganVil = Session::get('namanganVil');
                    $qoqonVil = Session::get('qoqonVil');
                    $jizzaxVil = Session::get('jizzaxVil');
                    $buxoroVil = Session::get('buxoroVil');
                    $samarqandVil = Session::get('samarqandVil');
                    $navoiyVil = Session::get('navoiyVil');
                    $sirdaryoVil = Session::get('sirdaryoVil');
                    $qashqadaryoVil = Session::get('qashqadaryoVil');
                    $surhondaryoVil = Session::get('surhondaryoVil');
                    $qoraqolpoqistonVil = Session::get('qoraqolpoqistonVil');
                    $newser = Session::get('newser');
                        ?>
                    @if($newser)
                        <li class=" nav-item"><a href="{{route('admin_news')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Новости</span></a>
                        </li>
                        <li class=" nav-item"><a href="{{route('admin_articlea')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Статьи</span></a>
                        </li>

                            <li class=" nav-item"><a href="{{route('photos')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Photos</span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('videos')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Videos</span></a>
                            </li>
                        @elseif($toshkent and !$newser)
                        <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                        </li>

                        <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                        </li>
                        @elseif($toshkentVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>

                        @elseif($andijonVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>

                        @elseif($namanganVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>

                        @elseif( $qoqonVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>

                        @elseif(  $jizzaxVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>

                        @elseif(  $buxoroVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>
                        @elseif(  $samarqandVil and !$newser )
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>
                        @elseif( $navoiyVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>
                        @elseif( $sirdaryoVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>
                        @elseif(  $qashqadaryoVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>
                        @elseif(  $surhondaryoVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>

                        @elseif( $qoraqolpoqistonVil and !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>

                       @elseif( !$newser)
                            <li class="has-sub nav-item"><a href="{{route('dashboard')}}"><i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span><span class="tag badge badge-pill badge-danger float-right mr-1 mt-1"></span></a>
                            </li>

                            <li class=" nav-item"><a href="{{route('people')}}"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Обращение населения</span></a>
                            </li>
                    @endif



                    @if(!$toshkent)
                        @if(!$toshkentVil)
                            @if(!$andijonVil)
                                @if(!$namanganVil)
                                    @if(!$qoqonVil)
                                        @if(!$jizzaxVil)
                                            @if(!$buxoroVil)
                                                @if(!$samarqandVil)
                                                    @if(!$navoiyVil)
                                                        @if(!$sirdaryoVil)
                                                            @if(!$qashqadaryoVil)
                                                                @if(!$surhondaryoVil)
                                                                    @if(!$qoraqolpoqistonVil)
                                                                    @if(!$newser)
                                                                        <li class=" nav-item"><a href="{{route('hot_lines')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Горячая линия</span></a>
                                                                        </li>

                                                                                <li class=" nav-item"><a href="{{route('info')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Информация</span></a>
                                                                                </li>


                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Документ</span></a>
                        </li>
                                                                       {{-- <li class=" nav-item"><a href="{{route('admin_news')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Новости</span></a>
                                                                        </li>--}}
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Документы</span></a>
                                                    <ul class="menu-content">
                                                        <li class=""><a href="{{route('codes')}}" class="menu-item">Кодексы</a>
                                                        </li>
                                                        <li><a href="{{route('laws')}}" class="menu-item">Законы</a>
                                                        </li>
                                                        <li><a href="{{route('decisions')}}" class="menu-item">Решения</a>
                                                        </li>
                                                        <li><a href="{{route('another')}}" class="menu-item">Другой </a>
                                                        </li>
                                                    </ul>
                                                </li>
                         <li class=" nav-item"><a href="{{route('addRegion')}}"><i class="ft-globe"></i><span data-i18n="" class="menu-title">Добавление региона</span></a>
                         <li class=" nav-item"><a href="{{route('addArea')}}"><i class="ft-globe"></i><span data-i18n="" class="menu-title">Добавление области</span></a>

                        <li class=" nav-item"><a href="{{route('addUser')}}"><i class="ft-user"></i><span data-i18n="" class="menu-title">Создать пользователя</span></a>
                                                </li>
                                                                        <li class=" nav-item"><a href="{{route('addImgs')}}"><i class="ft-menu"></i><span data-i18n="" class="menu-title">Добавление <br> рекламных изображений</span></a>
                                                                        </li>

                                                                        <li class=" nav-item"><a href="{{route('addTbo')}}"><i class="ft-globe"></i><span data-i18n="" class="menu-title">ТБО <br></span></a>
                                                                        </li>


                                                                        <li class=" nav-item"><a href="{{route('addMsp')}}"><i class="ft-globe"></i><span data-i18n="" class="menu-title">МСП <br></span></a>
                                                                        </li>

                                                                        <li class=" nav-item"><a href="{{route('addWaste')}}"><i class="ft-globe"></i><span data-i18n="" class="menu-title">{{trans('msg.pre')}} <br></span></a>
                                                                        </li>

                                                                        <li class=" nav-item"><a href="{{route('addPoints')}}"><i class="ft-globe"></i><span data-i18n="" class="menu-title">Ближайший премични пункты<br></span></a>
                                                                        </li>
                                                                    @endif
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endif
                                                    @endif
                                                @endif      `
                                            @endif
                                        @endif
                                    @endif
                                @endif
                            @endif
                        @endif

                     {{--   @elseif($newser)
                        <li class=" nav-item"><a href="{{route('admin_news')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Новости</span></a>
                        </li>
                        <li class=" nav-item"><a href="{{route('admin_articlea')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Новости artcls</span></a>
                        </li>--}}
                    @endif




                    {{--@if(!$toshkentVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$andijonVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$namanganVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$qoqonVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$jizzaxVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$buxoroVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$samarqandVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$navoiyVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$sirdaryoVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$qashqadaryoVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$surhondaryoVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif
                    @if(!$qoraqolpoqistonVil)
                        <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                        </li>
                        <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                            <ul class="menu-content">
                                <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                </li>
                                <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                </li>
                                <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                </li>
                                <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                        </li>`
                    @endif--}}

                 {{--   @if(!$toshkent || !$toshkentVil)
                        @if(!$andijonVil or !$namanganVil)
                            @if(!$qoqonVil or !$jizzaxVil)
                                @if( !$buxoroVil or !$samarqandVil)
                                    @if(!$navoiyVil or !$sirdaryoVil)
                                        @if(!$qashqadaryoVil or !$surhondaryoVil)
                                            @if(!$qoraqolpoqistonVil)
                                                <li class=" nav-item"><a href="{{route('documents')}}"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                                                </li>
                                                <li class="has-sub nav-item"><a href="#"><i class="ft-file-text"></i><span data-i18n="" class="menu-title">Documents</span></a>
                                                    <ul class="menu-content">
                                                        <li class=""><a href="{{route('codes')}}" class="menu-item">Кўдекслар</a>
                                                        </li>
                                                        <li><a href="{{route('laws')}}" class="menu-item">Қонунлар</a>
                                                        </li>
                                                        <li><a href="{{route('decisions')}}" class="menu-item">Қарорлар</a>
                                                        </li>
                                                        <li><a href="{{route('another')}}" class="menu-item">Бошқа </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class=" nav-item"><a href=""><i class="ft-user"></i><span data-i18n="" class="menu-title">Create User</span></a>
                                                </li>
                                            @endif
                                        @endif
                                    @endif
                                @endif
                            @endif
                        @endif  `
                    @endif--}}


                </ul>
            </div>
        </div>





        <!-- main menu content-->
        <div class="sidebar-background"></div>
        <!-- main menu footer-->
        <!-- include includes/menu-footer-->
        <!-- main menu footer-->
    </div>
    <!-- / main menu-->

    <div class="main-panel">

        <!-- Navbar (Header) Starts-->
       @yield('header')
        <!-- Navbar (Header) Ends-->

        <div class="main-content">
            <div class="content-wrapper"><!--Statistics cards Starts-->
                @yield('content')
            </div>
        </div>

        <footer class="footer footer-static footer-light">
            @yield('footer')
        </footer>

    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->

<div class="modal fade text-left" id="delete-codes" tabindex="1" role="dialog" aria-labelledby="myModalLabel35"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel35">Удалить страны
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="deleteCodes" method="post" action="">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <h4><i class="fa fa-warning" style="color: #ffbb01;"></i> Вы хотите удалить?</h4>

                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal"
                           value="Нет">
                    <input type="submit" class="btn btn-outline-primary btn-lg" value="Да">
                </div>
            </form>
        </div>
    </div>
</div>




<!-- BEGIN VENDOR JS-->
<script src="{{ asset('assets') }}/app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/core/popper.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/prism.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/screenfull.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset('assets') }}/app-assets/vendors/js/chartist.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN APEX JS-->
<script src="{{ asset('assets') }}/app-assets/js/app-sidebar.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/js/notification-sidebar.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/js/customizer.js" type="text/javascript"></script>
<!-- END APEX JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{ asset('assets') }}/app-assets/js/dashboard1.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->

<script src="{{ asset('assets') }}/app-assets/js/switch.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/switchery.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/js/data-tables/datatable-basic.js" type="text/javascript"></script>


<script>
    $("#for_tx").hide();
    $("#for_pd").hide();
    $("#for_lk").hide();
    $("#for_ph").hide();
    function tx()
    {
        $(document).ready(function(){
            $("#for_tx").show();
            $("#for_pd").hide();
            $("#for_lk").hide();
            $("#for_ph").hide();
        });

    }
    function pd() {
        $(document).ready(function(){
            $("#for_pd").show();
            $("#for_lk").hide();
            $("#for_ph").hide();
            $("#for_tx").hide();
        });
    }
    function lk() {
        $(document).ready(function(){
            $("#for_lk").show();
            $("#for_ph").hide();
            $("#for_tx").hide();
            $("#for_pd").hide();
        });
    }
    function ph() {
        $(document).ready(function(){
            $("#for_ph").show();
            $("#for_tx").hide();
            $("#for_pd").hide();
            $("#for_lk").hide();
        });
    }

</script>


<script>
    if (0 < 1){
        $('#rowCheck').hide();
    }
    function check() {
        var value = document.getElementById('myCheck').value;
        var x=$("#myCheck").is(":checked");
        if (x == true){
            $('#rowCheck').show();
            $('#inputCheck').hide();
            $('#file').hide();
        }else{
            $('#rowCheck').hide();
            $('#inputCheck').show();
            $('#file').show();
        }
    }
</script>

{{--<script type="text/javascript">
    $(document).ready(function () {
        $('#delete-codes').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('item-id');

            $('#deleteCodes').attr("action", "{{ url('/dashboard/codes') }}" + "/" + id);
        })

    });
</script>--}}

<script>
    $('#sizing').hide();
    function myAnswer(){
        $(document).ready(function () {
          $('#sizing').show(2000);
          $('#answerDen').hide(1500);
          $('#answerYes').hide(1500);
        })
    }
</script>




</body>
</html>
