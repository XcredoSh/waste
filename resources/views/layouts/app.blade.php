{{--<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>   WASTEUZ</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>--}}
    <!DOCTYPE html>
<html lang="en" class="loading">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Apex admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Apex admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Login Page - Apex responsive bootstrap 4 admin template</title>
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets') }}/app-assets/img/ico/apple-icon-60.html">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets') }}/app-assets/img/ico/apple-icon-76.html">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets') }}/app-assets/img/ico/apple-icon-120.html">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets') }}/app-assets/img/ico/apple-icon-152.html">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets') }}/app-assets/img/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets') }}/app-assets/img/ico/favicon-32.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/vendors/css/prism.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/app-assets/css/app.css">
    <body data-col="1-column" class=" 1-column  blank-page blank-page">
<body>
    <div id="app">


        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>


<script src="{{ asset('assets') }}/app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/core/popper.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/prism.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/screenfull.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>

<script src="{{ asset('assets') }}/app-assets/js/app-sidebar.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/js/notification-sidebar.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/app-assets/js/customizer.js" type="text/javascript"></script>

</body>
</html>
