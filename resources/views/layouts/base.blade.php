<!doctype html>
<html lang="{{app()->getLocale()}}">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>WASTEUZ</title>
    <link rel="icon" href="{{ asset('assets') }}/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/bootstrap.min.css">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
    <!-- animate CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="{{ asset('assets') }}/css/aos.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/mediaelementplayer.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/all.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/flaticon.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/themify-icons.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/nice-select.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/style.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/input/style.css" media="screen" title="no title" charset="utf-8">







    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{ asset('assets') }}/js/jquery-1.12.1.min.js"></script>


    <style>
        .block_items:hover{
            background-color: green;
            color: #ffffff;
        }
        .bg_val:hover{
            background-color: #0ab6ff;
            color: #ffffff;
        }
        .bg_h5:hover{
            color: #FFFFFF;
        }


    </style>
    <style>
        .ul_marg{
            margin: 0px;
            padding: 0px;
        }
        .li_none{
            list-style: none;
        }
        .user-wraper, .message-wrapper{
            border: 1px solid #dddddd;
            overflow-y: auto;
        }
        .user-wraper{
            height: 600px;
        }
        .user{
            cursor: pointer;
            padding: 5px 0px;
            position: relative;
        }
        .user:hover{
            background-color: #eeeeee;
        }
        .user:last-child{
            margin-bottom: 0px;
        }
        .pending{
            position: absolute;
            left: 13px;
            top: 9px;
            background-color: #b600ff;
            margin: 0px;
            border-radius: 50%;
            width: 10px;
            height: 10px;
            line-height: 18px;
            padding-left:  5px;
            color: #ffffff;
            font-size: 12px;
        }
        .media-left{
            margin: 0 10px;
        }
        .media-left img{
            width: 64px;
            border-radius: 64px;
        }
        .media-body p{
            padding: 6px 0px;
        }
        .message-wrapper{
            padding: 10px;
            height: 536px;
            background-color: #eeeeee;
        }
        .messages, .message {
            margin-bottom: 15px;
        }
        .recevied, .sent{
            width: 45%;
            padding: 3px 10px;
            border-radius: 10px;
        }
        .recevied{
            background-color: #ffffff;
        }
        .messages, message:last-child{
            margin-bottom: 0px;
        }
        .sent{
            background-color: #3bebff;
            float: right;
            text-align: right;
        }
        .message p{
            margin: 5px 0px ;
        }
        .date{
            color: darkblue;
        }
        #chat_input{
            width: 100%;
            padding: 12px 20px;
            margin: 15px 0 0 0;
            display: inline-block;
            border-radius: 4px;
            box-sizing: border-box;
            outline: none;
            border: 1px solid #cccccc;
        }
        #chat_input:focus{
            border: 1px solid #aaaaaa;
        }
        .img_media{
            width: 100%;
            height: 600px;

        }
        @media screen and (max-width: 768px){
            .img_title_med{
                width: 250px;
            }


        }

        @media screen and (max-width: 480px){
            .img_media{
                width: 100%;
                height: 200px;
            }
            .header_logo{
                width: 100px;
                position: relative;
                padding-bottom: -160px;
            }

        }
        @media screen and (min-width: 768px){
            .img_title_med{
                width: 250px;
            }


        }
        @media screen and (max-width: 768px){
            .action_map{
                height: 400px;
            }
            .img_sixza{
                height: 350px;
            }

        }
        @media screen and (max-width: 974px){
            .action_map{
                height: 400px;
            }
            .img_sixza{
                height: 350px;
            }
        }
        #juridical:hover{
            background-color: yellow;
        }
        #jismoniy:hover{
            background-color: yellow;
        }

    </style>
    <style>
      /*  .carousel-inner img {
            width: 100%;
            height: 100%;
        }
        .ulVideo:hover{
            background-color: #0afff4;
            color: #fff3cd;
        }*/
      .site-blocks-cover {
          background-size: cover;
          background-repeat: no-repeat;
          background-position: top;
          background-position: center center; }
      .site-blocks-cover.overlay {
          position: relative; }
      .site-blocks-cover.overlay:before {
          position: absolute;
          content: "";
          left: 0;
          bottom: 0;
          right: 0;
          top: 0;
          background: rgba(0, 0, 0, 0.4); }
      .site-blocks-cover .player {
          position: absolute;
          bottom: -250px;
          width: 100%; }
      .site-blocks-cover, .site-blocks-cover .row {
          min-height: 600px;
          height: calc(100vh - 73px); }
      .site-blocks-cover.inner-page-cover, .site-blocks-cover.inner-page-cover .row {
          min-height: 600px;
          height: calc(30vh); }
      .site-blocks-cover h1 {
          text-transform: uppercase;
          font-size: 4rem;
          font-weight: 900;
          color: #fff;
          line-height: 1.5; }
      @media (max-width: 991.98px) {
          .site-blocks-cover h1 {
              font-size: 2rem; } }
      .site-blocks-cover p {
          color: rgba(255, 255, 255, 0.5);
          font-size: 1.2rem;
          line-height: 1.5; }
      .site-blocks-cover .btn {
          border: 2px solid transparent; }
      .site-blocks-cover .btn:hover {
          color: #fff !important;
          background: none;
          border: 2px solid #fff; }
      .site-blocks-cover .intro-text {
          font-size: 16px;
          line-height: 1.5; }
      @media (max-width: 991.98px) {
          .site-blocks-cover .display-1 {
              font-size: 3rem; } }

    </style>

</head>


<body>
<!--::header part start::-->
@yield('header')
<!-- Header part end-->

<!-- banner part start-->
@yield('content')
<!-- banner part start-->

<!-- feature_part start-->
@yield('footer')


<div class="modal fade bs-modal-sm" id="signin" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="z-index: 9999999;">
    <div class="modal-dialog modal-sm ">
        <div class="modal-content ">
            <br>
            <div class="bs-example bs-example-tabs">
                <ul id="myTab" class="nav nav-tabs" style="margin: 10px">
                    {{--<li class="active" style="margin: 5px"><a href="#signin" data-toggle="tab">Вход</a></li>--}}
                    <li class="" style="margin: 5px"><a href="" data-toggle="tab">Вход</a></li>
                </ul>
            </div>
            <div class="modal-body">
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active show" id="">
                        <form class="form-horizontal" method="post" action="{{route('login')}}">
                            {{csrf_field()}}
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="userid">E-mail пользователя:</label>
                                    <div class="controls">
                                        <input required="" name="email" type="text" class="form-control" placeholder="E-mail" class="input-medium" required="">
                                    </div>
                                </div>

                                <!-- Password input-->
                                <div class="control-group">
                                    <label class="control-label" for="passwordinput">Пароль:</label>
                                    <div class="controls">
                                        <input required=""  name="password" class="form-control" type="password" placeholder="********" class="input-medium">
                                    </div>
                                </div>

                                <p class="alert-danger">
                                    <?php
                                    $message = Session::get('message');
                                    if($message){
                                        echo $message;
                                        Session::put('message', null);
                                    }
                                    ?>
                                </p>

                                <!-- Button -->
                                <div class="control-group">
                                    <label class="control-label" for="signin"></label>
                                    <div class="controls">
                                        <button id="signin" type="submit" name="signin" style="width: 100px; color: #000000" class="btn btn-success">Вход</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-default" style="width: 100px; margin-right: 20px;" data-dismiss="modal">Close</button>
                </center>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-sm" id="signup" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="z-index: 9999999;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content col-lg-12">
            <br>
            <div class="bs-example bs-example-tabs">
                <ul id="myTab" class="nav nav-tabs" style="margin: 10px">
                    {{--<li class="active" style="margin: 5px"><a href="#signin" data-toggle="tab">Вход</a></li>--}}
                    <li class="" style="margin: 5px"><a href="" data-toggle="tab">Регистрация</a></li>
                </ul>
            </div>
            <div class="modal-body">
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade  active show"  id="">
                        <form class="form-horizontal" method="post" action="{{route('register')}}">
                            {{csrf_field()}}
                            <fieldset>


                                <!-- Text input-->
                                <div class="control-group">
                                    <label class="control-label" for="userid">Имя пользователя:</label>
                                    <div class="controls">
                                        <input  name="name" class="form-control" type="text" placeholder="Имя" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="control-group">
                                    <label class="control-label" for="userid">Фамилия пользователя:</label>
                                    <div class="controls">
                                        <input  name="lastname" class="form-control" type="text" placeholder="Имя" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="control-group">
                                    <label class="control-label" for="Email">Электронная почта:</label>
                                    <div class="controls">
                                        <input  name="email" class="form-control" type="text" placeholder="test@example.com" class="input-large" required="">
                                    </div>
                                </div>


                                <!-- Password input-->
                                <div class="control-group">
                                    <label class="control-label" for="password">Пароль:</label>
                                    <div class="controls">
                                        <input name="password" class="form-control" type="password" placeholder="********" class="input-large" required="">
                                        <em>1-8 символов</em>
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="control-group">
                                    <label class="control-label" for="reenterpassword">Повторно введите пароль:</label>
                                    <div class="controls">
                                        <input  class="form-control" name="c_userpass" type="password" placeholder="********" class="input-large" required="">
                                    </div>
                                </div>

                                <!-- Multiple Radios (inline) -->
                                <br>

                                <div class="control-group">
                                    <label class="control-label" for="confirmsignup"></label>
                                    <div class="controls">
                                        <button  type="submit" name="confirmsignup" style="width: 150px; color: #000000; margin-bottom: 8px;" class="btn btn-success">Регистрация</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-default" style="width: 100px;" data-dismiss="modal">Close</button>
                </center>
            </div>
        </div>
    </div>
</div>
<!-- End -->


<!-- jquery plugins here-->
<!-- jquery -->

<!-- popper js -->
<script src="{{ asset('assets') }}/js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="{{ asset('assets') }}/js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="{{ asset('assets') }}/js/jquery.magnific-popup.js"></script>

<!-- swiper js -->
<script src="{{ asset('assets') }}/js/swiper.min.js"></script>
<!-- swiper js -->
<script src="{{ asset('assets') }}/js/masonry.pkgd.js"></script>
<!-- particles js -->

<!-- slick js -->
<script src="{{ asset('assets') }}/js/slick.min.js"></script>
<script src="{{ asset('assets') }}/js/jquery.counterup.min.js"></script>
<script src="{{ asset('assets') }}/js/waypoints.min.js"></script>
{{--<script src="{{ asset('assets') }}/js/contact.js"></script>--}}
<script src="{{ asset('assets') }}/js/jquery.ajaxchimp.min.js"></script>
<script src="{{ asset('assets') }}/js/jquery.form.js"></script>
<script src="{{ asset('assets') }}/js/jquery.validate.min.js"></script>
<script src="{{ asset('assets') }}/js/mail-script.js"></script>
<!-- custom js -->
<script src="{{ asset('assets') }}/js/custom.js"></script>
<script src="{{ asset('assets') }}/mapdata.js"></script>
<script src="{{ asset('assets') }}/countrymap.js"></script>
<script src="{{ asset('assets') }}/js/jquery.stellar.min.js"></script>
<script src="{{ asset('assets') }}/js/owl.carousel.min.js"></script>
<script src="{{ asset('assets') }}/js/jquery.nice-select.min.js"></script>
<script src="{{ asset('assets') }}/js/aos.js"></script>




<script>

    $(document).ready(function(){
        $(".demoPhoto").hide();
        $(".demoVideo").show();
        $("#demoVideo").click(function(){
            $(".demoPhoto").hide();
            $(".demoVideo").show();
        });
        $("#demoPhoto").click(function(){
            $(".demoVideo").hide();
            $(".demoPhoto").show();
        });
    });
   /* var demoPhoto = document.getElementById('demoPhoto');
    var demoVideo = document.getElementById('demoVideo');
    $("#demoPhoto").hide();
    $("#demoVideo").hide();

    function demoVideo() {
        $("#demoPhoto").hide();
    }

    function demoPhoto() {
        $("#demoVideo").hide();
    }*/
</script>
</body>

</html>
