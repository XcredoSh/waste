<section class="" style="padding-top: 60px;">
    <dic class="container-fluid" >
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

                    <div id="demo" class="carousel slide col-md-12" data-ride="carousel">
                        <ul class="carousel-indicators">
                            <li data-target="#demo" data-slide-to="0" class="active"></li>
                            <li data-target="#demo" data-slide-to="1"></li>
                            <li data-target="#demo" data-slide-to="2"></li>
                        </ul>
                        <div class="carousel-inner">
                            @foreach($img_menu as $key => $img)
                                @if($key == 0)
                                    <div class="carousel-item active" style="background-color: #0ab6ff;">
                                        <img src="{{ asset('assets') }}{{$img->in_img}}" alt="Los Angeles" class="img_media">
                                        <div class="carousel-caption" style="position: absolute; padding-bottom: 300px;  height: 100px;">
                                            <div class="img_title_med" style="background-color: #14A76C; position: relative; height: 110px; opacity: 0.7; padding-left: 15px; width: 850px; margin-top: -150px;">
                                                <h1 style="color: #FFFFFF; position: absolute; font-size: 64px;" >{{$img->img_title}}</h1><br><br><br>
                                                <h3 style="color: #ffffff; position: absolute;">{{trans('msg.industry_portal')}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                @elseif($key > 0)
                                    <div class="carousel-item " style="background-color: #0ab6ff;">
                                        <img src="{{ asset('assets') }}{{$img->in_img}}" alt="Los Angeles" class="img_media">
                                        <div class="carousel-caption " style="position: absolute; padding-bottom: 300px;  height: 100px;">
                                            <div class="img_title_med" style="background-color: #14A76C; position: relative; height: 110px; opacity: 0.7; padding-left: 15px; width: 850px; margin-top: -150px;">
                                            <h1 style="color: #ffffff; position: absolute;  font-size: 64px;">{{$img->img_title}}</h1><br><br><br>
                                                <h3 style="color: #FFFFFF; position: absolute; ">{{trans('msg.industry_portal')}}</h3></div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </div>
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                    </div>

            </div>
        </div>

    </dic>
</section>
<section class="" style="">
    <div class="container">
        <h4>{{trans('msg.pnews')}}</h4>
        <div class="row">
            @foreach($newsы as $news)
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class=" "  style="">
                        <a href="{{route('news')}}">
                            <div class="single-defination" style=" border: 1px solid #ccc; padding: 10px; height: 181px">
                                <img src="{{$news->img}}"  style="float: left; margin-right: 8px;  width: 160.33px; height: 140px;">
                                @if(session()->get('locale') == "ru" or session()->get('locale') == "")
                                <h6 style="padding-top: 10px; text-align: justify;" id="textWith">{{$news->news_name_ru}}</h6>
                                @elseif(session()->get('locale') == "uz")
                                    <h6 style="padding-top: 10px; text-align: justify;" id="textWith">{{$news->news_name_uz}}</h6>
                                @endif
                            </div>
                            <b class="date">{{date('d M y, h:i', strtotime($news->created))}}</b>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<script>
   var te =  $('#textWith');
    console.log(te);
   te.text(te.text().substring(0,80))
</script>
<br>
<section class="padding_top single_feature" style="padding-top: 80px; padding-bottom: 80px; background-color: #F4F4F4">
    <div class="container">
        <div class="row ">
            <div class="col-lg-8 col-xl-8 col-sm-12 col-md-12 single_feature">
                <div class="cta_text text-center single_feature_part">
                    <h1 style="color: cornflowerblue;">{{trans('msg.applications')}}</h1>
                    <li style="list-style: none;">{{trans('msg.ilova')}}</li><br>
                    <a href="{{route('hot_line')}}"  class="btn_2 banner_btn_1" style="border: none; background-color: #0ab6ff; color: #ffffff;">{{trans('msg.zayav')}}</a>
                   {{-- <a href=""  data-toggle="modal" data-target="#signup" class="btn_2 banner_btn_1" style="border: none; background-color: #0ab6ff; color: #ffffff;">Зарегистрироваться</a>--}}
                    <a href="{{route('cabinetLogin')}}"  class="btn_2 banner_btn_2" style="border: none; background-color: yellow; color: #000000;">{{trans('msg.status')}}</a>
                </div>
            </div>
            <div class="col-lg-4" class="h4_stat">
                <h4 style="float: left; " >{{trans('msg.stat')}}</h4><br><br>
                <div class=""  >
                    <ul class="list-group">

                        <li class="list-group-item" style="padding-top: 3px; padding-bottom: 3px; margin-bottom: 5px; border: none;" >{{trans('msg.arizz')}}:  <span class="badge">{{count($apps)}}</span></li>
                        <li class="list-group-item"  style="padding-top: 3px; padding-bottom: 3px; margin-bottom: 5px; border: none;">{{trans('msg.chiqildi')}}: <span class="badge">{{count($users)}}</span></li>
                        <li class="list-group-item"  style="padding-top: 3px; padding-bottom: 3px; margin-bottom: 5px; border: none;">{{trans('msg.kor')}}: <span class="badge">3</span></li>

                    </ul>


                </div>
            </div>
        </div>
    </div>
</section><br><br><br>
<section class="use_sasu ">
    <div class="container">
        <div class="row ">
            <div class="col-md-6">
                <button type="button" id="" style="width: 100% ; border: 0px; border-radius: 5px; padding: 10px; background-color:  #0ab6ff; color: #ffffff; float: right;" data-toggle="tooltip" data-placement="top" title="Пожалуйста, зарегистрируйтесь!"><h5 style="margin: 0px;  color: #ffffff;">{{trans('msg.sub')}}</h5></button>
            </div>

        </div>
    </div>
</section><br><br><br><br>



<!-- all services -->
<section class="use_sasu ">
    <div class="container">

        <h5 class="title_uslugi" style="display: none;">{{trans('msg.xiz')}}</h5>

        <div class="row justify-content-center " >


                <div class="col-lg-3 col-sm-6 " >
                    <div class="single_feature">
                        <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                            <img src="{{ asset('assets') }}/img/icon/gps.png" style=" position: relative" alt="">
                            {{--  <i class="fa fa-map-marker fa-5x"  aria-hidden="true" style="   color: green; "></i>--}}
                            <a href="https://wialon.uz/" style="" >
                                <h4 style="color: cornflowerblue;">{{trans('msg.gps')}}</h4>
                                <p>{{trans('msg.online')}}</p>
                            </a>

                        </div>
                    </div>
                </div>

                        <div class="col-lg-3 col-sm-6 " >
                            <div class="single_feature">
                                <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                                    <img src="{{ asset('assets') }}/img/icon/factory1.png" style=" position: relative" alt="">
                                    <a href="{{route('structure')}}">
                                        <h4 style="color: cornflowerblue;">{{trans('msg.inf')}}</h4>
                                        <p>{{trans('msg.may')}}</p>
                                    </a>
                                </div>
                            </div>
                        </div>






                        <div class="col-lg-3 col-sm-6  " >
                            <div class="single_feature">
                                <div class="single_feature_part"  style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                                    <img src="{{ asset('assets') }}/img/icon/package1.png" style=" position: relative" alt="">
                                    <a href="{{route('services')}}">
                                        <h4 style="color: cornflowerblue;">{{trans('msg.normat')}}</h4>
                                        <p>

                                            {{trans('msg.chiq')}}</p>
                                    </a>

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6  " >
                            <div class="single_feature">
                                <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                                    <img src="{{ asset('assets') }}/img/icon/garbage1.png" style=" position: relative" alt="">
                                    <a class="nav-link"  href="#" data-toggle="modal" data-target="#signin">
                                        <h4 style="color: cornflowerblue;">{{trans('msg.yiq')}}</h4>
                                        <p>
                                            {{trans('msg.chiqindi')}}</p>
                                    </a>

                                </div>
                            </div>
                        </div>
            <div class="col-lg-3 col-sm-6  ">
                <div class="single_feature">
                    <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/garbage-truck1.png" style=" position: relative" alt="">
                        <a href="{{route('special_machinery')}}">
                            <h4 style="color: cornflowerblue;">{{trans('msg.max')}}</h4>
                            <p> {{trans('msg.xarid')}}</p>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 ">
                <div class="single_feature">
                    <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/timing-belt1.png" style=" position: relative" alt="">
                        <a href="{{route('equipment')}}">
                            <h4 style="color: cornflowerblue;">{{trans('msg.usk')}}</h4>
                            <p>
                                {{trans('msg.xarid')}}</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6 " >
                <div class="single_feature">
                    <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/water1.png" style=" position: relative" alt="">
                        <a href="{{route('recyclable_materials')}}">
                            <h4 style="color: cornflowerblue;">{{trans('msg.qayta')}}</h4>
                            <p>

                                {{trans('msg.ikki')}}</p>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 " >
                <div class="single_feature">

                </div>
            </div>




        </div>

    </div>

</section>
<br><br><br><br>
<section class="use_sasu ">
    <div class="container">
        <div class="row ">

            <div class="col-md-6">
                <button type="button" id="" style="width: 100% ; border: 0px; border-radius: 5px;  padding: 10px; background-color:  #0ab6ff; color: #ffffff;" data-toggle="tooltip" data-placement="top" title="Пожалуйста, зарегистрируйтесь!"><h5 style="margin: 0px;  color: #ffffff;">{{trans('msg.sab')}}</h5></button>
            </div>
        </div>
    </div>
</section><br><br><br><br>
<section class="use_sasu ">
    <div class="container">

        <h5 class="title_uslugi" style="display: none;">{{trans('msg.xiz')}}</h5>

        <div class="row justify-content-center " >

            <div class="col-lg-3 col-sm-6  " >
                <div class="single_feature">
                    <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/litter1.png" style=" position: relative" alt="">{{--<i class="fa fa-users fa-5x" style="color: #00FF00;" aria-hidden="true"></i>--}}
                        <a href="https://cleancity.uz/startpage;jsessionid=6863FE8714C8DC31FAE2F2E8F941EC21.thweb3" >
                            <h4 style="color: cornflowerblue;">{{trans('msg.aba')}}</h4>
                            <p>{{trans('msg.xis')}}</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6 " >
                <div class="single_feature">
                    <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/gps.png" style=" position: relative" alt="">
                        {{--  <i class="fa fa-map-marker fa-5x"  aria-hidden="true" style="   color: green; "></i>--}}
                        <a href="https://wialon.uz/" style="" >
                            <h4 style="color: cornflowerblue;">{{trans('msg.gps')}}</h4>
                            <p>{{trans('msg.online')}}</p>
                        </a>

                    </div>
                </div>
            </div>




            <div class="col-lg-3 col-sm-6  ">
                <div class="single_feature">
                    <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/garbage-truck1.png" style=" position: relative" alt="">
                        <a href="{{route('special_machinery')}}">
                            <h4 style="color: cornflowerblue;">{{trans('msg.max')}}</h4>
                            <p> {{trans('msg.xarid')}}</p>
                        </a>

                    </div>
                </div>
            </div>









            <div class="col-lg-3 col-sm-6 " >
                <div class="single_feature">
                    <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/factory1.png" style=" position: relative" alt="">
                        <a href="{{route('structure')}}">
                            <h4 style="color: cornflowerblue;">{{trans('msg.inf')}}</h4>
                            <p>{{trans('msg.may')}}</p>
                        </a>
                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-sm-6 ">
                <div class="single_feature">
                    <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/timing-belt1.png" style=" position: relative" alt="">
                        <a href="{{route('equipment')}}">
                            <h4 style="color: cornflowerblue;">{{trans('msg.usk')}}</h4>
                            <p>
                                {{trans('msg.xarid')}}</p>
                        </a>
                    </div>
                </div>
            </div>





            <div class="col-lg-3 col-sm-6  " >
                <div class="single_feature">
                    <div class="single_feature_part"  style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/package1.png" style=" position: relative" alt="">
                        <a href="{{route('services')}}">
                            <h4 style="color: cornflowerblue;">{{trans('msg.normat')}}</h4>
                            <p>

                                {{trans('msg.chiq')}}</p>
                        </a>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6  " >
                <div class="single_feature">
                    <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/garbage1.png" style=" position: relative" alt="">
                        <a class="nav-link"  href="#" data-toggle="modal" data-target="#signin">
                            <h4 style="color: cornflowerblue;">{{trans('msg.yiq')}}</h4>
                            <p>
                                {{trans('msg.chiqindi')}}</p>
                        </a>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6 " >
                <div class="single_feature">
                    <div class="single_feature_part" style="padding-top: 5px; padding-bottom: 5px; height: 309px;">
                        <img src="{{ asset('assets') }}/img/icon/water1.png" style=" position: relative" alt="">
                        <a href="{{route('recyclable_materials')}}">
                            <h4 style="color: cornflowerblue;">{{trans('msg.qayta')}}</h4>
                            <p>

                                {{trans('msg.ikki')}}</p>
                        </a>

                    </div>
                </div>
            </div>







        </div>

    </div>

</section>

{{--<section class="use_sasu ">
    <div class="container">

        <div class="row justify-content-center">

            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="single_feature">
                    <div class="single_feature_part"  >

                        <h2  style="color: #000000; text-align: center;">{{trans('msg.info')}}</h2>
                        <p>{{trans('msg.moj')}}</p>
                        <div class="buttun-group-area mt-10 col-md-12" style="border-redius: 10px;">
                            <div class="col-md-12 text-center">
                                <a  href="{{route('informatsionWaste')}}" class="btn_2 banner_btn_1">{{trans('msg.tib')}}</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>--}}

<br><br><br><br><br>
<section class="use_sasu ">
    <div class="container">

        <div class="row justify-content-center">

            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="single_feature">
                    <div class="single_feature_part"  >

                        <h2  style="color: #000000; text-align: center;">{{trans('msg.stra')}}</h2>
                        <p>{{trans('msg.qatiy')}}</p>
                        <div class="buttun-group-area mt-10 col-md-12" style="border-redius: 10px;">
                            <div class="col-md-12 text-center">
                                <a target="_blank" href="{{ asset('assets') }}/docs/strategy.pdf" class="btn_2 banner_btn_1">{{trans('msg.go_website')}}</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- map part start -->
<div class="whole-wrap">
    <div class="container box_1170">
        <div class="section-top-border">

            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <div class="col-lg-12 ">
                        <div class="col-lg-12">
                            <div id="demo" class="carousel slide col-md-12" data-ride="carousel">
                                <ul class="carousel-indicators">
                                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                                    <li data-target="#demo" data-slide-to="1"></li>
                                    <li data-target="#demo" data-slide-to="2"></li>
                                </ul>
                                <div class="carousel-inner">

                                            <div class="carousel-item active action_map">
                                                <img src="{{ asset('assets') }}/img/blog/uz-07.png" class="img_sixza" alt="Los Map" >
                                                <div class="carousel-caption" style="position: absolute; padding-bottom: 300px;">
                                                    <h2 style="color: #fff; position: absolute;"></h2><br><br>
                                                    <h5 style="color: #fff; position: absolute;"></h5>
                                                </div>
                                            </div>
                                    <div class="carousel-item action_map">
                                        <img src="{{ asset('assets') }}/img/blog/map.jpg"  class="img_sixza"  alt="Los Map">
                                        <div class="carousel-caption" style="position: absolute; padding-bottom: 300px;">
                                            <h2 style="color: #fff; position: absolute;"></h2><br><br>
                                            <h5 style="color: #fff; position: absolute;"></h5>
                                        </div>
                                    </div>


                                </div>
                                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#demo" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                </a>
                            </div>
                        </div>
                    </div>



                </div>
                <div class="col-md-4 col-sm-12 mt-sm-20" style="">
                    <section class="use_sasu padding_top" style="padding-top: 0px;">
                        <div class="container">
                            <div class="row">
                                <div class="single_feature">
                                        <div class="col-md-12 col-sm-12" style="height: 130px;">
                                            <a href="{{route('map_tbo')}}">
                                                <div class="single_feature">
                                                    <div class="single_feature_part">
                                                        <img src="{{ asset('assets') }}/img/icon/tbo.jpg" alt="" style="float: left; width: 45px; height: 45px;">
                                                        <h4 style=" margin-bottom: 3px;">{{trans('msg.tpo')}}</h4>
                                                        <p style="font-size: 12px; ">{{trans('msg.one')}}</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-12 col-sm-12" style="height: 130px;">
                                            <a href="{{route('map_msp')}}">
                                                <div class="single_feature">
                                                    <div class="single_feature_part">
                                                        <img src="{{ asset('assets') }}/img/icon/msp.png" alt="" style="float: left; width: 45px; height: 45px;">
                                                        <h4  style=" margin-bottom: 3px;">{{trans('msg.eng')}}</h4>
                                                        <p  style="font-size: 12px; ">{{trans('msg.two')}}</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-12 col-sm-12" style="height: 130px;">
                                            <a href="{{route('waste')}}">
                                                <div class="single_feature">
                                                    <div class="single_feature_part">
                                                        <img src="{{ asset('assets') }}/img/icon/images01.png" alt="" style="float: left; width: 45px; height: 45px;">
                                                        <h4  style=" margin-bottom: 3px;">{{trans('msg.pre')}} </h4>

                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <div class="col-md-12 col-sm-12" style="height: 130px;">
                                        <a href="{{route('points')}}">
                                            <div class="single_feature">
                                                <div class="single_feature_part">
                                                    <img src="{{ asset('assets') }}/img/icon/icon_445.png" alt="" style="float: left; width: 45px; height: 45px;">
                                                    <h4  style=" margin-bottom: 3px;">{{trans('msg.yaqin')}} </h4>

                                                </div>
                                            </div>
                                        </a>
                                    </div>


                                </div>
                            </div>

                        </div>

                    </section>
                </div>
            </div>
        </div>
    </div>

</div>

<br><br><br><br>

<div class="container">
    <div class="row">

        <div style=" width: 100%; height: 650px; position: relative; background-color: #F4F4F4;">

            <div style=" width: 100%; height: 100px; position: relative; padding: 15px;">
                <div style="position: relative;">
                    <ul >
                        <li style="float: left; width: 250px; ">
                            <h3 style="margin: 0px; padding-top: 6px;">WASTE.UZ <span style="background-color: #0afff4; color: green;padding: 3px;">MEDIA</span></h3>
                        </li>
                        <li style="float: left; ">
                            <button class="ulVideo" id="demoVideo" style="border: none; width: 100px; text-align: center; padding: 10px 10px; margin:0px; margin-right: 10px;" >{{trans('msg.vid')}}</button>
                        </li>
                        <li  style="float: left;">
                            <button class="ulVideo" id="demoPhoto" style="border: none; width: 100px; text-align: center; padding: 10px 10px; margin:0px;" >{{trans('msg.ph')}}</button>
                        </li>
                    </ul>
                </div>
            </div>






            <div id="demo" class="carousel slide demoVideo" data-ride="carousel" >

                <!-- Indicators -->
                <ul class="carousel-indicators">

                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    @foreach($gallerys as $key=> $item)
                        @if($item->type == 2)
                            @if($key==1)
                    <div class="carousel-item active">

                            <video width="100%" height="550px" controls>
                                <source src="{{$item->url}}" type="video/mp4">
                                <source src="mov_bbb.ogg" type="video/ogg">
                            </video>

                    </div>
                                @else
                                <div class="carousel-item ">

                                    <video width="100%"  height="550px" controls>
                                        <source src="{{$item->url}}" type="video/mp4">
                                        <source src="mov_bbb.ogg" type="video/ogg">
                                    </video>

                                </div>

                    @endif   @endif
                 @endforeach


                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </div>
            <div id="demo" class="carousel slide demoPhoto" data-ride="carousel" >

                <!-- Indicators -->
                <ul class="carousel-indicators">

                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    @foreach($gallerys as $key=> $item)
                        @if($item->type == 1)
                            @if($key==0)
                                <div class="carousel-item active">
                                    <img src="{{$item->url}}" alt="Los Angeles" width="100%" height="550px">


                                </div>
                            @else
                                <div class="carousel-item ">

                                    <img src="{{$item->url}}" alt="Los Angeles" width="100%" height="550px">

                                </div>

                            @endif   @endif
                    @endforeach


                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </div>

      {{--      <div class="slide-one-item home-slider owl-carousel demoPhoto">
                @foreach($gallerys as $item)
                    @if($item->type == 1)
                <div class="site-blocks-cover overlay" style="background-image: url({{$item->url}});" data-aos="fade" data-stellar-background-ratio="0.5">
                    <div class="container">
                        <div class="row align-items-center justify-content-center text-center">

                            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">


                                <h1 class="text-white font-weight-light">Туризм в Узбекистане</h1>
                                <p class="mb-5"></p>


                            </div>
                        </div>
                    </div>
                </div>

                @endif
            @endforeach

            </div>--}}


        </div>




        {{-- <div class="embed-responsive embed-responsive-16by9">
             <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/v64KOxKVLVg" allowfullscreen></iframe>
         </div>--}}

    </div>
</div>


<div class="container box_1170">
    <div class="section-top-border">
        <h3 class="mb-30">{{trans('msg.ariz')}}</h3>
        <div class="row">

            @foreach($equipments as $equipment)
            {{--    <h1>{{$equipment->id}}</h1>--}}
            <div class="col-lg-12">
                <blockquote class="generic-blockquote">
                    <div class="row">
                        <div class="col-lg-3">
                            <i style="color: green;">{{$equipment->shop_name}}/ {{$equipment->as_app_type}}</i><br>
                            <i>15:52, 02.07.2019</i><br><br>

                        </div>
                        <div class="col-lg-9 set_id_a">
                            <a href="{{route('chat')}}"  id="{{$equipment->id}}">
                                <p style="margin-bottom: 0px;">{{trans('msg.Equipment')}}- {{$equipment->monufacture_model}}</p>
                                <p>№1234 - <b>{{$equipment->message}}</b></p>
                            </a>
                        </div>
                    </div>
                </blockquote>
            </div>
            @endforeach
              {{-- <script type="text/javascript">
                    $(document).ready(function () {
                       var a_id = "";
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $('.set_id_a').click( function () {

                            a_id = $(this).attr('id');
                            $.ajax({
                                type: "get",
                                url: "chat/" + a_id,
                                date: "",
                                cache: false,
                                success: function (get_data) {
                                }
                            })
                        });
                    });
                </script>--}}

            @foreach($special_machineries as $special_machiner)

            <div class="col-lg-12">
              {{--  <h1>{{$special_machiner->id}}</h1>--}}
                <blockquote class="generic-blockquote">
                    <div class="row">
                        <div class="col-lg-3">
                            <i style="color: green;">{{$special_machiner->spec_shop_name}}/ {{$special_machiner->spec_app_type}}</i><br>
                            <i>15:52, 02.07.2019</i><br><br>

                        </div>
                        <div class="col-lg-9">
                            <a href="{{route('chat')}}">
                                <p style="margin-bottom: 0px;">{{trans('msg.mx')}} - {{$special_machiner->ts_group}}</p>
                                <p>№1588 - <b>{{$special_machiner->message}}</b></p>
                            </a>
                        </div>
                    </div>
                </blockquote>
            </div>
            @endforeach

            @foreach($recyclable_materials as $recyclable_material)
            <div class="col-lg-12">
               {{-- <h1>{{$recyclable_material->id}}</h1>--}}
                <blockquote class="generic-blockquote">
                    <div class="row">
                        <div class="col-lg-3">
                            <i style="color: green;">{{$recyclable_material->rec_shop_name}}/ {{$recyclable_material->rec_app_type}}</i><br>
                            <i>15:52, 02.07.2019</i><br><br>
                        </div>
                        <div class="col-lg-9">
                            <a href="{{route('chat')}}">
                                <p style="margin-bottom: 0px;">{{trans('msg.qt')}} - {{$recyclable_material->category}}</p>
                                <p>№1345 - <b>{{$recyclable_material->message}}</b></p>
                            </a>
                        </div>
                    </div>
                </blockquote>
            </div>
           @endforeach



        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog" style="z-index: 9999999999999">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
                <h4 class="modal-title">Спецтехника</h4>
            </div>
            <div class="modal-body">
                <ul>
                    <li><a href="http://man.uz/ru/">Ман.уз</a></li>
                    <li><a href="http://krantas.uz/">Крантас.уз</a></li>
                    <li><a href="http://www.uzxcmg.uz/">UzXCMG.уз</a></li>
                    <li><a href="http://howo.uz/">HOWO.UZ</a></li>
                    <li><a href="http://www.samauto.uz/uz/">Самаркандский автомобильный завод</a></li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>









