
<!-- Start Align Area -->
<div class="whole-wrap" style="padding-top: 100px">
    <div class="container box_1170">
        <div class="section-top-border">
            <h3 class="mb-30"></h3>
            <div class="row">
                <div class="col-md-3 ">
                    <div class="text-center">
                        <img class="" src="{{ asset('assets') }}/img/man.png" width="60%" alt="" class="img-fluid">
                    </div>
                    <br><br>
                    {{--@foreach($dataUser as $data_user)
                        @if($data_user->email == $email)
                            <h5 class="text-center">{{$data_user->first_name}} {{$data_user->last_name}}</h5>
                        @endif
                    @endforeach--}}
                    @foreach($users as $user)
                        @if($user->id == Auth::id())
                            <h5 class="text-center">{{$user->name}} {{$user->lastname}}</h5>
                        @endif
                    @endforeach

                    <div class="">

                       {{-- <ul class="list-unstyled">
                            @foreach($dataUser as $data_user)
                                @if($data_user->email == $email)
                            <li><a href="#" class="single_social_icon"><i class="fas fa-globe"> {{$data_user->area}} </i></a></li>
                            <li><a href="#" class="single_social_icon"><i class="fas fa-envelope"> {{$data_user->email}}</i> </a></li>
                                @endif
                            @endforeach
                        </ul>--}}
                        <ul class="list-unstyled">
                            @foreach($users as $user)
                                @if($user->id == Auth::id())

                                    <li><a href="#" class="single_social_icon"><i class="fas fa-envelope"> {{$user->email}}</i> </a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>


                </div>
                <div class="col-md-9 mt-sm-20">
                    <div class="progress-table-wrap">
                        <div class="section-top-border" style="padding-top: 10px;">
                            <div class="row">
                               {{-- @dd($dataUser)--}}
                                @foreach($dataUser as $data_user)
                                    @if($data_user->email == Auth::id() )
                                        @foreach($dataAnswer as $dataAns)
                                        <div class="col-md-12 section-top-border text-right" style="border: 2px solid; padding-top: 15px;margin-bottom: 15px;">
                                            <h3 class="mb-30 text-left">Ответ</h3>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-right">{{$dataAns->description}}</p>

                                                </div>
                                                <div class="col-md-6">
                                                    <img src="{{ asset('assets') }}{{$dataAns->media}}" alt="" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                                @endforeach

                                <div class="col-lg-8 col-md-8">
                                    <h3 class="mb-30">{{trans('msg.un')}}</h3>
                                    {!! Form::open(['url' => 'cabinet/application', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}

                                        {{csrf_field()}}
                                        <div class="mt-10">
                                            <input type="text" name="first_name" placeholder="{{trans('msg.ism')}}"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.ism')}}'" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="last_name" placeholder="{{trans('msg.familya')}}"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.familya')}}'" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="email" placeholder="{{trans('msg.email2')}}"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.email2')}}'" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <input type="phone " name="phone" placeholder="{{trans('msg.tel')}}"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.tel')}}'" required
                                                   class="single-input">
                                        </div>

                                        <div class="mt-10">
                                            <input type="text" name="passport_number" placeholder="{{trans('msg.pass')}}"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.pass')}}'" required
                                                   class="single-input">
                                        </div>


                                        <div class="input-group-icon mt-10">
                                            <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                                            <div class="form-select" id="default-select_1">
                                                    <select name="region_id" id="region_id"  onchange="myFunction()">
                                                    @foreach($regions as $region)
                                                        <option  value="{{$region->areas_id}}">{{$region->region_name_ru}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="input-group-icon mt-10">
                                            {{--<div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>--}}
                                            <div class="form-select append-select" id="default-select">
                                            </div>
                                        </div>


                                        <div class="input-group-icon mt-10">
                                            <div class="icon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
                                            <input type="text" name="address" placeholder="{{trans('msg.add')}}" onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Address'" required class="single-input">
                                        </div>

                                        <div class="row">
                                            <div class="input-group">
                                                <div class="col-md-6">
                                                    <div class="input-group-icon mt-10">
                                                        <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                                                        <input type="file" name="media" placeholder="{{trans('msg.img')}}" onfocus="this.placeholder = ''"
                                                               onblur="this.placeholder = 'Media'" required class="single-input">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="input-group-icon mt-10">
                                                        <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                                                        <input type="date" name="date" placeholder="{{trans('msg.img')}}" onfocus="this.placeholder = ''"
                                                               onblur="this.placeholder = 'Media'" required class="single-input">
                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                        <div class="mt-10">
							<textarea class="single-textarea" name="message" placeholder="{{trans('msg.smss')}}" onfocus="this.placeholder = ''"
                                      onblur="this.placeholder = 'Message'" required></textarea>
                                        </div>
                                        <div class="button-group-area">
                                            <input class="genric-btn danger" type="submit" value="{{trans('msg.sms')}}">

                                        </div>

                                    {!! Form::close() !!}
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

</div>

<script>
    function myFunction(){

        $.get("{{ URL::to('ajaxRequest') }}", function (data) {
            var reg = document.getElementById('region_id').value;
            var element = '<select id="value-select" name="area" class="form-control">';
            element += '<option selected disabled>...</option>';

            for(var i=0; i<data.length; i++) {

                if (data[i].region_status == reg)
                {
                    element += '<option value="'+ data[i].area_name_ru + '">' + data[i].area_name_ru + '</option>';
                }
            }

            element += '</select>';
            $('.append-select').append(element);
        })


    }
</script>
