
<!-- ================ contact section start ================= -->

<br><br><br>
<section class="contact-section section_padding">
    <div class="container">


        <div class="row">
            @foreach($informations as $information)
            <div class="col-md-4" style=" padding: 20px;" >
                <div style="width: 100%; height: 100%; " class="text-center">
                    <a target="_blank" href="{{$information->informations}}">
                        <img src="{{ asset('assets') }}/img/icon/archive.png" alt="" width="80px">
                        @if(session()->get('locale') == "ru" or session()->get('locale') == "")
                        <h6>{{$information->text_ru}}</h6>
                        @elseif(session()->get('locale') == "uz")
                            <h6>{{$information->text_uz}}</h6>
                        @endif


                    </a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->

