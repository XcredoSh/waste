

<br><br><br><br><br><br><br><br>


<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <h5 class="mb-30">
                {{trans('msg.news')}}</h5>

        @foreach($news as $new)
                <div class="row">

                    <div class="col-md-12 mt-sm-20">
                        @if(session()->get('locale') == "ru" or session()->get('locale') == "")
                        <p style="    font-size: 19px;
    line-height: 1.63; margin-bottom: 2px; padding-top: 50px;">{{$new->news_name_ru}}</p><hr>
                        @elseif(session()->get('locale') == "uz")
                            <p style="    font-size: 19px;
    line-height: 1.63; margin-bottom: 2px; padding-top: 50px;">{{$new->news_name_uz}}</p><hr>
                        @endif
                        <img src="{{$new->img}}" width="640px" alt=""><br>
                            <b style="font-size: 16px;">{{$new->created}}</b> <i class="fa fa-eye" style="padding-left: 10px;"></i>

                    </div>
                    {{--<div class="col-md-3">
                        <img src="{{ asset('assets') }}/{{$new->img}}" width="" alt="">
                        <b>12:23/12.05.2019 {{ date('Y') }}</b>
                    </div>--}}
                </div>
            @endforeach
        </div>

        <div class="col-lg-4">
            <h5 class="mb-30">
               </h5>

            @foreach($news as $new)
                <div class="row" style="border: 1px solid #ccc; padding: 5px; margin-bottom: 10px;">
                    <div class="col-md-5">
                        <img src="{{$new->img}}"  alt="">
                        <b style="font-size: 12px;">{{$new->created}}/{{ date('Y') }}</b>
                    </div>
                    <div class="col-md-7 mt-sm-20">
                        @if(session()->get('locale') == "ru" or session()->get('locale') == "")
                        <p style="line-height: 1.1;">{{$new->news_name_ru}}</p>
                        @elseif(session()->get('locale') == "uz")
                            <p style="line-height: 1.1;">{{$new->news_name_uz}}</p>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>

    </div>



</div>
</div>

