
<section class="contact-section section_padding ">
    <div class="container">


        <div class="col-md-2" style="float: right; position: relative">
            <h3>{{trans('msg.za')}}</h3>
            <section class="use_sasu ">
                <div class="container">
                    <div class="row col-md-12" style="padding-top: 30px">
                        <ul>
                            <li><a href="http://man.uz/ru/"><b>MAN.UZ</b></a></li>
                            <li><a href="http://krantas.uz/"><b>KRANTAS.UZ</b></a></li>
                            <li><a href="http://www.uzxcmg.uz/"><b>UzXCMG.UZ</b></a></li>
                            <li><a href="http://howo.uz/"><b>HOWO.UZ</b></a></li>
                            <li><a href="http://www.samauto.uz/uz/"><b>Самаркандский автомобильный завод</b></a></li>

                        </ul>
                    </div>
                </div>

            </section>
        </div>
        <div class="row">

            <form method="post" action="{{route('special_save')}}">
            @csrf
            <section class="use_sasu">
                <div class="container">
                    <div class="row col-md-12">
                        <div class="col-lg-9 col-md-9">
                            <div class="section_tittle ">
                                <h3>{{trans('msg.spec')}}
                                </h3>
                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-12 " >

                                    <p style="float: left">{{trans('msg.type')}}</p>
                                    <table class="table" >
                                        <tr>
                                            <td  class="active block_items" style="border: none;">
                                                <label for="kino1"><span>{{trans('msg.sotish')}}</span></label>
                                                <input type="radio" name="app_type" id="kino1" value="1">

                                            </td>
                                            <td  class="active block_items" style="border: none;">
                                                <label for="kino1"><span>{{trans('msg.olish')}}</span></label>
                                                <input type="radio" name="app_type" id="kino1" value="2">

                                            </td>
                                            <td  class="active block_items" style="border: none;">
                                                <label for="kino1"><span>{{trans('msg.iun')}}</span></label>
                                                <input type="radio" name="app_type" id="kino1" value="3">

                                            </td>
                                            <td  class="active block_items" style="border: none;">
                                                <label for="kino1"><span>{{trans('msg.iolish')}}</span></label>
                                                <input type="radio" name="app_type" id="kino1" value="4">

                                            </td>

                                        </tr>

                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <section class="use_sasu col-md-12">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="section_tittle ">

                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-12 " >
                                    <p style="float: left;">{{trans('msg.ariza_turi')}}</p><br>

                                    <div class="input-group-icon mt-10  col-md-12">

                                        <div class="form-select" id="default-select_1">
                                            <label style="float: right; position: relative" for="select_item">{{trans('msg.tc')}}</label>
                                            <select name="ts_group" id="select_item" tabindex="-1" aria-hidden="" onchange="">
                                                <option  value="0" data-name="Не указано">Не указано</option>

                                                <optgroup label="Конвейеры">
                                                    <option  value="Ленточные" data-name="Ленточные">Ленточные</option>
                                                    <option  value="Цепные" data-name="Цепные">Цепные</option>
                                                    <option  value="Пластинчатые" data-name="Пластинчатые">Пластинчатые</option>
                                                </optgroup>

                                                <optgroup label="Прессовое оборудование">
                                                    <option  value="5" data-name="Ленточные">Горизонтальные пресса</option>
                                                    <option  value="6" data-name="Цепные">Вертикальные пресса</option>
                                                    <option  value="7" data-name="Пластинчатые">Пресс-компакторы</option>
                                                    <option  value="8" data-name="Пластинчатые">Пресс-компакторы</option>
                                                    <option  value="9" data-name="Пластинчатые">Специальные прессы</option>
                                                    <option  value="10" data-name="Пластинчатые">Пресс-ножницы для металла</option>
                                                </optgroup>


                                            </select>
                                        </div>
                                    </div><br><br>

                                    <div class="input-group-icon mt-10  col-md-12">

                                        <div class="input-group-icon" style="margin-top: 0px;">
                                            <label style="float: right; position: relative" for="select_item">{{trans('msg.oz')}}</label>
                                            <input type="text" name="modification" placeholder="{{trans('msg.oz')}}"
                                                   required class="single-input">
                                        </div>
                                    </div>
                                    <br><br>

                                    <div class="mt-10 col-md-12">
                                        <label style="float: right; position: relative" for="select_item">{{trans('msg.tav')}}</label>
                                        <textarea class="single-textarea" name="message" placeholder="{{trans('msg.tav')}}" onfocus="this.placeholder = ''"
                                                  onblur="this.placeholder = '{{trans('msg.tav')}}'" required>
                                         </textarea>
                                    </div>
                                    <br><br>
                                    <div class="input-group-icon mt-10  col-md-12">




                                        <div class="input-group">

                                            <div class="col-md-6">
                                                <div class="form-select" id="default-select_1 col-md-6">
                                                    <label style="float: left; position: relative" for="select_item">{{trans('msg.yil')}}:</label>
                                                    <select name="year_of_issue" id="select_item"  onchange="">
                                                        <option  value="2019">2019</option>
                                                        <option  value="2019">2018</option>
                                                        <option  value="2019">2017</option>
                                                        <option  value="2019">2016</option>

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="input-group-icon mt-10" style="margin-top: 0px;">
                                                    <label style="float: left; position: relative" for="select_item">{{trans('msg.km')}}:</label>
                                                    <input type="text" name="mileage" placeholder="{{trans('msg.km')}}:"
                                                           required class="single-input">

                                                </div>
                                            </div>

                                        </div><br><br>


                                        <div class="input-group">

                                            <div class="col-md-6">
                                                <div class="form-select" id="default-select_1 col-md-6">
                                                    <label style="float: left; position: relative" for="select_item">VIN:</label>
                                                    <input type="text" name="vin" placeholder="VIN:"
                                                           required class="single-input">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <div class="col-md-6">
                                                        <div class="input-group-icon " style="margin-top: 0px;">
                                                            <label style="float: left; position: relative" for="select_item">ПТС:</label>
                                                            <select name="ptc" id="select_item"  onchange="">
                                                                <option  value="Да">{{trans('msg.ha')}}</option>
                                                                <option  value="Нет">{{trans('msg.yoq')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="input-group-icon" style="margin-top: 0px;">
                                                            <label style="float: left; position: relative" for="select_item">{{trans('msg.erigan')}}:</label>
                                                            <select name="cleared" id="select_item"  onchange="">
                                                                <option  value="Да">{{trans('msg.ha')}}</option>
                                                                <option  value="Нет">{{trans('msg.yoq')}}</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                        </div><br><br>

                                        <div class="input-group">

                                            <div class="col-md-6">
                                                <div class="form-select" id="default-select_1 col-md-6">
                                                    <label style="float: left; position: relative" for="select_item">{{trans('msg.holati')}}:</label>
                                                    <select name="condition" id="select_item"  onchange="">
                                                        <option  value="Удовлетворительное">Удовлетворительное</option>
                                                        <option  value="Хорошее">Хорошее</option>
                                                        <option  value="Отличное">Отличное</option>
                                                        <option  value="Новый">Новый</option>

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="input-group-icon mt-10" style="margin-top: 0px;">
                                                    <label style="float: left; position: relative" for="select_item">{{trans('msg.egalar')}}:</label>
                                                    <select name="number_of_owners" id="select_item"  onchange="">
                                                        <option  value="1">1</option>
                                                        <option  value="2">2</option>
                                                        <option  value="2 и более">2 и более</option>
                                                    </select>

                                                </div>
                                            </div>

                                        </div><br><br>
                                        <div class="input-group-icon mt-10  col-md-12">
                                            <label style="float: left; position: relative" for="select_item">{{trans('msg.narx')}}</label>
                                            <div class="input-group">
                                                <div class="col-md-6">
                                                    <div class="input-group-icon mt-10">
                                                        <div class="icon"><i class="" aria-hidden="true"></i></div>
                                                        <input type="text" name="price" placeholder="СЎМ"
                                                               required class="single-input">

                                                    </div>
                                                </div>


                                            </div>

                                        </div>




                                    </div>
                                    <br><br>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </section>

            <section class="use_sasu col-md-12 ">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="section_tittle ">

                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-12 " >

                                    <p style="float: left">{{trans('msg.joy')}}</p><br>
                                    <div id="map2" style="width: 745px; height: 400px"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>


            <br><br>

            <div class="input-group-icon mt-10  col-md-12">
                <label style="float: left; position: relative" for="select_item">{{trans('msg.aloq')}}</label>
                <div class="input-group">
                    <div class="col-md-6">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-users" aria-hidden="true"></i></div>
                            <input type="text" name="face" placeholder="{{trans('msg.shax')}}:"
                                   required class="single-input">

                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <div class="col-md-6">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                            <input type="text" name="tel" placeholder="Телефон:
"
                                   required class="single-input">

                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <div class="col-md-6">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                            <input type="text" name="email" placeholder="E-mail:"
                                   required class="single-input">

                        </div>
                    </div>
                </div>

            </div>

            <br>
            <div class="input-group-icon mt-10  col-md-12">
                <input class="genric-btn danger" type="submit" value="{{trans('msg.yub')}}">

            </div>
            </form>

        </div>

    </div>
</section>
<!-- ================ contact section end ================= -->
