<div class="whole-wrap" style="padding-top: 100px">
    <div class="container box_1170">
        <div class="section-top-border">
            <h3 class="mb-30"></h3>
            <div class="row">
                <div class="col-md-3 ">
                  {{--  <div class="text-center">
                        <img class="" src="{{ asset('assets') }}/img/man.png" width="60%" alt="" class="img-fluid">
                    </div>--}}
                    <br><br>

                </div>
                <div class="col-md-9 mt-sm-20">
                    <div class="progress-table-wrap">
                        <div class="section-top-border" style="padding-top: 10px;">
                            <div class="row">
                                {{--@foreach($dataUser as $data_user)
                                    @if($data_user->email == $email)
                                        @foreach($dataAnswer as $dataAns)
                                            <div class="col-md-12 section-top-border text-right" style="border: 2px solid; padding-top: 15px;margin-bottom: 15px;">
                                                <h3 class="mb-30 text-left">Ответ</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="text-right">{{$dataAns->description}}</p>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <img src="{{ asset('assets') }}{{$dataAns->media}}" alt="" class="img-fluid">
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                @endforeach--}}

                                <div class="col-lg-8 col-md-8">


                                    <h3 class="mb-30">{{trans('msg.rasmiy')}}
                                    </h3>

                                        {!! Form::open(['url' => 'hot-line', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                                        {{csrf_field()}}
                                        <div class="mt-10">
                                            <input type="text" name="first_name" placeholder="{{trans('msg.ism')}}"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.ism')}}'" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="last_name" placeholder="{{trans('msg.familya')}}"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.familya')}}'" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="email" placeholder="{{trans('msg.email2')}}"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.email2')}}'" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <input type="phone " name="phone" placeholder="{{trans('msg.tel')}}"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.tel')}}'" required
                                                   class="single-input">
                                        </div>

                                        <div class="mt-10">
                                            <input type="text" name="passport_number" placeholder="{{trans('msg.pass')}}"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.pass')}}'" required
                                                   class="single-input">
                                        </div>


                                        <div class="input-group-icon mt-10">
                                            <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                                            <div class="form-select" id="default-select_1">
                                                <select name="region_id" id="region_id"  onchange="myFunction()">
                                                   @foreach($regions as $region)
                                                        <option  value="{{$region->areas_id}}">{{$region->region_name_ru}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="input-group-icon mt-10">
                                            {{--<div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>--}}
                                            <div class="form-select append-select" id="default-select">
                                            </div>
                                        </div>


                                        <div class="input-group-icon mt-10">
                                            <div class="icon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
                                            <input type="text" name="address" placeholder="{{trans('msg.add')}}" onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Адрес'" required class="single-input">
                                        </div><br><br>

                                        <div class="row">
                                            <div class="input-group">
                                                <div class="col-md-6">
                                                    <h6>Добавить картинку</h6>
                                                    <div class="input-group-icon mt-10">

                                                        <lable class="custom-file-label">{{trans('msg.plusimg')}}</lable>
                                                        <input type="file" name="media" placeholder="{{trans('msg.plusimg')}}" onfocus="this.placeholder = ''"
                                                               onblur="this.placeholder = '{{trans('msg.plusimg')}}'" required class="single-input custom-file-input">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <h6 style="color: #FFFFFF;">Добавить</h6>
                                                    <div class="input-group-icon mt-10">
                                                        <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                                                       {{-- <lable class="custom-file-label">Добавить картинку</lable>--}}
                                                        <input type="date" name="date" placeholder="" onfocus="this.placeholder = ''"
                                                               onblur="this.placeholder = ''" required class="single-input">
                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                        <div class="mt-10">
							<textarea class="single-textarea" name="message" placeholder="{{trans('msg.xabar')}}" onfocus="this.placeholder = ''"
                                      onblur="this.placeholder = '{{trans('msg.xabar')}}'" required></textarea>
                                        </div>
                                        <div class="button-group-area">
                                            <input class="genric-btn danger" type="submit" value="{{trans('msg.sms')}}">

                                        </div>

                                    {!! Form::close() !!}
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

</div>

<script>
    function myFunction(){

        $.get("{{ URL::to('ajaxRequest') }}", function (data) {
            var reg = document.getElementById('region_id').value;
            var element = '<select id="value-select" name="area" class="form-control">';
            element += '<option selected disabled>...</option>';

            for(var i=0; i<data.length; i++) {

                if (data[i].region_status == reg)
                {
                    element += '<option value="'+ data[i].area_name_ru + '">' + data[i].area_name_ru + '</option>';
                }
            }

            element += '</select>';
            $('.append-select').append(element);
        })


    }
</script>
