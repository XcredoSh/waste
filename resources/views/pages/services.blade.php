<section class=" section_padding padding_top" id="" style="padding-bottom: 10px">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-xl-8">
                <div class="cta_text text-center">
                    <h2>{{trans('msg.services')}}</h2>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Start Sample Area -->

<!-- End Sample Area -->

<!-- Start Button -->

<!-- End Button -->

<!-- Start Align Area -->
<div class="whole-wrap">
    <div class="container box_1170">
        <div class="section-top-border">
            <h3 class="mb-30">{{trans('msg.codes')}}</h3>
            <div class="progress-table-wrap">
                <div class="progress-table">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>{{trans('msg.nomi')}}</th>
                            <th>{{trans('msg.roy')}}</th>
                            <th>{{trans('msg.data')}}</th>
                            <th>Линк</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($documents as $key=>$document)
                            @if($document->document_types_id == 1)
                                <tr>
                                    <td style="padding-bottom: 0px;">{{$key+1}}</td>
                                    <td style="padding-bottom: 0px;">{{$document->name}}</td>
                                    <td style="padding-bottom: 0px;">{{$document->number }}</td>
                                    <td style="padding-bottom: 0px;">{{$document->date}}</td>
                                    <td style="padding-bottom: 0px; padding-top: 0px;">
                                        @if($document->url == "0")
                                            <div class="button-group-area">
                                                <a href="{{$document->file}}" download style="margin-top: 5px;"  class="genric-btn success">Download</a>
                                            </div>
                                        @elseif($document->file == "0" and $document->is_locale == "1")
                                            <div class="">
                                                <a href="{{$document->url}}"  style="margin-top: 5px;"  class="">{{$document->url}}</a>
                                            </div>
                                        @endif

                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>



    </div>
</div>



<div class="whole-wrap">
    <div class="container box_1170">
        <div class="section-top-border">
            <h3 class="mb-30">{{trans('msg.zakon')}}</h3>
            <div class="progress-table-wrap">
                <div class="progress-table">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>{{trans('msg.nomi')}}</th>
                            <th>{{trans('msg.roy')}}</th>
                            <th>{{trans('msg.data')}}</th>
                            <th>Линк</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($documents as $key=>$document)
                            @if($document->document_types_id == 2)
                                <tr>
                                    <td style="padding-bottom: 0px;">{{$key+1}}</td>
                                    <td style="padding-bottom: 0px;">{{$document->name}}</td>
                                    <td style="padding-bottom: 0px;">{{$document->number }}</td>
                                    <td style="padding-bottom: 0px;">{{$document->date}}</td>
                                    <td style="padding-bottom: 0px; padding-top: 0px;">
                                        @if($document->url == "0")
                                            <div class="button-group-area">
                                                <a href="{{$document->file}}" download style="margin-top: 5px;"  class="genric-btn success">Download</a>
                                            </div>
                                        @elseif($document->file == "0" or $document->is_locale == "1")
                                            <div class="">
                                                <a href="{{$document->url}}"  style="margin-top: 5px;"  class="">{{$document->url}}</a>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>



    </div>
</div>


<div class="whole-wrap">
    <div class="container box_1170">
        <div class="section-top-border">
            <h3 class="mb-30">{{trans('msg.rash')}}</h3>
            <div class="progress-table-wrap">
                <div class="progress-table">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>{{trans('msg.nomi')}}</th>
                            <th>{{trans('msg.roy')}}</th>
                            <th>{{trans('msg.data')}}</th>
                            <th>Линк</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($documents as $key=>$document)

                            @if($document->document_types_id == 3)
                            <tr>
                                <td style="padding-bottom: 0px;">{{$key+1}}</td>
                                <td style="padding-bottom: 0px;">{{$document->name}}</td>
                                <td style="padding-bottom: 0px;">{{$document->number }}</td>
                                <td style="padding-bottom: 0px;">{{$document->date}}</td>
                                <td style="padding-bottom: 0px; padding-top: 0px;">
                                    @if($document->url == "0")
                                        <div class="button-group-area">
                                            <a href="{{$document->file}}" download style="margin-top: 5px;" class="genric-btn success">Download</a>
                                        </div>
                                    @elseif($document->file == "0" and $document->is_locale == "1")
                                        <div class="">
                                            <a href="{{$document->url}}" style="margin-top: 5px;" class="">{{$document->url}}</a>
                                        </div>
                                    @endif

                                </td>
                            </tr>
                            @endif
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>



    </div>
</div>

<div class="whole-wrap">
    <div class="container box_1170">
        <div class="section-top-border">
            <h3 class="mb-30">{{trans('msg.dru')}}</h3>
            <div class="progress-table-wrap">
                <div class="progress-table">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>{{trans('msg.nomi')}}</th>
                            <th>{{trans('msg.roy')}}</th>
                            <th>{{trans('msg.data')}}</th>
                            <th>Линк</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($documents as $key=>$document)

                            @if($document->document_types_id == 4)
                                <tr>
                                    <td style="padding-bottom: 0px;">{{$key+1}}</td>
                                    <td style="padding-bottom: 0px;">{{$document->name}}</td>
                                    <td style="padding-bottom: 0px;">{{$document->number }}</td>
                                    <td style="padding-bottom: 0px;">{{$document->date}}</td>
                                    <td style="padding-bottom: 0px; padding-top: 0px;">
                                        @if($document->url == "0")
                                            <div class="button-group-area">
                                                <a href="{{$document->file}}" download style="margin-top: 5px;"  class="genric-btn success">Download</a>
                                            </div>
                                        @elseif($document->file == "0" and $document->is_locale == "1")
                                            <div class="">
                                                <a href="{{$document->url}}"  style="margin-top: 5px;"  class="">{{$document->url}}</a>
                                            </div>
                                        @endif

                                    </td>
                                </tr>
                            @endif
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>



    </div>
</div>
