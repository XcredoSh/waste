<section class="contact-section section_padding">
    <div class="container">


        <div class="row">
            <div class="col-12">
                <h2 class="contact-title">{{trans('msg.my')}}</h2>
            </div>

            <div class="col-12">
                <div class="col-md-12">

                    <div style="padding: 1px;">
                        @foreach($opens as $item)
                            @if($item->about_us_id == 1)
                            @if($item->category_id == 5)
                                @if($item->text_photo != null)
                                    <img src="{{$item->photo}}" alt="" style="width: 798px; height: 500px"><br>
                                        @if(session()->get('locale') == "uz")
                                          <p style="width: 798px;">{{$item->text_photo}}</p>
                                        @elseif(session()->get('locale') == "ru" or session()->get('locale') == "")
                                            <p style="width: 798px;">{{$item->text_photo_ru}}</p>
                                        @endif
                                @endif
                            @endif
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="col-md-12">

                    <div style="padding: 1px;">
                        @foreach($opens as $item)
                            @if($item->about_us_id == 1)
                                @if($item->category_id == 1)
                                    @if(session()->get('locale') == "uz")
                                    <p style="width: 728px;">{{$item->text}}</p>
                                        <hr style="width: 680px; float: left">
                                    @elseif(session()->get('locale') == "ru" or session()->get('locale') == "")
                                        <p style="width: 728px;">{{$item->text_ru}}</p>
                                        <hr style="width: 680px; float: left">
                                    @endif

                                @endif
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="col-md-12">

                    <div style="padding: 1px;">
                        @foreach($opens as $item)
                            @if($item->about_us_id == 1)
                                @if($item->category_id == 2)
                                    @if(session()->get('locale') == "uz")
                                    <a target="_blank" href="{{$item->pdf}}"  style="width: 100%; padding: 2px; height: 30px; background-color: green; color: #FFFFFF">{{$item->pdf_name_uz}}</a>
                                    @elseif(session()->get('locale') == "ru" or session()->get('locale') == "")
                                        <a target="_blank" href="{{$item->pdf}}"  style="width: 100%; padding: 2px; height: 30px; background-color: green; color: #FFFFFF">{{$item->pdf_name_ru}}</a>
                                    @endif
                                @endif
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>


            <div class="col-12">
                <div class="col-md-12">

                    <div style="padding: 1px;">
                        @foreach($opens as $item)
                            @if($item->about_us_id == 1)
                                @if($item->category_id == 3)
                                    @if(session()->get('locale') == "uz")
                                    <a href="{{$item->link}}" target="_blank" style="width: 100%; padding: 2px; height: 30px; background-color: blue; color: #FFFFFF">{{$item->link_name_uz}}</a>
                                    @elseif(session()->get('locale') == "ru" or session()->get('locale') == "")
                                        <a href="{{$item->link}}" target="_blank" style="width: 100%; padding: 2px; height: 30px; background-color: blue; color: #FFFFFF">{{$item->link_name_ru}}</a>
                                    @endif
                                @endif
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>


            </div>
        </div>

</section>
