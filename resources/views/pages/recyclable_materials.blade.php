
<section class="contact-section section_padding ">
    <div class="container">


        <div class="row">


            <form  method="POST" action="{{route('recyclable_save')}}">
                @csrf
            <section class="use_sasu ">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="section_tittle ">
                                <h3>{{trans('msg.qay')}}</h3>
                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-9 " >

                                    <p style="float: left">{{trans('msg.type')}}</p>
                                    <table class="table" >
                                        <tr>

                                            <td  class="active block_items" style="border: none;  ">
                                                <label for="kino1"><span>{{trans('msg.sotish')}}</span></label>
                                                <input type="radio" name="app_type" id="kino1" value="1">

                                            </td>
                                            <td  class="active block_items" style="border: none; ">
                                                <label for="kino1"><span>{{trans('msg.olish')}}</span></label>
                                                <input type="radio" name="app_type" id="kino1" value="2">

                                            </td>


                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <section class="use_sasu col-md-12">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="section_tittle ">

                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-12 " >
                                    <p style="float: left;">{{trans('msg.wer')}}</p><br>

                                    <div class="input-group-icon mt-10  col-md-12">

                                        <div class="form-select" id="default-select_1">
                                            <label style="float: right; position: relative" for="select_item">{{trans('msg.tur')}}</label>
                                            <select name="category" id="select_item" tabindex="-1" aria-hidden="" onchange="">
                                                <option  value="0" data-name="Не указано">Не указано</option>

                                                <optgroup label="Конвейеры">
                                                    <option  value="Полимеры, пластмассы" data-name="Ленточные">Полимеры, пластмассы</option>
                                                    <option  value="Акрилонитрилбутадиенстирол (АБС пластик)" data-name="Цепные"> - Акрилонитрилбутадиенстирол (АБС пластик)</option>
                                                    <option  value="Полиамид (ПА)" data-name="Пластинчатые"> - Полиамид (ПА)</option>
                                                    <option  value="Поливинилхлорид (ПВХ" data-name="Пластинчатые"> - Поливинилхлорид (ПВХ)</option>
                                                    <option  value=" Поликарбонат (ПК)" data-name="Пластинчатые"> - Поликарбонат (ПК)</option>
                                                    <option  value="6" data-name="Пластинчатые"> - Полипропилен (ПП)</option>
                                                    <option  value="7" data-name="Пластинчатые"> - Полистирол (УПС,ПСС, ВПС)</option>
                                                    <option  value="8" data-name="Пластинчатые"> - Полиэтилен ВД (ПВД)</option>
                                                    <option  value="9" data-name="Пластинчатые"> - Полиэтилен НД (ПНД)</option>
                                                    <option  value="10" data-name="Пластинчатые"> - Полиэтилентерефталат (ПЭТ)</option>
                                                    <option  value="11" data-name="Пластинчатые"> - Полиуретан (ПУ)</option>
                                                    <option  value="12" data-name="Пластинчатые"> - Фторопласт</option>
                                                    <option  value="13" data-name="Пластинчатые">Цветной металлолом</option>
                                                    <option  value="14" data-name="Пластинчатые"> - Аккумуляторы (гель ,полипропилен)</option>
                                                    <option  value="15" data-name="Пластинчатые"> - Аккумуляторы (эбонит)</option>
                                                    <option  value="16" data-name="Пластинчатые"> - Свинец в оболочке</option>
                                                    <option  value="17" data-name="Пластинчатые"> - Свинец грузики</option>
                                                    <option  value="18" data-name="Пластинчатые"> - Свинец переплав</option>
                                                </optgroup>




                                            </select>
                                        </div>
                                    </div><br><br>
                                    <div class="single-element-widget mt-30 col-md-12">

                                        {{--<div class="switch-wrap d-flex justify-content-between">
                                            <div class="primary-checkbox">
                                                <input type="checkbox" id="default-checkbox">
                                                <label for="default-checkbox"></label>
                                            </div>
                                            <p style="position: absolute;  float: left; margin-left: 30px; margin-top: -5px;"><b>Добавить свою категорию</b></p>
                                        </div><br>--}}

                                       {{-- <div class="col-md-12">
                                            <div class="input-group-icon mt-10">
                                                <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                                                <input type="text" name="" placeholder="Новая категория"
                                                       required class="single-input">

                                            </div>
                                        </div>--}}




                                    </div>

                                    <br><br>

                                    <div class="mt-10 col-md-12">
                                        <label style="float: right; position: relative" for="select_item">{{trans('msg.tav')}}</label>
                                        <textarea class="single-textarea" name="message" placeholder="- агрегатное состояние;
- фракция, если дробленое или резаное;
- вес/объём единицы упаковки (россыпь, тюк, кипа, бочка);
- степень загрязнения;
- цвет." onfocus="this.placeholder = ''"
                                                  onblur="this.placeholder = 'Описание'" required>
                                         </textarea>
                                    </div>
                                    <br><br>
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <div class="col-md-4">
                                                <div class="input-group-icon " style="margin-top: 0px;">
                                                    <label style="float: left; position: relative" for="select_item">{{trans('msg.um')}}:</label>

                                                    <div class="item">
                                                        <div class="quantity">
                                                            <button class="plus-btn" type="button" name="button" style="margin-top: 10px;">
                                                               <b>+</b>
                                                            </button>
                                                            <input type="text" name="total_amount" value="0" style="background-color: green; color: #ffffff;">
                                                            <button class="minus-btn" type="button" name="button" style="margin-top: 10px;">
                                                              <b>-</b>
                                                            </button>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-4">

                                                <div class="input-group-icon" style="margin-top: 0px;">
                                                    <label style="float: left; position: relative" for="select_item">{{trans('msg.birlik')}}</label>
<br><br>
                                                    <div class="switch-wrap d-flex justify-content-between" style="height: 25px;">
                                                        <p>m <sup>3</sup> </p>
                                                        <div class="">
                                                            <input type="checkbox" name="unit_of_measuroment" >
                                                            <label for="default-radio"></label>
                                                        </div>
                                                    </div>
                                                    <div class="switch-wrap d-flex justify-content-between"  style="height: 25px;">
                                                        <p>Тонны</p>
                                                        <div class="">
                                                            <input type="checkbox" name="unit_of_measuroment" >
                                                            <label for="default-radio"></label>
                                                        </div>
                                                    </div>
                                                    <div class="switch-wrap d-flex justify-content-between"  style="height: 25px;">
                                                        <p>Штуки</p>
                                                        <div class="">
                                                            <input type="checkbox" name="unit_of_measuroment" >
                                                            <label for="default-radio"></label>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-4">

                                                <div class="input-group-icon" style="margin-top: 0px;">
                                                    <label style="float: left; position: relative" for="select_item">{{trans('msg.narx')}}</label>
                                                    <input type="text" name="price" placeholder="Сум"
                                                           required class="single-input">
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <br><br>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </section>



            <section class="use_sasu col-md-12 ">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="section_tittle ">

                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-12 " >

                                    <p style="float: left">{{trans('msg.joy')}}</p><br>
                                    <div id="map2" style="width: 745px; height: 400px"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <section class="use_sasu col-md-12 ">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="section_tittle ">

                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-12 " >

                                    <p style="float: left">{{trans('msg.qosh')}}Я</p>

                                    <div class="mt-10 col-md-12">
                                        <label style="float: right; position: relative" for="select_item">{{trans('msg.izox')}}:</label>
                                        <textarea class="single-textarea" name="comment" placeholder="{{trans('msg.izox')}}:" onfocus="this.placeholder = ''"
                                                  onblur="this.placeholder = '{{trans('msg.izox')}}:'" required>
                                         </textarea>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <br><br>

            <div class="input-group-icon mt-10  col-md-12">
                <label style="float: left; position: relative" for="select_item">{{trans('msg.aloq')}}</label>
                <div class="input-group">
                    <div class="col-md-6">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-users" aria-hidden="true"></i></div>
                            <input type="text" name="face" placeholder="{{trans('msg.shax')}}:"
                                   required class="single-input">

                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <div class="col-md-6">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                            <input type="text" name="tel" placeholder="Телефон:
"
                                   required class="single-input">

                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <div class="col-md-6">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                            <input type="text" name="email" placeholder="E-mail:"
                                   required class="single-input">

                        </div>
                    </div>
                </div>

            </div>

            <br>
            <div class="input-group-icon mt-10  col-md-12">
                <input class="genric-btn danger" type="submit" value="{{trans('msg.yub')}}">

            </div>

            </form>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->

<script type="text/javascript">
    $('.minus-btn').on('click', function(e) {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest('div').find('input');
        var value = parseInt($input.val());

        if (value > 1) {
            value = value - 1;
        } else {
            value = 0;
        }

        $input.val(value);

    });

    $('.plus-btn').on('click', function(e) {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest('div').find('input');
        var value = parseInt($input.val());

        if (value < 100) {
            value = value + 1;
        } else {
            value =100;
        }

        $input.val(value);
    });

    $('.like-btn').on('click', function() {
        $(this).toggleClass('is-active');
    });
</script>
