<br><br><br><br><br>

<div class="container box_1170">
    <div class="section-top-border">
        <h3 class="mb-30">{{trans('msg.barcha')}}</h3>
        <div class="row">
            <div class="col-lg-12">
                <blockquote class="generic-blockquote">
                    <div class="row">
                        <div class="col-lg-3">
                            <i style="color: green;">{{trans('msg.yiq')}}</i><br>
                            <i>15:52, 02.07.2019</i><br><br>
                            <p>{{trans('msg.qabul')}}</p>
                        </div>
                        <div class="col-lg-9">
                            <a href="">
                                <p>№1234 - <b>Нужно вывезти строительный мусор и траву с участка 3 самосвала 20м3 плюс нужен трактор с отбойником, чтобы отбить ленточный фундамент 70м2. содрать
                                        травянной покров с участка 10 соток, разломать сарай. 1 человек грузчик нужен. - 60.0</b></p>
                            </a>
                        </div>
                    </div>
                </blockquote>
            </div>

            <div class="col-lg-12">
                <blockquote class="generic-blockquote">
                    <div class="row">
                        <div class="col-lg-3">
                            <i style="color: green;">Вывоз мусора</i><br>
                            <i>15:52, 02.07.2019</i><br><br>
                            <p>Принимаются ставки</p>
                        </div>
                        <div class="col-lg-9">
                            <a href="">
                                <p>№1588 - <b>ТБО ул Бирюлевская, 56, стр2, 0,8 куб м, 1 раз в неделю. - 1.0</b></p>
                            </a>
                        </div>
                    </div>
                </blockquote>
            </div>


            <div class="col-lg-12">
                <blockquote class="generic-blockquote">
                    <div class="row">
                        <div class="col-lg-3">
                            <i style="color: green;">Вывоз мусора</i><br>
                            <i>15:52, 02.07.2019</i><br><br>
                            <p>Принимаются ставки</p>
                        </div>
                        <div class="col-lg-9">
                            <a href="">
                                <p>№1345 - <b>Строительный мусор(доски,линолиум,бумага,смеси,металл) - 3.0</b></p>
                            </a>
                        </div>
                    </div>
                </blockquote>
            </div>
        </div>
    </div>
</div>
