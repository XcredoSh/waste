<!-- breadcrumb start-->
<section class="cta_part section_padding padding_top" id="">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-xl-8">
                <div class="cta_text text-center">
                    <h2>{{trans('msg.contact')}}</h2>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<!-- ================ contact section start ================= -->
<section class="contact-section section_padding">
    <div class="container">


        <div class="row">
            <div class="col-12">
                <h2 class="contact-title">{{trans('msg.alo')}}</h2>
            </div>
            <div class="col-lg-8">
                <form class="form-contact contact_form" action="{{route('contact_store')}}" method="post" id=""
                     >
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">

                  <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9"
                            onfocus="this.placeholder = ''" onblur="this.placeholder = '{{trans('msg.xab')}}'"
                            placeholder='{{trans('msg.xab')}}'></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" name="name" id="name" type="text" onfocus="this.placeholder = ''"
                                       onblur="this.placeholder = '{{trans('msg.kir')}}'" placeholder='{{trans('msg.kir')}}'>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" name="email" id="email" type="email" onfocus="this.placeholder = ''"
                                       onblur="this.placeholder = '{{trans('msg.email')}}'" placeholder='{{trans('msg.email')}}'>
                            </div>
                        </div>

                    </div>
                    <div class="form-group mt-3">
                        <button type="submit" class="button button-contactForm ">{{trans('msg.sms')}} <i class="flaticon-right-arrow"></i> </button>
                    </div>
                </form>
            </div>
            <div class="col-lg-4">
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-home"></i></span>
                    <div class="media-body">
                        <h3>{{trans('msg.address')}}</h3>

                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                    <div class="media-body">
                        <h3>207-11-03</h3>
                        <p>Факс: (99871) 236-33-31
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-email"></i></span>
                    <div class="media-body">
                        <h3>info@uznature.uz</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->
