
<!-- ================ contact section start ================= -->
<section class="contact-section section_padding">
    <div class="container">


        <div class="row">


            <section class="use_sasu ">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="section_tittle text-center">
                                <h3>{{trans('msg.titles')}}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-3 col-sm-6">
                            <div class="single_feature">
                                <div class="single_feature_part">
                                    <img src="{{ asset('assets') }}/img/icon/tbo.jpg" alt="">
                                    <h4>{{trans('msg.tpo')}}</h4>
                                    <p>{{trans('msg.one')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="single_feature">
                                <div class="single_feature_part">
                                    <img src="{{ asset('assets') }}/img/icon/msp.png" alt="">
                                    <h4>{{trans('msg.mcp')}}</h4>
                                    <p>{{trans('msg.two')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="single_feature">
                                <div class="single_feature_part">
                                    <img src="{{ asset('assets') }}/img/icon/images01.png" alt="">
                                    <h4>{{trans('msg.pre')}} </h4>
                                    <p>{{trans('msg.three')}}</p>
                                </div>
                            </div>
                        </div>
                       <div class="col-lg-3 col-sm-6">
                            <div class="single_feature">
                                <a data-toggle="modal"  data-target="#myModal">
                                    <div class="single_feature_part">
                                        <img src="{{ asset('assets') }}/img/truck.png" alt="">
                                        <h4>{{trans('msg.spes')}}</h4>
                                        {{trans('msg.foo')}}

                                    </div>
                                </a>

                            </div>

                        </div>
                    </div>
                </div>

            </section>

        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->
