
<section class="contact-section section_padding ">
    <div class="container">


        <div class="row">
            <form action="{{route('equipment_save')}}" method="post">
                {{@csrf_field()}}

            <section class="use_sasu ">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="section_tittle ">
                                <h3>{{trans('msg.uskuna')}}</h3>
                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-9 " >

                                    <p style="float: left">{{trans('msg.type')}}</p>
                                    <table class="table" >
                                        <tr>
                                            <td  class="active block_items" style="border: none;">
                                                <label for="kino1"><span>{{trans('msg.sotish')}}</span></label>
                                                <input type="radio"  class="radioBtnClass" name="app_type"  value="1">

                                            </td>
                                            <td  class="active block_items" style="border: none;">
                                                <label for="kino1"><span>{{trans('msg.olish')}}</span></label>
                                                <input type="radio" class="radioBtnClass" name="app_type"  value="2">

                                            </td>
                                            <td  class="active block_items" style="border: none;">
                                                <label for="kino1"><span>{{trans('msg.iun')}}</span></label>
                                                <input type="radio" class="radioBtnClass" name="app_type"  value="3">

                                            </td>
                                            <td  class="active block_items" style="border: none;">
                                                <label for="kino1"><span>{{trans('msg.iolish')}}</span></label>
                                                <input type="radio" class="radioBtnClass" name="app_type"  value="4">

                                            </td>

                                        </tr>

                                    </table>
                                    {{--<script>
                                        $('.radioBtnClass').click(function(){
                                            if($("input[type='radio'].radioBtnClass").is(':checked')) {
                                                var card_type = $("input[type='radio'].radioBtnClass:checked").val();
                                                alert(card_type);
                                            }
                                        });

                                    </script>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <section class="use_sasu col-md-9">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="section_tittle ">

                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-12 " >
                                    <p style="float: left;">{{trans('msg.ariza_turi')}}</p><br>

                                    <div class="input-group-icon mt-10  col-md-12">

                                        <div class="form-select" id="default-select_1">
                                            <label style="float: right; position: relative" for="select_item">{{trans('msg.utype')}}</label>
                                            <select name="type_equipment" id="type_equipment" aria-hidden="" >
                                                <option  value="0" data-name="Не указано">Не указано</option>

                                                <optgroup label="Конвейеры">
                                                    <option  data-name="Ленточные">Конвейеры</option>
                                                    <option  value="Ленточные" data-name="Ленточные"> - Ленточные</option>
                                                    <option  value="Цепные" data-name="Цепные"> - Цепные</option>
                                                    <option  value="Пластинчатые" data-name=""> - Пластинчатые</option>
                                                </optgroup>

                                                <optgroup label="Прессовое оборудование">
                                                    <option data-name="Ленточные">Горизонтальные пресса</option>
                                                    <option  value="Вертикальные пресса" data-name="Цепные"> - Вертикальные пресса</option>
                                                    <option  value="Пресс-компакторы" data-name="Пластинчатые"> - Пресс-компакторы</option>
                                                    <option  value="Пресс-компакторы" data-name="Пластинчатые"> - Пресс-компакторы</option>
                                                    <option  value="Специальные прессы" data-name="Пластинчатые"> - Специальные прессы</option>
                                                    <option  value="Пресс-ножницы для металла" data-name="Пластинчатые"> - Пресс-ножницы для металла</option>
                                                </optgroup>


                                            </select>
                                        </div>
                                    </div><br><br>
                                    <div class="input-group-icon mt-10  col-md-12">

                                        <div class="form-select append-select-eq" id="default-select_1">
                                            <label style="float: right; position: relative" for="select_item">{{trans('msg.model')}}</label>
                                            <select name="monufacture_model" id="type_equipment" aria-hidden=""  >
                                                <option  value="0" data-name="Не указано">Не указано</option>

                                                <optgroup label="Конвейеры">
                                                    <option  value="Экомтех: КС1М 2250" data-name="Ленточные">Экомтех: КС1М 2250</option>
                                                    <option  value="Экомтех: ЛС1Ь 1050" data-name="Ленточные"> - Экомтех: ЛС1Ь 1050</option>

                                                </optgroup>

                                                <optgroup label="Прессовое оборудование">
                                                    <option data-name="Ленточные">Горизонтальные пресса</option>
                                                    <option  value="Вертикальные пресса" data-name="Цепные"> - Вертикальные пресса</option>
                                                    <option  value="Пресс-компакторы" data-name="Пластинчатые"> - Пресс-компакторы</option>
                                                    <option  value="6" data-name="Пластинчатые"> - Пресс-компакторы</option>
                                                    <option  value="7" data-name="Пластинчатые"> - Специальные прессы</option>
                                                    <option  value="8" data-name="Пластинчатые"> - Пресс-ножницы для металла</option>
                                                </optgroup>


                                            </select>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        function myFunctionEq(){
                                            $.get("{{ route('ajaxRequestEq') }}", function (data) {
                                                var eq = document.getElementById('type_equipment').value;

                                                var element = '<select id="value-select" name="monufacture_model" class="form-control">';
                                                element += '<option selected disabled>...</option>';

                                                for(var i=0; i<data.length; i++) {
                                                    console.log(data);

                                                    if (data[i].equipment_id == eq)
                                                    {
                                                        element += '<option value="'+ data[i].manufacture_name_ru + '">' + data[i].manufacture_name_ru + '</option>';
                                                    }
                                                }
                                                element += '</select>';
                                                $('.append-select-eq').append(element);
                                            })

                                        }
                                    </script>
                                    <br><br>

                                    <div class="mt-10 col-md-12">
                                        <label style="float: right; position: relative" for="select_item">{{trans('msg.tav')}}</label>
							             <textarea class="single-textarea" name="message" placeholder="Описание" onfocus="this.placeholder = ''"
                                              onblur="this.placeholder = '{{trans('msg.tav')}}'" required>
                                         </textarea>
                                    </div>
                                    <br><br>
                                    <div class="input-group-icon mt-10  col-md-12">
                                        <label style="float: right; position: relative" for="select_item">{{trans('msg.narx')}}</label>
                                        <div class="input-group">
                                            <div class="col-md-6">
                                                <div class="input-group-icon mt-10">
                                                    <div class="icon"><i class="" aria-hidden="true"></i></div>
                                                    <input type="text" name="price" placeholder="СЎМ"
                                                           required class="single-input">

                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                    <br><br>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </section>

            <section class="use_sasu col-md-9 ">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="section_tittle ">

                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-12 " >

                                    <p style="float: left">{{trans('msg.joy')}}</p><br>
                                    <div id="map2" style="width: 745px; height: 400px"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <section class="use_sasu col-md-9 ">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 col-md-12">
                            <div class="section_tittle ">

                            </div>
                        </div>


                        <div class=" col-md-12 ">
                            <div class="single_feature">
                                <div class="single_feature_part  col-md-12 " >

                                    <p style="float: left">{{trans('msg.qosh')}}</p>

                                    <div class="mt-10 col-md-12">
                                        <label style="float: right; position: relative" for="select_item">{{trans('msg.izox')}}:</label>
                                        <textarea class="single-textarea" name="comment" placeholder="{{trans('msg.izox')}}:" onfocus="this.placeholder = ''"
                                                  onblur="this.placeholder = '{{trans('msg.izox')}}:'" required>
                                         </textarea>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <br><br>

            <div class="input-group-icon mt-10  col-md-12">
                <label style="float: left; position: relative" for="select_item">{{trans('msg.aloq')}}</label>
                <div class="input-group">
                    <div class="col-md-6">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-users" aria-hidden="true"></i></div>
                            <input type="text" name="face" placeholder="{{trans('msg.shax')}}:"
                                   required class="single-input">

                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <div class="col-md-6">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                            <input type="text" name="tel" placeholder="Телефон:
"
                                   required class="single-input">

                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <div class="col-md-6">
                        <div class="input-group-icon mt-10">
                            <div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                            <input type="text" name="email" placeholder="E-mail:"
                                   required class="single-input">

                        </div>
                    </div>
                </div>

            </div>

            <br>
            <div class="input-group-icon mt-10  col-md-12">
                <input class="genric-btn danger" type="submit" value="{{trans('msg.yub')}}">

            </div>

            </form>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->
{{--<script>
    function myFunctionEq(){

        $.get("{{ URL::to('ajaxRequestEq') }}", function (data) {
            var eq = document.getElementById('type_equipment').value;
            console.log(eq);
            var element = '<select id="value-select" name="monufacture_model" class="form-control">';
            element += '<option selected disabled>...</option>';

            for(var i=0; i<data.length; i++) {

                if (data[i].equipment_id == eq)
                {
                    element += '<option value="'+ data[i].manufacture_name_ru + '">' + data[i].manufacture_name_ru + '</option>';
                }
            }

            element += '</select>';
            $('.append-select-eq').append(element);
        })


    }
</script>--}}
