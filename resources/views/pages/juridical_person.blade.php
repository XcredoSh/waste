<div class="whole-wrap" style="padding-top: 100px">
    <div class="container box_1170">
        <div class="section-top-border">
            <h3 class="mb-30"></h3>
            <div class="row">
                <div class="col-md-3 ">
                    {{--  <div class="text-center">
                          <img class="" src="{{ asset('assets') }}/img/man.png" width="60%" alt="" class="img-fluid">
                      </div>--}}
                    <br><br>
                </div>
                <div class="col-md-9 mt-sm-20">
                    <div class="progress-table-wrap">
                        <div class="section-top-border" style="padding-top: 10px;">
                            <div class="row">
                                {{--@foreach($dataUser as $data_user)
                                    @if($data_user->email == $email)
                                        @foreach($dataAnswer as $dataAns)
                                            <div class="col-md-12 section-top-border text-right" style="border: 2px solid; padding-top: 15px;margin-bottom: 15px;">
                                                <h3 class="mb-30 text-left">Ответ</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="text-right">{{$dataAns->description}}</p>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <img src="{{ asset('assets') }}{{$dataAns->media}}" alt="" class="img-fluid">
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                @endforeach--}}

                                <div class="col-lg-8 col-md-8">


                                    <h3 class="mb-30">Юридическое лицо
                                    </h3>
                                    <form action="{{route('juridical_post')}}" method="post">
                                        {{csrf_field()}}

                                        <div class="mt-10">

                                            <textarea class="single-textarea" name="name_organization" placeholder="Полное наименование" onfocus="this.placeholder = ''"
                                                      onblur="this.placeholder = 'Полное наименование'" required></textarea>
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="inn" placeholder="ИНН"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'ИНН'" required
                                                   class="single-input">
                                        </div>

                                        <div class="mt-10">
                                            <input type="email" name="email" placeholder="Почтовый адрес "
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Почтовый адрес '" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="telefon" placeholder="Контактный телефон организации "
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Контактный телефон организации'" required
                                                   class="single-input">
                                        </div>
                                        <div class="input-group-icon mt-10">
                                            <div class="icon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
                                            <textarea class="single-textarea" name="j_address" placeholder="Адрес регистрации" onfocus="this.placeholder = ''"
                                                      onblur="this.placeholder = 'Адрес регистрации'" required></textarea>
                                        </div><br><br>


                                        <div class="mt-10">
                                            <input type="text" name="last_name" placeholder="Фамилия"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Фамилия'" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="first_name" placeholder="Имя"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Имя'" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <input type="text" name="middle_name" placeholder="Отчество"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Отчество'" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <input type="phone " name="p_tel" placeholder="Телефон"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Телефон'" required
                                                   class="single-input">
                                        </div>

                                        <div class="mt-10">
                                            <input type="email" name="p_email" placeholder="Ваш адрес электронной почты"
                                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ваш адрес электронной почты'" required
                                                   class="single-input">
                                        </div>
                                        <div class="mt-10">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="password" name="password" placeholder="Пароль"
                                                           onfocus="this.placeholder = ''" onblur="this.placeholder = 'Пароль'" required
                                                           class="single-input">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="password" name="c_password" placeholder="Повтарите"
                                                           onfocus="this.placeholder = ''" onblur="this.placeholder = 'Повтарите'" required
                                                           class="single-input">
                                                </div>
                                            </div>
                                        </div>




                                   <br><br>



                                        <div class="button-group-area">
                                            <input class="genric-btn danger" type="submit" value="Отправить">

                                        </div>

                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<script>
    function myFunction(){

        $.get("{{ URL::to('ajaxRequest') }}", function (data) {
            var reg = document.getElementById('region_id').value;
            var element = '<select id="value-select" name="area" class="form-control">';
            element += '<option selected disabled>...</option>';

            for(var i=0; i<data.length; i++) {

                if (data[i].region_status == reg)
                {
                    element += '<option value="'+ data[i].area_name_ru + '">' + data[i].area_name_ru + '</option>';
                }
            }

            element += '</select>';
            $('.append-select').append(element);
        })


    }
</script>
