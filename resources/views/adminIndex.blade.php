@extends('layouts.admin.base')
@section('header')
    @include('layouts.admin.header')
@endsection
@section('content')
    @include('admin.' .$page)
@endsection
@section('footer')
    @include('layouts.admin.footer')
@endsection

