
<div class="message-wrapper">
    <ul class="messages ul_marg">


      @foreach($messages as $message)

            <li class="message clearfix li_none">
                <div class="{{ ($message->from == \Illuminate\Support\Facades\Auth::id()) ? 'sent' : 'recevied' }}">
                    <p>{{$message->message}}</p>
                    <p class="date">{{date('d M y, h:i a', strtotime($message->created_at))}}</p>
                </div>
            </li>
        @endforeach

    </ul>
</div>
<div class="input-text input-group">

    <input type="text" name="message" id="chat_input"  class="submit form-control" style="width: 680px;">
    <div class="input-group-btn" id="send_msg" style="position: absolute; right: -50px; bottom: 0px;">
        <button class="btn btn-default"  style="height: 38px; width: 50px; ">
            <i class="fa fa-paper-plane " style="font-size:28px;color:red"></i>
        </button>
    </div>
</div>



