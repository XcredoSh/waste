<?php
/**
 * Created by PhpStorm.
 * User: Shakhzod
 * Date: 9/22/2019
 * Time: 3:32 AM
 */

namespace App;


class mapWaste
{
    protected $table = 'mapWaste';
    protected $fillable = [
        'marker_name ', 'hintContent', 'balloonContent', 'lan', 'lng',
    ];
}
