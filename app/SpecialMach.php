<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialMach extends Model
{
    protected $table = 'special_machinery';
    protected $fillable = [
       'app_type', 'ts_group', 'modification', 'message', 'year_of_issue', 'mileage', 'vin', 'ptc', 'cleared', 'condition', 'number_of_owners', 'price', 'face', 'tel', 'email',
    ];
}
