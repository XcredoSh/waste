<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{

    protected $table = 'applications_answers';
    protected $fillable = [
        'applications_id', 'user_answer_id', 'description', 'media', 'created_at',
    ];
}
