<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Juridical extends Authenticatable
{
    use Notifiable;


    protected $table = 'juridical_person';
    protected $fillable = [
        'name_organization ', 'telefon', 'email', 'inn','last_name','first_name','middle_name','p_tel','p_email', '	j_address',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
