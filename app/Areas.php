<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    protected $table = 'areas';
    protected $fillable = [
        'area_id', 'region_status', 'area_name_uz', 'area_name_ru',
    ];
}
