<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applications extends Model
{
    protected $table = 'applications';
    protected $fillable = [
        'id','first_name', 'last_name', 'phone','email', 'passport_number', 'region_id', 'area', 'address', 'media', 'date', 'message', 'app_status_id',
    ];
}
