<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Writer extends Authenticatable
{
    use Notifiable;

    protected $guard = 'juridical';

    protected $fillable = [
        'last_name', 'email', 'password', 'inn',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
