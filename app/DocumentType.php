<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    protected $table = 'documents_types';
    protected $fillable = [
        'id', 'name_uz', 'name_ru', 'ordering',
    ];
}
