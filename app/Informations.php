<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informations extends Model
{
    protected $table = 'informations';
    protected $fillable = [
        'id','category_id', 'link', 'pdf', 'photo', 'text_photo',
    ];
    public $timestamps = false;
}
