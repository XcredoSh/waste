<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $table = 'category_info';
    protected $fillable = [
       'name',
    ];
}
