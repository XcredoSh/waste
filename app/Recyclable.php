<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recyclable extends Model
{
    protected $table = 'recyclable_materials';
    protected $fillable = [
        'app_type', 'category', 'new_category', 'message', 'total_amount', 'unit_of_measuroment', 'price', 'map', 'comment', 'face', 'tel', 'email',
    ];
}
