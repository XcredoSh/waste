<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer_and_model extends Model
{
    protected $table = 'manufacturer_and_model';
    protected $fillable = [
        'equipment_id', 'manufacture_name_ru ', 'manufacture_name_uz',
    ];
}
