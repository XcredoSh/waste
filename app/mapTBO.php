<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mapTBO extends Model
{
    protected $table = 'mapWaste';
    protected $fillable = [
        'marker_name ', 'hintContent', 'balloonContent', 'lan', 'lng',
    ];
}
