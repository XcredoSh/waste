<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regions extends Model
{
    protected $table = 'regions';
    protected $fillable = [
        'areas_id', 'region_name_uz', 'region_name_ru',
    ];
}
