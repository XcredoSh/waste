<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = [
        'art_name_uz ', 'art_name_uz', 'art_image', 'created',
    ];
    public $timestamps = false;
}
