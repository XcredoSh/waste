<?php

namespace App\Http\Controllers;

use App\mapMSP;
use App\mapTBO;
use App\mapWaste;
use Illuminate\Support\Facades\DB;

class MapsController extends Controller
{
    public function map_tbo()
    {
        $mapdates = mapTBO::all();

        //$mapdates =  DB::table('maps')->select('marker_name ','iconContent', 'hintContent', 'balloonContent', 'lan', 'lng')->get();
        return view('map_tbo', compact('mapdates'));
    }


    public function map_msp()
    {
        $mapdatesMsp = mapMSP::all();
        return view('map_msp', compact('mapdatesMsp'));
    }


    public function waste()
    {
        $mapdatesWaste = DB::select('select * from mapWaste') ;
        return view('waste', compact('mapdatesWaste'));
    }

    public function points()
    {
        $mapdatesPoints = DB::select('select * from mapPoints') ;
        return view('points', compact('mapdatesPoints'));
    }


}
