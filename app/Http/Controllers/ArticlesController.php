<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Information;
use Illuminate\Support\Facades\DB;

class ArticlesController extends Controller
{
    public function index()
    {
        $articles = DB::select('select * from articles');
        return view('index', [
            'page' =>'articles',
            'articles' => $articles,
        ]);
    }
    public function informationWaste()
    {

        $informations = Information::all();
        return view('index', [
            'page' =>'informations',
            'informations' => $informations,
        ]);
    }
}
