<?php

namespace App\Http\Controllers\Admin;

use App\Regions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddRegionController extends Controller
{
    public function index()
    {
        return view('adminIndex', [
            'page' => 'addRegion',
        ]);
    }

    public function addRegion(Request $request)
    {
        //dd($request->all());

        if (empty($request)) {
            return redirect()->back();
        } else {
            $region = new Regions;
            $region->areas_id = $request->get('areas_id');
            $region->region_name_ru = $request->get('region_name_ru');
            $region->region_name_uz = $request->get('region_name_uz');
            $region->save();
            if ($region) {
                return redirect()->back();
            }
        }
    }
}
