<?php
/**
 * Created by PhpStorm.
 * User: Shakhzod
 * Date: 9/22/2019
 * Time: 9:41 AM
 */

namespace App\Http\Controllers\Admin;


use App\News;
use Carbon\Carbon;
use App\Information;
use App\Gallery;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class AddNewsController
{
    public function admin_news()
    {
        $news = News::all();
        return view('adminIndex', [
            'page' => 'addNews',
            'news' =>$news,
        ]);
    }


    public function admin_newsDel($id)
    {
        $catss = News::find($id);
        $catss->delete();
        return redirect()->back();
    }








    public function addingNews(Request $request)
    {
       //dd($request->all());

        if (empty($request)) {
            return redirect()->back();
        } else {
            $news = new News;
            $news->news_name_uz = $request->get('news_name_uz');
            $news->news_name_ru = $request->get('news_name_ru');
            $news->created = Carbon::now()->format('Y-m-d H:m:s');
            $news->img = "Picture";
           $news->save();
            $urlId = $news->id;

            $image = $request->file('img');

                    $destinationPath = 'assets/img/news/';
                    $profileImage = $urlId.$image->getClientOriginalName();;
                    $image->move($destinationPath, $profileImage);
            $pictureUri = $destinationPath.$profileImage;
            $productPic = News::find($urlId);
            $productPic->img =  $pictureUri;
            $productPic->save();
            }

            if ($news) {
                return redirect()->back();
            }

    }

    public function information()
    {
        $informations = Information::all();
        return view('adminIndex', [
            'page' => 'information',
            'informations' => $informations,
        ]);
    }

    public function del($id)
    {
        $catss = Information::find($id);
        $catss->delete();
        return redirect()->back();
    }

    public function informationPost(Request $request)
    {
        //dd($request->all());

        if (empty($request)) {
            return redirect()->back();
        } else {
            $info = new Information;
            $info->text_uz = $request->get('text_uz');
            $info->text_ru = $request->get('text_ru');
            $info->created_at = Carbon::now()->format('Y-m-d H:m:s');
            $info->informations = "Info";
            $info->save();
            $urlId = $info->id;

            $image = $request->file('information');

            $destinationPath = 'assets/information/';
            $profileImage = $urlId.$image->getClientOriginalName();;
            $image->move($destinationPath, $profileImage);
            $pictureUri = $destinationPath.$profileImage;
            $productPic = Information::find($urlId);
            $productPic->informations =  $pictureUri;
            $productPic->save();

            return redirect()->back();


        }
    }

    public function photos()
    {
        $photos = Gallery::all();
        return view('adminIndex', [
            'page' => 'photos',
            'photos' => $photos,
        ]);
    }

    public function deiP($id)
    {
        $cats = Gallery::find($id);
        $cats->delete();
        return redirect()->back();
    }
    public function deiV($id)
    {
        $cats = Gallery::find($id);
        $cats->delete();
        return redirect()->back();
    }

    public function photosPost(Request $request)
    {
        if (empty($request)) {
            return redirect()->back();
        } else {
            $info = new Gallery();
            $info->url = "Photo";
            $info->type = '1';
            $info->save();
            $urlId = $info->id;

            $image = $request->file('photos');

            $destinationPath = 'assets/photos/';
            $profileImage = $urlId.$image->getClientOriginalName();;
            $image->move($destinationPath, $profileImage);
            $pictureUri = $destinationPath.$profileImage;
            $productPic = Gallery::find($urlId);
            $productPic->url =  $pictureUri;
            $productPic->save();
            return redirect()->back();


        }
    }



    public function videos()
    {
        $vodeos = Gallery::all();
        return view('adminIndex', [
            'page' => 'videos',
            'vodeos' => $vodeos,
        ]);
    }

    public function videosPost( Request $request)
    {
        if (empty($request)) {
            return redirect()->back();
        } else {
            $info = new Gallery();
            $info->url  = "Video";
            $info->type = '2';
            $info->save();
            $urlId = $info->id;

            $image = $request->file('videos');

            $destinationPath = 'assets/videos/';
            $profileImage = $urlId.$image->getClientOriginalName();;
            $image->move($destinationPath, $profileImage);
            $pictureUri = $destinationPath.$profileImage;
            $productPic = Gallery::find($urlId);
            $productPic->url =  $pictureUri;
            $productPic->save();
            return redirect()->back();


        }
    }
}
