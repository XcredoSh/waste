<?php

namespace App\Http\Controllers\Admin;

use App\Areas;
use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddUserController extends Controller
{
    public function index()
    {
        return view('adminIndex', [
            'page' => 'addUser',
        ]);
    }

    public function addUser(Request $request)
    {
        //dd($request->all());

        if (empty($request)) {
            return redirect()->back();
        } else {
            $admin = new Admin();
            $admin->name = $request->get('name');
            $admin->email = $request->get('email');
            $admin->password = $request->get('password');
                $man =  $request->get('region_inspector_id');
                if($man == 0){
                    $admin->status = 1;
                    $admin->region_inspector_id = 0;
                }else if($man > 0){
                    $admin->region_inspector_id = $request->get('region_inspector_id');
                    $admin->status = 0;
                    $admin->is_admin = 0;
                }

            $admin->role = 'inspector';
            $role= $admin->save();

            if ($role) {
                return redirect()->back();
            }
        }
    }
}
