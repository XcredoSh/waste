<?php
/**
 * Created by PhpStorm.
 * User: Shakhzod
 * Date: 9/22/2019
 * Time: 3:52 AM
 */

namespace App\Http\Controllers\Admin;

use App\mapPoint;
use Illuminate\Http\Request;

class addPointController
{
    public function addPoints()
    {
        return view('adminIndex', [
            'page' => 'addPoint',
        ]);
    }

    public function addingPoints(Request $request)
    {
        //dd($request->all());

            $point = new mapPoint;
            $point->marker_name = $request->get('marker_name');
            $point->hintContent = $request->get('hintContent');
            $point->balloonContent = $request->get('balloonContent');
            $point->lan = $request->get('lan');
            $point->lng = $request->get('lng');
            $point->save();
            if ($point) {
                return redirect()->back();
            }

    }
}
