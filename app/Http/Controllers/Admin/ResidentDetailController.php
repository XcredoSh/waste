<?php

namespace App\Http\Controllers\Admin;


use App\Applications;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ResidentDetailController extends Controller
{
    public function resident_details($region_id)
    {
        $arr= Applications::find($region_id);
        $resident_details_ids[] = $arr;

        //dd($resident_details_ids);
        $applications = DB::table('applications')->select('id','first_name', 'last_name', 'phone', 'passport_number', 'region_id', 'area', 'address', 'media', 'date', 'message')->get();
        $regions_id = DB::table('regions')->get();
        $areas_id = DB::table('areas')->get();
        return view('adminIndex', [
            'page' => 'residtntes_details',
            'applications' => $applications,
            'regions_id' => $regions_id,
            'areas_id' => $areas_id,
            'resident_details_ids'=> $resident_details_ids,
    ]);
    }


    public function update(Request $request, $id)
    {
        //dd($request->app_status_id);

        $update = Applications::findOrFail($id);
        $www = $request->all();
        $update->update($www);

        return redirect()->back();

    }
}
