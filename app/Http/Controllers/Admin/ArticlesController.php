<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{
    public function index()
    {
        $articels = Article::all();
        return view('adminIndex', [
            'page' => 'newsAdminAdd',
            'articels' =>$articels,
        ]);
    }

    public function indexDel($id)
    {
        $catss = Article::find($id);
        $catss->delete();
        return redirect()->back();
    }

    public function indexpost(Request $request)
    {
       // dd($request->all());
        if (empty($request)) {
            return redirect()->back();
        } else {
            $news = new Article();
            $news->art_name_uz = $request->get('art_name_uz');
            $news->art_name_ru = $request->get('art_name_ru');
            $news->created = Carbon::now()->format('Y-m-d H:m:s');
            $news->art_image = "Picture";
            $news->save();
            $urlId = $news->id;

            $image = $request->file('art_image');

            $destinationPath = 'assets/img/articles/';
            $profileImage = $urlId.$image->getClientOriginalName();;
            $image->move($destinationPath, $profileImage);
            $pictureUri = $destinationPath.$profileImage;
            $productPic = Article::find($urlId);
            $productPic->art_image =  $pictureUri;
            $productPic->save();


        }

        if ($news) {
            return redirect()->back();
        }
    }
}
