<?php
/**
 * Created by PhpStorm.
 * User: Shakhzod
 * Date: 9/22/2019
 * Time: 2:17 AM
 */

namespace App\Http\Controllers\Admin;

use App\mapTBO;
use Illuminate\Http\Request;

class AddTboController
{
    public function addTbo()
    {
        return view('adminIndex', [
            'page' => 'addTbo',
        ]);
    }

    public function addingTbo(Request $request)
    {
        //dd($request->all());

        if (empty($request)) {
            return redirect()->back();
        } else {
            $aera = new mapTBO;
            $aera->marker_name = $request->get('marker_name');
            $aera->hintContent = $request->get('hintContent');
            $aera->balloonContent = $request->get('balloonContent');
            $aera->lan = $request->get('lan');
            $aera->lng = $request->get('lng');
            $aera->save();
            if ($aera) {
                return redirect()->back();
            }
        }
    }
}
