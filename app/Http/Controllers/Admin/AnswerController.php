<?php

namespace App\Http\Controllers\Admin;


use App\Answers;
use App\Applications;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AnswerController extends Controller
{
   public function sendAnswer(Request $request)
   {
       // dd($request->all());
       if (empty($request)) {
           return redirect()->back();
       }else{
           $user = new Answers();
       }
       $user->applications_id = $request->get('userID');
       $user->user_anwser_id = $request->get('answerID');
       $user->description = $request->get('toUserMessage');
       $user->media = "Picture";
       $user->save();
       $lastId = $user->id;
       $image = $request->file('toUserFile');
       $destinationPath = 'assets/apps/';
       $profileImage = $lastId.$image->getClientOriginalName();;
       $image->move($destinationPath, $profileImage);
       $pictureUri = $destinationPath.$profileImage;
       $productPic = Answers::find($lastId);
       $productPic->media =  $pictureUri;
       $productPic->save();
       return redirect()->back();
   }
}
