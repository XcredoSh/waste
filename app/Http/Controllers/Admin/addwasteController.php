<?php
/**
 * Created by PhpStorm.
 * User: Shakhzod
 * Date: 9/22/2019
 * Time: 3:28 AM
 */

namespace App\Http\Controllers\Admin;

use App\mapWaste;
use Illuminate\Http\Request;

class addWasteController
{
    public function addWaste()
    {
        return view('adminIndex', [
            'page' => 'addWaste',
        ]);
    }

    public function addingWaste(Request $request)
    {
        //dd($request->all());

        if (empty($request)) {
            return redirect()->back();
        } else {
            $aera = new mapWaste;
            $aera->marker_name = $request->get('marker_name');
            $aera->hintContent = $request->get('hintContent');
            $aera->balloonContent = $request->get('balloonContent');
            $aera->lan = $request->get('lan');
            $aera->lng = $request->get('lng');
            $aera->save();
            if ($aera) {
                return redirect()->back();
            }
        }
    }
}
