<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Support\Facades\Session;
use Nexmo\Client\Exception\Validation;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

session_start();

class AdminController extends Controller
{
    use AuthenticatesUsers;


    protected $redirectTo = '/alisa';

    public function __construct()
    {

        $this->middleware('guest:admin')->except('logout');

    }
    protected function guard()
    {
        return Auth::guard('admin');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*protected function guard(){
        return Auth::guard('admin');
    }*/

/* public function __construct()
    {
        $this->middleware('auth:admin');
    }*/

    public function login(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        $inspectors = DB::table('admins')->select('id', 'name', 'email', 'password', 'region_inspector_id', 'role' ,'status')->get()->toArray();
        /*$inspe = DB::table('admins')->select('id', 'name', 'email', 'region_inspector_id', 'role' ,'status')->get();
        $data = DB::select('select email from admins');
        dd($data[2]->email);
        foreach ($data as $key=>$ins){

            if ($ins->email == "jizzax@mail.ru"){
                dd("this is jizzax@mail.ru");
            }else{
                dd("this is not jizzax@mail.ru");
            }
        }*/
 /*       foreach ($inspectors as $item) {
            if ( ($email == $item->email) and ($password == $item->password) )
            {
                $result = DB::table('admins')
                    ->where('email' , $email)
                    ->where('password', $password)
                    ->first();
               if ($result)
               {
                   Session::put('name', $result->name);
                   Session::put('id', $result->id);
                   Session::put('newser', 'this is newser');
                   return view('adminIndex', [
                       'page' => 'newsAdminAdd',
                   ]);
               }else{
                   Session::put('message', 'Email or password Invalid');
                   return redirect()->back();
               }
            }
        }*/

           foreach($inspectors as $inspector)
           {
               if ($email == $inspector->email){
                   if($inspector->status == 0){
                       if ($inspector->region_inspector_id == 1){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();
                               $app_inspector_show_1 = DB::select('SELECT region_id FROM applications WHERE region_id = 1');
                               Session::put('toshkent', 'this is Tashkent');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_1' => $app_inspector_show_1,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }elseif ($inspector->region_inspector_id == 2){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_2 = DB::select('SELECT region_id FROM applications WHERE region_id = 2');

                               Session::put('toshkentVil', 'this is Tashkent vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_2' => $app_inspector_show_2,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 3){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_3 = DB::select('SELECT region_id FROM applications WHERE region_id = 3');

                               Session::put('andijonVil', 'this is Andijon vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_3' => $app_inspector_show_3,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 4){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_4 = DB::select('SELECT region_id FROM applications WHERE region_id = 4');

                               Session::put('namanganVil', 'this is Namengan vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_4' => $app_inspector_show_4,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 5){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_5 = DB::select('SELECT region_id FROM applications WHERE region_id = 5');

                               Session::put('qoqonVil', 'this is Namengan vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_5' => $app_inspector_show_5,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 6){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_6 = DB::select('SELECT region_id FROM applications WHERE region_id = 6');

                               Session::put('jizzaxVil', 'this is Jizzax vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_6' => $app_inspector_show_6,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 7){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_7 = DB::select('SELECT region_id FROM applications WHERE region_id = 7');

                               Session::put('buxoroVil', 'this is Buxoro vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_7' => $app_inspector_show_7,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 8){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_8 = DB::select('SELECT region_id FROM applications WHERE region_id = 8');

                               Session::put('samarqandVil', 'this is Samarqand vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_8' => $app_inspector_show_8,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 9){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_9 = DB::select('SELECT region_id FROM applications WHERE region_id = 9');

                               Session::put('navoiyVil', 'this is Navoiy vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_9' => $app_inspector_show_9,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 10){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_10 = DB::select('SELECT region_id FROM applications WHERE region_id = 10');

                               Session::put('sirdaryoVil', 'this is Sirdaryo vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_10' => $app_inspector_show_10,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 11){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_11 = DB::select('SELECT region_id FROM applications WHERE region_id = 11');

                               Session::put('qashqadaryoVil', 'this is Qash vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_11' => $app_inspector_show_11,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 12){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_12 = DB::select('SELECT region_id FROM applications WHERE region_id = 12');

                               Session::put('surhondaryoVil', 'this is Sirdqryo vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_12' => $app_inspector_show_12,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 13){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('name', $result->name);
                               Session::put('id', $result->id);

                               $users =  $documents = DB::table('users')->select( 'id')->get();
                               $applicate = DB::table('applications')->select( 'id')->get();
                               $docx = DB::table('documents')->select( 'id')->get();

                               $app_inspector_show_13 = DB::select('SELECT region_id FROM applications WHERE region_id = 13');

                               Session::put('qoraqolpoqistonVil', 'this is Qoraqolpoq vil');
                               return view('adminIndex', [
                                   'page' => 'content',
                                   'users' => $users,
                                   'applicate' => $applicate,
                                   'docx' => $docx,
                                   'inspectors' => $inspectors,
                                   'app_inspector_show_13' => $app_inspector_show_13,
                               ]);
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }else if ($inspector->region_inspector_id == 14){
                           $result = DB::table('admins')
                               ->where('email' , $email)
                               ->where('password', $password)
                               ->first();
                           if ($result){
                               Session::put('newser', 'this is newser');
                               return redirect('/dashboard');
                           }else{
                               Session::put('message', 'Email or password Invalid');
                               return redirect()->back();
                           }
                       }

                   }else if(($inspector->status == 1)){
                       $result = DB::table('admins')
                           ->where('email' , $email)
                           ->where('password', $password)
                           ->first();
                       if ($result){
                           Session::put('name', $result->name);
                           Session::put('id', $result->id);
                           Session::put('admin', 'this is Admin');
                           return redirect('/dashboard');
                       }else{
                           Session::put('message', 'Email or password Invalid');
                           return redirect()->back();
                       }
                   }

               }
           }



           /* $result = DB::table('admins')
                ->where('email' , $email)
                ->where('password', $password)
                ->first();
            if ($result){
                Session::put('name', $result->name);
                Session::put('id', $result->id);
                return redirect('/dashboard');
            }else{
                Session::put('message', 'Email or password Invalid');
                return redirect()->back();
            }*/

/*
        $app_inspector_show_1 = DB::select('SELECT region_id FROM applications WHERE region_id = 1');
        $app_inspector_show_2 = DB::select('SELECT region_id FROM applications WHERE region_id = 2');
        $app_inspector_show_3 = DB::select('SELECT region_id FROM applications WHERE region_id = 3');
        $app_inspector_show_4 = DB::select('SELECT region_id FROM applications WHERE region_id = 4');
        $app_inspector_show_5 = DB::select('SELECT region_id FROM applications WHERE region_id = 5');
        $app_inspector_show_6 = DB::select('SELECT region_id FROM applications WHERE region_id = 6');
        $app_inspector_show_7 = DB::select('SELECT region_id FROM applications WHERE region_id = 7');
        $app_inspector_show_8 = DB::select('SELECT region_id FROM applications WHERE region_id = 8');
        $app_inspector_show_9 = DB::select('SELECT region_id FROM applications WHERE region_id = 9');
        $app_inspector_show_10 = DB::select('SELECT region_id FROM applications WHERE region_id = 10');
        $app_inspector_show_11 = DB::select('SELECT region_id FROM applications WHERE region_id = 11');
        $app_inspector_show_12 = DB::select('SELECT region_id FROM applications WHERE region_id = 12');
        $app_inspector_show_13 = DB::select('SELECT region_id FROM applications WHERE region_id = 13');
        'page' => 'content',
                                'users' => $users,
                                'applicate' => $applicate,
                                'docx' => $docx,
                                'inspectors' => $inspectors,
                                'app_inspector_show_1' => $app_inspector_show_1,
                                'app_inspector_show_2' => $app_inspector_show_2,
                                'app_inspector_show_3' => $app_inspector_show_3,
                                'app_inspector_show_4' => $app_inspector_show_4,
                                'app_inspector_show_5' => $app_inspector_show_5,
                                'app_inspector_show_6' => $app_inspector_show_6,
                                'app_inspector_show_7' => $app_inspector_show_7,
                                'app_inspector_show_8' => $app_inspector_show_8,
                                'app_inspector_show_9' => $app_inspector_show_9,
                                'app_inspector_show_10' => $app_inspector_show_10,
                                'app_inspector_show_11' => $app_inspector_show_11,
                                'app_inspector_show_12' => $app_inspector_show_12,
                                'app_inspector_show_13' => $app_inspector_show_13,*/

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $users =  $documents = DB::table('users')->select( 'id')->get();
        $applicate = DB::table('applications')->select( 'id')->get();
        $docx = DB::table('documents')->select( 'id')->get();
        $inspectors = DB::table('admins')->select('id', 'name', 'region_inspector_id', 'role' ,'status')->get();
        $app_inspector_show_1 = DB::select('SELECT region_id FROM applications WHERE region_id = 1');
        $app_inspector_show_2 = DB::select('SELECT region_id FROM applications WHERE region_id = 2');
        $app_inspector_show_3 = DB::select('SELECT region_id FROM applications WHERE region_id = 3');
        $app_inspector_show_4 = DB::select('SELECT region_id FROM applications WHERE region_id = 4');
        $app_inspector_show_5 = DB::select('SELECT region_id FROM applications WHERE region_id = 5');
        $app_inspector_show_6 = DB::select('SELECT region_id FROM applications WHERE region_id = 6');
        $app_inspector_show_7 = DB::select('SELECT region_id FROM applications WHERE region_id = 7');
        $app_inspector_show_8 = DB::select('SELECT region_id FROM applications WHERE region_id = 8');
        $app_inspector_show_9 = DB::select('SELECT region_id FROM applications WHERE region_id = 9');
        $app_inspector_show_10 = DB::select('SELECT region_id FROM applications WHERE region_id = 10');
        $app_inspector_show_11 = DB::select('SELECT region_id FROM applications WHERE region_id = 11');
        $app_inspector_show_12 = DB::select('SELECT region_id FROM applications WHERE region_id = 12');
        $app_inspector_show_13 = DB::select('SELECT region_id FROM applications WHERE region_id = 13');
        $app_inspector_show_14 = DB::select('SELECT region_id FROM applications WHERE region_id = 14');
        return view('adminIndex', [
            'page' => 'content',
            'users' => $users,
            'applicate' => $applicate,
            'docx' => $docx,
            'inspectors' => $inspectors,
            'app_inspector_show_1' => $app_inspector_show_1,
            'app_inspector_show_2' => $app_inspector_show_2,
            'app_inspector_show_3' => $app_inspector_show_3,
            'app_inspector_show_4' => $app_inspector_show_4,
            'app_inspector_show_5' => $app_inspector_show_5,
            'app_inspector_show_6' => $app_inspector_show_6,
            'app_inspector_show_7' => $app_inspector_show_7,
            'app_inspector_show_8' => $app_inspector_show_8,
            'app_inspector_show_9' => $app_inspector_show_9,
            'app_inspector_show_10' => $app_inspector_show_10,
            'app_inspector_show_11' => $app_inspector_show_11,
            'app_inspector_show_12' => $app_inspector_show_12,
            'app_inspector_show_13' => $app_inspector_show_13,
            'app_inspector_show_14' => $app_inspector_show_14
        ]);
        /*foreach($inspectors as $inspector){
            if ($inspector->region_inspector_id == 1){
                return view('adminIndex', [
                    'page' => 'content',
                    'users' => $users,
                    'applicate' => $applicate,
                    'docx' => $docx,
                    'inspectors' => $inspectors,
                ]);
            } else if ($inspector->region_inspector_id !== 1){
                Session::put('toshkent_vil', 'this is Tashkent vil');
                return view('adminIndex', [
                    'page' => 'content',
                    'users' => $users,
                    'applicate' => $applicate,
                    'docx' => $docx,
                    'inspectors' => $inspectors,
                ]);
            }
        }*/




    }

    public  function create()
    {
        return view('adminLogin');
    }

    public  function logout(Request $request, Admin $status)
    {
        $status = Admin::find($status);
        Session::put('name', null);
        Session::put('id', null);
       /* Session::put('status', DB::table('admins')
            ->updateOrInsert(
                ['status' => '0']
            ));*/
       /* $update = DB::table('admins')
                     ->updateOrInsert(
                         ['status' => '0']
            );*/
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/admin');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hot_lines()
    {
        $applications = DB::table('applications')->select('id','first_name', 'last_name', 'phone', 'passport_number', 'region_id', 'area', 'address', 'app_status_id', 'media', 'date', 'message')->get();
        $inspectors = DB::table('admins')->select('id', 'name', 'region_inspector_id', 'role' ,'status')->get();
        $regions_id = DB::table('regions')->get();
        $areas_id = DB::table('areas')->get();
        return view('adminIndex', [
            'page' => 'hot_lines',
            'applications' => $applications,
            'regions_id' => $regions_id,
            'areas_id' => $areas_id,
            'inspectors' => $inspectors,
        ]);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
