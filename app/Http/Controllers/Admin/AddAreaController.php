<?php

namespace App\Http\Controllers\Admin;

use App\Areas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddAreaController extends Controller
{
    public function index()
    {
        return view('adminIndex', [
            'page' => 'addArea',
        ]);
    }

    public function addArea(Request $request)
    {
        //dd($request->all());

        if (empty($request)) {
            return redirect()->back();
        } else {
            $aera = new Areas;
            $aera->region_status = $request->get('numberArea');
            $aera->area_name_uz = $request->get('area_name_uz');
            $aera->area_name_ru = $request->get('area_name_ru');
            $aera->save();
            if ($aera) {
                return redirect()->back();
            }
        }
    }
}
