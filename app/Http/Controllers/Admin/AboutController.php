<?php

namespace App\Http\Controllers\Admin;


use App\Info;
use App\Informations;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function info()
    {
        $infos = Info::all();
        return view('adminIndex', [
            'page' => 'info',
            'infos' => $infos,
        ]);
    }
    public function informa(Request $request)
    {
        //dd($request->all());

        if (empty($request->file('photo')) == null)
        {
            $inf = new Informations();
            $inf->category_id = $request->get('category_id');
            $inf->about_us_id = $request->get('about_us_id');
            $inf->pdf = $request->get('pdf');
            $inf->link = $request->get('link');
            $inf->link_name_uz = $request->get('link_name_uz');
            $inf->link_name_ru = $request->get('link_name_ru');
            $inf->pdf_name_uz = $request->get('pdf_name_uz');
            $inf->pdf_name_ru = $request->get('pdf_name_ru');
            $inf->photo = "photo";
            $inf->text = $request->get('text');
            $inf->text_ru = $request->get('text_ru');
            $inf->text_photo = $request->get('text_photo');
            $inf->text_photo_ru = $request->get('text_photo_ru');
            $inf->save();
            $lastId = $inf->id;

            $image = $request->file('photo');
            $destinationPath = 'assets/info/';
            $profileImage = $lastId.$image->getClientOriginalName();;
            $image->move($destinationPath, $profileImage);
            $pictureUri = $destinationPath.$profileImage;
            $productPic = Informations::find($lastId);
            $productPic->photo =  $pictureUri;
            $productPic->save();
            return redirect()->back();
        }else{

            if (empty($request->file('photo')) == null )
            {
                $inf = new Informations();
                $inf->category_id = $request->get('category_id');
                $inf->about_us_id = $request->get('about_us_id');
                $inf->pdf = $request->get('pdf');
                $inf->link = $request->get('link');
                $inf->link_name_uz = $request->get('link_name_uz');
                $inf->link_name_ru = $request->get('link_name_ru');
                $inf->pdf_name_uz = $request->get('pdf_name_uz');
                $inf->pdf_name_ru = $request->get('pdf_name_ru');
                $inf->photo = "photo";
                $inf->text = $request->get('text');
                $inf->text_ru = $request->get('text_ru');
                $inf->text_photo = $request->get('text_photo');
                $inf->text_photo_ru = $request->get('text_photo_ru');
                $inf->save();

                $lastId = $inf->id;
                $image = $request->file('pdf');
                $destinationPath = 'assets/pdf/';
                $profileImage = $lastId.$image->getClientOriginalName();;
                $image->move($destinationPath, $profileImage);
                $pictureUri = $destinationPath.$profileImage;
                $productPic = Informations::find($lastId);
                $productPic->pdf =  $pictureUri;
                $productPic->save();
                return redirect()->back();
            }
            }

            $inf = new Informations();
            $inf->category_id = $request->get('category_id');
            $inf->about_us_id = $request->get('about_us_id');
            $inf->pdf = $request->get('pdf');
            $inf->link = $request->get('link');
            $inf->link_name_uz = $request->get('link_name_uz');
            $inf->link_name_ru = $request->get('link_name_ru');
              $inf->pdf_name_uz = $request->get('pdf_name_uz');
            $inf->pdf_name_ru = $request->get('pdf_name_ru');
            $inf->photo = "photo";
            $inf->text = $request->get('text');
        $inf->text_ru = $request->get('text_ru');
            $inf->text_photo = $request->get('text_photo');
        $inf->text_photo_ru = $request->get('text_photo_ru');
            $inf->save();

            return redirect()->back();
        }




}
