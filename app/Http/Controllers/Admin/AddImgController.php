<?php

namespace App\Http\Controllers\Admin;


use App\Img;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AddImgController extends Controller
{
    public function upload()
    {
        $imgs = DB::table('addimg')->select('id', 'img_title', 'in_img')->get();
        return view('adminIndex', [
            'page' => 'addImages',
            'imgs' => $imgs,
        ]);
    }

    public function adding_img(Request $request)
    {
        //dd($request->all());
        if (empty($request)) {
            return redirect()->back();
        } else {
            $img = new Img;

            $img->img_title = $request->get('img_title');
            $img->in_img = "Pictur";

            $img->save();

            $path = 'public/assets/img/menu';

            if ($img) {
                return redirect()->back();
            }
        }
    }

    public function destroy($id)
    {
        DB::table('addimg')->where('id', $id)->delete();
       /* $img = Img::find($id);
        $img->delete();*/
        return redirect('/dashboard/img');
    }
}
