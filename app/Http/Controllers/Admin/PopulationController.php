<?php

namespace App\Http\Controllers\Admin;

use App\Regions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PopulationController extends Controller
{
    public function index()
    {
        $applications = DB::table('applications')->select('id','first_name', 'last_name', 'phone', 'passport_number', 'region_id', 'area', 'address', 'app_status_id', 'media', 'date', 'message')->get();
        $inspectors = DB::table('admins')->select('id', 'name', 'region_inspector_id', 'role' ,'status')->get();
        $regions_id = DB::table('regions')->get();
        $areas_id = DB::table('areas')->get();
        return view('adminIndex', [
            'page' => 'people',
            'applications' => $applications,
            'regions_id' => $regions_id,
            'areas_id' => $areas_id,
            'inspectors' => $inspectors,
        ]);
    }

    public function show_by_population(Request $request)
    {
        //dd($request->region_id);
        if ($request){
            return redirect('/doshboard/resident_details/'.$request->get('region_id'));
        }
        /*if ($status) {
            return redirect('/user-platforms/'.$request->get('user_id'))->with('new_user_plarform', 'true');
        }*/

        /*$applications = DB::table('applications')->select('id','first_name', 'last_name', 'phone', 'passport_number', 'region_id', 'area', 'address', 'media', 'date', 'message')->get();
        $inspectors = DB::table('admins')->select('id', 'name', 'region_inspector_id', 'role' ,'status')->get();
        $regions_id = DB::table('regions')->get();
        $areas_id = DB::table('areas')->get();


        return view('adminIndex', [
            'page' => 'people',
            'applications' => $applications,
            'regions_id' => $regions_id,
            'areas_id' => $areas_id,
            'inspectors' => $inspectors,
        ]);*/

    }



}
