<?php

namespace App\Http\Controllers\Admin;

use App\DocumentType;
use App\Document;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use function Sodium\compare;

class DocumentController extends Controller
{

    public function index()
    {
        $document_types = DB::table('documents_types')->select( 'id', 'name_uz', 'name_ru', 'ordering')->get();

        return view('adminIndex', [
            'page' => 'document',
            'document_types' => $document_types,
        ]);
    }


    public function document_save(Request $request)
    {
       // dd($request->all());
        $doc = new Document();
        $doc->name = $request->get('name');
        $doc->number = $request->get('number');
        $doc->date = $request->get('date');
        $doc->file = "File";
        $doc->document_types_id = $request->get('document_types_id');

        $doc->is_locale = $request->get('is_locale');
        if ($request->get('is_locale') == 'on'){
            $doc->is_locale = 1;
                $doc->url = $request->get('url');
        }else{
            $doc->is_locale = 0;
        }
        if ($request->get('url') == null){
            $doc->url = 0;
        }else if($request->get('file') == null){
            $doc->file = 0;

        }
        $doc->status = 1;
        $doc->save();

        $lastId = $doc->id;
        $pictureInfo = $request->file('file');
        if ($request->get('url') !== null)
        {
            $doc->url = $request->get('url');
        }else{
            $picturName = $lastId.$pictureInfo->getClientOriginalName();
            $fulder = "assets/images/documents/";
            $pictureInfo->move($fulder, $picturName);

            $pictureUri = $fulder.$picturName;
            $productPic = Document::find($lastId);
            $productPic->file = $pictureUri;
            $productPic->save();
        }


        if ($doc) {
            return redirect()->back();
        }
    }





}
