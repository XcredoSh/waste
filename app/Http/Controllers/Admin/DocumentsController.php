<?php

namespace App\Http\Controllers\Admin;

use Alert;
use App\Applications;
use App\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;


class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documentShows = DB::table('documents')->select('id', 'document_types_id', 'name', 'number', 'date', 'file', 'url', 'is_locale', 'status')->get();
        foreach ($documentShows as $documentShow){
           if ($documentShow->document_types_id == 1){
               return view('adminIndex', [
                   'page' => 'codes',
                   'documentShows' => $documentShows,
               ]);
           }
        }
    }

public function laws()
{
    $documentLaws = DB::table('documents')->select('id', 'document_types_id', 'name', 'number', 'date', 'file', 'url', 'is_locale', 'status')->get();
    foreach ($documentLaws as $documentLaw){
        if ($documentLaw->document_types_id == 2){
            return view('adminIndex', [
                'page' => 'laws',
                'documentLaws' => $documentLaws,
            ]);
        }
    }
}


public function decisions()
{
        $documentВecisions = DB::table('documents')->select('id', 'document_types_id', 'name', 'number', 'date', 'file', 'url', 'is_locale', 'status')->get();
        foreach ($documentВecisions as $documentВecision){
            if ($documentВecision->document_types_id == 3){
                return view('adminIndex', [
                    'page' => 'decisions',
                    'documentВecisions' => $documentВecisions,
                ]);
            }
        }
}


public function another()
{
        $documentanothers = DB::table('documents')->select('id', 'document_types_id', 'name', 'number', 'date', 'file', 'url', 'is_locale', 'status')->get();
        foreach ($documentanothers as $documentanother){
            if ($documentanother->document_types_id == 4){
                return view('adminIndex', [
                    'page' => 'another',
                    'documentanothers' => $documentanothers,
                ]);
            }
        }
}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCodes($id)
    {
        $cats = Document::find($id);
        $cats->delete();
        return redirect()->back();
    }

    public function destroyLaws($id)
    {
        $cats = Document::find($id);
        $cats->delete();
        return redirect()->back();
    }

    public function destroyDecisions($id)
    {
        $cats = Document::find($id);
        $cats->delete();
        return redirect()->back();
    }

    public function destroyAnothers($id)
    {
        $cats = Document::find($id);
        $cats->delete();
        return redirect()->back();
    }
}
