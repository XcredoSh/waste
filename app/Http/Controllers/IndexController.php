<?php

namespace App\Http\Controllers;


use App\News;
use App\Equipments;
use App\Gallery;
use App\SpecialMach;
use App\Recyclable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apps = DB::select('select id from applications');
        $users = DB::select('select id from users');
        $img_menu = DB::select('select * from addimg');
        $newsы = DB::select('select * from news  ORDER BY id DESC LIMIT 3');
        $juridical_persons = DB::select('select * from juridical_person');

        $equipments = Equipments::select([
            'id',
            'shop_name',
            DB::raw('(select app_name_ru from applications_type where id=equipments.app_type) as as_app_type'),
            'type_equipment',
            'message',
            'comment',
            'monufacture_model'
        ])->get();

        $special_machineries= SpecialMach::select([
            'id',
            'spec_shop_name',
            DB::raw('(select app_name_ru from applications_type where id= special_machinery.app_type) as spec_app_type'),
            'ts_group',
            'message'
        ])->get();

        $recyclable_materials = Recyclable::select([
            'id',
            'rec_shop_name',
            DB::raw('(select app_name_ru from applications_type where id=  recyclable_materials.app_type) as rec_app_type'),
            'category',
            'message',
            'comment'
        ])->get();


        $gallerys = Gallery::all();




        return view('index', [
            'page' =>'main',
            'apps' => $apps,
            'users' => $users,
            'juridical_persons' => $juridical_persons,
            'img_menu' => $img_menu,
            'newsы' => $newsы,
            'equipments' => $equipments,
            'special_machineries' => $special_machineries,
            'recyclable_materials' => $recyclable_materials,
            'gallerys' => $gallerys,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminLogin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
