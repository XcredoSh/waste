<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Notifications\Notifiable;
//use App\Http\Controllers\Auth\Authenticatable;
//use Illuminate\Auth\Authenticatable;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use Notifiable;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest:admin')->except('logout');

    }
    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function showAdminLoginForm()
    {
        return view('adminLogin',  ['url' => 'admin']);
    }

    public function adminLogin(Request $request)
    {

   /*     if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('dashboard');
        }*/

        $email = $request->input('email');
        $password = $request->input('password');

        $result = DB::table('admins')
            ->where('email' , $email)
            ->where('password', $password)
            ->first();
        if ($request){
            return redirect()->route('dashboard');
        }

        return redirect()->back();

    }
    public function showLoginForm()
    {
      //  return view('auth\login');
    }

}
