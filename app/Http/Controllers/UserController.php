<?php

namespace App\Http\Controllers;

use App\Applications;
use Illuminate\Http\Request;
use App\User;
use App\Answers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Description;

session_start();

class UserController extends Controller
{
  /* public function  __construct()
    {
        $this->middleware('auth');
    }*/



    public function register(Request $request)
    {
      //  dd($request->all());
        if (empty($request)) {
            return redirect()->back();
        } else {
            $user = new User();
            $user->name = $request->get('name');
            $user->lastname = $request->get('lastname');
            $user->email = $request->get('email');
            $user->setRememberToken(Str::random(60));
            $user->password = $request->get('password');
            if ($request->get('password') == $request->get('c_password')) {
                $user->password = $request->get('password');
            }
            $user->status = 1;
            $user->is_admin = 1;
            $status = $user->save();
            if ($status) {
                return redirect()->back();
            }
        }

    }



    public function login(Request $request)
    {


     $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);


       $email = $request->input('email');
       $password = $request->input('password');

        $result = DB::table('users')
            ->where('email' , $email)
            ->where('password', $password)
            ->first();

        if ($result) {
            $user_data = DB::table('users')->select('name', 'lastname')->get();
            $regions = DB::table('regions')->select('areas_id', 'region_name_uz', 'region_name_ru')->get();
            $areas = DB::table('areas')->select('area_id', 'region_status', 'area_name_uz', 'area_name_ru')->get();
            $dataUser = Applications::all();
            $dataAnswer = Answers::all();
            if ($user_data) {
                return view('index', [
                    'page' => 'cabinet',
                    'user_data' => $user_data,
                    'regions' => $regions,
                    'areas' => $areas,
                    'dataUser' => $dataUser,
                    'email' => $email,
                    'dataAnswer' => $dataAnswer,
                ]);
            }else{
                return "no cobinet";
            }

        }else{
            Session::put('message', 'Email or password Invalid');
            return "This is not Auth";
        }

    }
}
