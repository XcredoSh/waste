<?php

namespace App\Http\Controllers;


use App\User;
use App\SpecialMach;
use App\Equipments;
use App\Recyclable;
use App\Message;
use App\Applications;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Nexmo\Response;
use Pusher\Pusher;
class ChatController extends Controller
{
    public function chat()
    {
        $users = User::where('id', '!=', Auth::id())->get();
        $specials = SpecialMach::where('id', '!=', Auth::id())->get();
        $equipments = Equipments::where('id', '!=', Auth::id())->get();
        $recyclables= Recyclable::where('id', '!=', Auth::id())->get();

        /*$users = DB::select('select users.id, users.name, users.email, count(is_read) as unread
          from users LEFT JOIN messages ON users.id = messages.from and is_read = 0 and messages.to = ". Auth::id() ."
          where users.id != ". Auth::id() ."
            
          group by users.id,  users.name, users.email
         ');*/
        //$equipments = DB::select('select * from equipments');
        $equipments = Equipments::select([
            'id',
            'shop_name',
            'email',
            DB::raw('(select app_name_ru from applications_type where id=equipments.app_type) as as_app_type'),
            'type_equipment',
            'message',
            'comment',
            'monufacture_model'
        ])->get();
        $special_machineries= SpecialMach::select([
            'id',
            'spec_shop_name',
            DB::raw('(select app_name_ru from applications_type where id= special_machinery.app_type) as spec_app_type'),
            'ts_group',
            'message'
        ])->get();

        return view('Chats.chat', [
            'users' => $users,
            'specials' => $specials,
            'equipments' => $equipments,
            'recyclables' => $recyclables,
            'special_machineries' => $special_machineries,
        ]);
    }

   /* public function chat_id($id)
    {
        return  $id;
    }*/
   public function getChat($id)
   {
       return "kkk";
   }

    public function getMessage($user_id)
    {
       //return $user_id;
        $my_id = Auth::id();
        Message::where(['from' => $user_id, 'to' => $my_id])->update(['is_read' => 1]);
        $messages = Message::where(
            function ($query) use ($user_id, $my_id)
            {
                $query->where('from', $my_id)->where('to', $user_id);
            })
            ->orWhere(function ($query) use ($user_id, $my_id)
        {
            $query->where('from', $user_id)->where('to', $my_id);
        })->get();

    return view('messages.index', ['messages' => $messages]);

    }

    public function sentMessage(Request $request)
    {
//       dd($request->all());
//        return json_encode($request->get('message'));
        $from = Auth::id();
        $to = $request->get('receiver_id');
        $message = $request->get('message');
        $data = new Message();
        $data->from = $from;
        $data->to = $to;
        $data->message = $message;
        $data->is_read = 0;
        $data->save();

        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );
        $data = ['from' => $from, 'to' => $to];
        $pusher->trigger('my-channel', 'my-event', $data);

    }

    public function editAnnouncement($id)
    {
        return "Hello";
    }

}
