<?php

namespace App\Http\Controllers;


use App\Juridical;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class JuridicalPersonController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:juridical');
    }
    public function juridical_per()
    {
        return view('index', [
            'page' =>'juridical_person',
            'url' => 'juridical'
        ]);
    }


    public function juridical_post(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        //dd($request->all());
        if (empty($request)) {
            return redirect()->back();
        } else {
            $ju = new Juridical;
            $ju->name_organization = $request->get('name_organization');
            $ju->inn = $request->get('inn');
            $ju->last_name = $request->get('last_name');
            $ju->first_name = $request->get('first_name');
            $ju->middle_name = $request->get('middle_name');
            $ju->p_tel = $request->get('p_tel');
            $ju->p_email = $request->get('p_email');
            $ju->telefon = $request->get('telefon');
            $ju->j_address = $request->get('j_address');
            $ju->email = $request->get('email');

            if ($request->get('password') == $request->get('c_password')) {
                $ju->password = Hash::make($request->get('password'));
            }
            $ju->setRememberToken(Str::random(60));
            $ju->save();
            if ($ju) {
                return redirect('/')->with('urlJur', 'juridical');
            }
        }
    }
}
