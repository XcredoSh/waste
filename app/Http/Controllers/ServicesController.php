<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Document;
use App\Equipments;
use App\News;
use App\Recyclable;
use App\SpecialMach;
use App\Manufacturer_and_model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class ServicesController extends Controller
{
    public function index()
    {
        $documents = DB::table('documents')->select( 'id', 'name', 'document_types_id', 'number', 'date', 'file', 'url', 'is_locale', 'status')->get();

        return view('index', [
            'page' => 'services',
            'documents' => $documents,
        ]);
    }

    public function equipment()
    {
        return view('index', [
            'page' => 'equipment',
        ]);
    }

    public function ajaxRequestEq()
    {
        $dataGetEq = DB::table('manufacturer_and_model')->select('equipment_id', 'manufacture_name_ru ', 'manufacture_name_uz')->get();

        return Response::json($dataGetEq);
    }

    public function equipment_save(Request $request)
    {
        //dd($request->all());
        if (empty($request)) {
            return redirect()->back();
        } else {
            $eq = new Equipments;
            $eq->id = Auth::id();
            $eq->app_type = $request->get('app_type');
            $eq->shop_name = "Оборудование — Оформление заявки";
            $eq->type_equipment = $request->get('type_equipment');
            $eq->monufacture_model = $request->get('monufacture_model');
            $eq->message = $request->get('message');
            $eq->price = $request->get('price');
            $eq->comment = $request->get('comment');
            $eq->face = $request->get('face');
            $eq->tel = $request->get('tel');
            $eq->email = $request->get('email');

            $eq->save();
            if ($eq) {
                return redirect()->back();
            }
        }
    }

    public function recyclable_save(Request $request)
    {
        //dd($request->all());
        if (empty($request)) {
            return redirect()->back();
        } else {
            $re = new Recyclable;
            $re->rec_shop_name = "Вторсырье — Оформление заявки";
            $re->app_type = $request->get('app_type');
            $re->category = $request->get('category');
            $re->message = $request->get('message');
            $re->total_amount = $request->get('total_amount');
            $re->unit_of_measuroment = $request->get('unit_of_measuroment');
            $re->price = $request->get('price');
            $re->comment = $request->get('comment');
            $re->face = $request->get('face');
            $re->tel = $request->get('tel');
            $re->email = $request->get('email');
            $re->save();
            if ($re) {
                return redirect()->back();
            }
        }
    }



    public function special_machinery()
    {
        return view('index', [
            'page' => 'special_machinery',
        ]);
    }



    public function special_save(Request $request)
    {
        //dd($request->all());
        if (empty($request)) {
            return redirect()->back();
        } else {
            $sp = new SpecialMach;
            $sp->id = Auth::id();
            $sp->spec_shop_name = "Спецтехника — Оформление заявки";
            $sp->app_type = $request->get('app_type');
            $sp->ts_group = $request->get('ts_group');
            $sp->modification = $request->get('modification');
            $sp->message = $request->get('message');
            $sp->year_of_issue = $request->get('year_of_issue');
            $sp->mileage = $request->get('mileage');
            $sp->vin = $request->get('vin');
            $sp->ptc = $request->get('ptc');
            $sp->cleared = $request->get('cleared');
            $sp->condition = $request->get('condition');
            $sp->number_of_owners = $request->get('number_of_owners');
            $sp->price = $request->get('price');
            $sp->face = $request->get('face');
            $sp->tel = $request->get('tel');
            $sp->email = $request->get('email');
            $sp->save();
            if ($sp) {
                return redirect()->back();
            }
        }
    }






    public function recyclable_materials()
    {
        return view('index', [
            'page' => 'recyclable_materials',
        ]);
    }

    public function news()
    {
        $news = News::all();
        return view('index', [
            'page' => 'news',
            'news' => $news,
        ]);
    }

    public function applications()
    {
        return view('index', [
            'page' => 'applications',
        ]);
    }
}
