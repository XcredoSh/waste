<?php

namespace App\Http\Controllers;


use App\User;
use App\Answers;
use App\Regions;
use App\Areas;
use App\Applications;
use function GuzzleHttp\Psr7\get_message_body_summary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;


class CabinetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function show_cabinet()
    {
        $user_data = DB::table('users')->select('name', 'lastname')->get();
        $regions = DB::table('regions')->select('areas_id', 'region_name_uz', 'region_name_ru')->get();
        $areas = DB::table('areas')->select('area_id', 'region_status', 'area_name_uz', 'area_name_ru')->get();
        // $campaigns = DB::select('select campaign_id from user_platforms where user_id=? and platform_types_id=1', [$userId]);
        //$dataUser = Applications::all()->toArray();
        $dataUser = DB::select('SELECT * FROM  applications ');
        $dataAnswer = Answers::all();
        $users = User::all();
        if ($user_data) {
            return view('index', [
                'page' => 'cabinet',
                'users' => $users,
                'user_data' => $user_data,
                'dataUser' => $dataUser,
                'regions' => $regions,
                'areas' => $areas,
                'dataAnswer' =>$dataAnswer,
            ]);
        }else{
            return "no cobinet";
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cabinetLogin()
    {
        return view('loginCabinet');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cabinetPost(Request $request)
    {
        //dd($request->all());
        $email = $request->input('email');
        $password = $request->input('password');
        $apps = Applications::select([
            'id',
            'first_name',
            'last_name',
            'email',
            'phone',
            'passport_number',
            'region_id',
            'area',
            'address',
            'media',
            'date',
            'media',
            'message',
            'app_status_id',
            'created_at',
        ])->where('email', '=', $email)->get();

        $user_data = DB::table('users')->select('name', 'lastname')->get();
        $regions = DB::table('regions')->select('areas_id', 'region_name_uz', 'region_name_ru')->get();
        $areas = DB::table('areas')->select('area_id', 'region_status', 'area_name_uz', 'area_name_ru')->get();
        $dataUser = DB::select('SELECT * FROM  applications ');
        $dataAnswer = Answers::all();


        $result = DB::table('users')
            ->where('email' , $email)
            ->where('password', $password)
            ->first();
        if ($result){
            return view('index', [
                'page' =>'cabinettwo',
                'email' => $email,
                'apps' => $apps,
                'user_data' => $user_data,
                'dataUser' => $dataUser,
                'regions' => $regions,
                'areas' => $areas,
                'dataAnswer' =>$dataAnswer,
            ]);
        }else{
            Session::put('message', 'Email or password Invalid');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function send_application(Request $request)
    {
        //dd($request->all());
        if (empty($request)) {
            return redirect()->back();
        }else{
            $app = new Applications();
        }
        $app->first_name = $request->get('first_name');
        $app->last_name = $request->get('last_name');
        $app->email = $request->get('email');
        $app->phone = $request->get('phone');
        $app->passport_number = $request->get('passport_number');
        $app->region_id = $request->get('region_id');
        $app->area = $request->get('area');
        $app->media = "Picture";
        $app->address = $request->get('address');
        $app->date = $request->get('date');
        $app->message = $request->get('message');
        $app->app_status_id = 1;
        $app->save();

        $lastId = $app->id;


        $image = $request->file('media');

        $destinationPath = 'assets/apps/';
        $profileImage = $lastId.$image->getClientOriginalName();;
        $image->move($destinationPath, $profileImage);
        $pictureUri = $destinationPath.$profileImage;
        $productPic = Applications::find($lastId);
        $productPic->media =  $pictureUri;
        $productPic->save();
        return redirect('/cabinet');
    }



    //Ajax

    public function ajaxRequest()
    {
        $dataGet = DB::table('areas')->select('region_status', 'area_name_ru', 'area_name_uz')->get();

        return Response::json($dataGet);
    }

   /* public function ajaxRequestPost(Request $request)
    {
        $input = $request->all();
        return response()->json(['success'=>'Got Simple Ajax Request.']);
    }*/
}
