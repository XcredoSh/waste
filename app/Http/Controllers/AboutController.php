<?php

namespace App\Http\Controllers;


use App\Informations;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $opens = Informations::all();
        return view('index', [
            'page' =>'about',
            'opens' => $opens,
        ]);
    }
    public function openData()
    {
        $opens = Informations::all();
        return view('index', [
            'page' =>'open',
            'opens' => $opens,
        ]);
    }
}
