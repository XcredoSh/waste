<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JuridicalPersonLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:juridical')->except('logout');
    }


    public function juridical_per()
    {
        return view('index', [
            'page' =>'juridical_person',
            'url' => 'juridical'
        ]);
    }


    public function juridicalLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/admin');
        }
        return back()->withInput($request->only('email', 'remember'));
    }

}
