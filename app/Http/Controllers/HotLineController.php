<?php

namespace App\Http\Controllers;


use App\Applications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HotLineController extends Controller
{
    public function hot_line()
    {
        $regions = DB::table('regions')->select('areas_id', 'region_name_uz', 'region_name_ru')->get();
        return view('index', [
            'page' => 'hot_line',
            'regions' => $regions,
        ]);
    }
    public function send_application_2(Request $request)
    {
        //dd($request->all());
        if (empty($request)) {
            return redirect()->back();
        }else{
            $app = new Applications();
        }
        $app->first_name = $request->get('first_name');
        $app->last_name = $request->get('last_name');
        $app->email = $request->get('email');
        $app->phone = $request->get('phone');
        $app->passport_number = $request->get('passport_number');
        $app->region_id = $request->get('region_id');
        $app->area = $request->get('area');
        $app->media = "Picture";
        $app->address = $request->get('address');
        $app->date = $request->get('date');
        $app->message = $request->get('message');
        $app->app_status_id = 1;
        $app->save();

        $lastId = $app->id;


        $image = $request->file('media');

        $destinationPath = 'assets/apps/';
        $profileImage = $lastId.$image->getClientOriginalName();;
        $image->move($destinationPath, $profileImage);
        $pictureUri = $destinationPath.$profileImage;
        $productPic = Applications::find($lastId);
        $productPic->media =  $pictureUri;
        $productPic->save();

        return redirect('/hot-line')->with('app',$app);
    }
}
