<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipments extends Model
{
    protected $table = 'equipments';
    protected $fillable = [
        'app_type','shop_name', 'price_of_app', 'type_equipment', 'monufacture_model', 'message', 'price', 'map	', 'comment', 'face', 'tel', 'email',
    ];
}
