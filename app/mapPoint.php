<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mapPoint extends Model
{
    protected $table = 'mapPoints';
    protected $fillable = [
        'marker_name ', 'hintContent', 'balloonContent', 'lan', 'lng',
    ];
}
