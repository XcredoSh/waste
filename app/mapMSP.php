<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mapMSP extends Model
{
    protected $table = 'mapsMSP';
    protected $fillable = [
        'marker_name ', 'hintContent', 'balloonContent', 'lan', 'lng',
    ];
}
