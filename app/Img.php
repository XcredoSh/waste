<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Img extends Model
{
    protected $table = 'addimg';
    protected $fillable = [
        'in_img', 'img_title', 'id',
    ];
}
