<?php
/**
 * Created by PhpStorm.
 * User: Shakhzod
 * Date: 9/22/2019
 * Time: 10:07 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable = [
        'news_name_uz ', 'news_name_ru ', 'img', 'created',
    ];
}
