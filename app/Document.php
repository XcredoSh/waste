<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'documents';
    protected $fillable = [
        'id', 'document_types_id', 'name', 'file', 'number', 'date', 'url', 'is_locale',' status',
    ];
}
