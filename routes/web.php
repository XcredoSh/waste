<?php


//Localization
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});


//Auth::routes();


//User
Route::get('/', 'IndexController@index')->name('indexOne');
Route::get('/news', 'ServicesController@news')->name('news');
Route::get('/services', 'ServicesController@index')->name('services');

Route::get('/contact', 'ContactController@index')->name('contact');
Route::post('/contact', 'ContactController@store')->name('contact_store');




Route::get('/equipment', 'ServicesController@equipment')->name('equipment');
Route::post('/equipment', 'ServicesController@equipment_save')->name('equipment_save');

Route::get('/hot-line', 'HotLineController@hot_line')->name('hot_line');
Route::post('/hot-line', 'HotLineController@send_application_2')->name('send_application_2');


Route::get('/applications', 'ServicesController@applications')->name('applications');

//Ajax
Route::get('ajaxRequest', 'CabinetController@ajaxRequest');
Route::post('ajaxRequest', 'CabinetController@ajaxRequestPost');

//Maps
Route::get('/map-tbo', 'MapsController@map_tbo')->name('map_tbo');
Route::get('/map-msp', 'MapsController@map_msp')->name('map_msp');
Route::get('/waste', 'MapsController@waste')->name('waste');
Route::get('/points', 'MapsController@points')->name('points');


Route::get('/juridical-person', 'JuridicalPersonController@juridical_per')->name('juridical_per');
Route::post('/juridical-person', 'JuridicalPersonController@juridical_post')->name('juridical_post');


//Route::group(['middleware' => 'auth'], function () {
    Route::get('/structure', 'StructureController@index')->name('structure');

        Route::get('ajaxRequestEq', 'ServicesController@ajaxRequestEq')->name('ajaxRequestEq');
        Route::post('ajaxRequestEq', 'ServicesController@ajaxRequestPostEqPost');

        Route::get('/special_machinery', 'ServicesController@special_machinery')->name('special_machinery');
        Route::post('/special_machinery', 'ServicesController@special_save')->name('special_save');

        Route::get('/recyclable_materials', 'ServicesController@recyclable_materials')->name('recyclable_materials');
        Route::post('/recyclable_materials', 'ServicesController@recyclable_save')->name('recyclable_save');


    Route::post('/ragister', 'UserController@register')->name('register');
    Route::post('/user-login', 'UserController@login')->name('login');


//});


Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::post('/login/admin', 'Auth\LoginController@adminLogin')->name('admin');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');

Auth::routes();

Route::get('/', 'IndexController@index')->name('indexOne');
Route::get('/cabinet', 'CabinetController@show_cabinet')->name('cabinet');
Route::post('/cabinet/application', 'CabinetController@send_application')->name('send_application');
Route::get('/admin', 'Admin\AdminController@create');
Route::post('/admin', 'Admin\AdminController@login')->name('adminLogins');
Route::group( ['middleware' => 'auth'], function(){
});
Route::get('/dashboard', 'Admin\AdminController@dashboard')->name('dashboard');
Route::post('/login/admin', 'Auth\LoginController@adminLogin');

Route::get('/information', 'Admin\AddNewsController@information')->name('information');
Route::get('/information/deleated/{id}', 'Admin\AddNewsController@del');
Route::get('/informatsion', 'ArticlesController@informationWaste')->name('informatsionWaste');
Route::post('/information', 'Admin\AddNewsController@informationPost')->name('informationPost');



Route::get('/photos', 'Admin\AddNewsController@photos')->name('photos');
Route::get('/photos/deleted/{id}', 'Admin\AddNewsController@deiP');
Route::post('/photos', 'Admin\AddNewsController@photosPost')->name('photos');

Route::get('/videos', 'Admin\AddNewsController@videos')->name('videos');
Route::get('/videos/deleted/{id}', 'Admin\AddNewsController@deiV');
Route::post('/videos', 'Admin\AddNewsController@videosPost')->name('videos');








//Route::group( ['middleware' => 'auth:admin'], function(){


    //Route::get('/logout', 'Admin\AdminController@logout')->name('logout');

//Route::put('/logout/{ststus}', 'Admin\AdminController@logout')->name('logout');
    //Route::post('/admin', 'Admin\AdminController@login')->name('adminLogins');
    //Route::get('/dashboard', 'Admin\AdminController@dashboard')->name('dashboard');
    Route::get('/dashboard/doc' , 'Admin\DocumentController@index')->name('documents');
    Route::get('/dashboard/population' , 'Admin\PopulationController@index')->name('people');
    Route::post('/dashboard/population' , 'Admin\PopulationController@show_by_population')->name('show_by_population');

//Admin/Documents
    Route::post('/document_save' , 'Admin\DocumentController@document_save')->name('document_save');

//Residents datails
    Route::get('/doshboard/resident_details/{region_id}', 'Admin\ResidentDetailController@resident_details')->name('resident_details');

//update
    Route::patch('/doshboard/resident_details/{id}', 'Admin\ResidentDetailController@update')->name('newActionThis');

//answers
    Route::post('/doshboard/resident_details', 'Admin\AnswerController@sendAnswer')->name('answerName');

//Adding area and Region
    Route::get('/dashboard/addRegion', 'Admin\AddRegionController@index')->name('addRegion');
    Route::get('/dashboard/addArea', 'Admin\AddAreaController@index')->name('addArea');

    Route::get('/dashboard/addUser', 'Admin\AddUserController@index')->name('addUser');
    Route::post('/dashboard/addUser', 'Admin\AddUserController@addUser')->name('addUserPost');

    Route::post('/dashboard/addRegion', 'Admin\AddRegionController@addRegion')->name('addRegionPost');
    Route::post('/dashboard/addArea', 'Admin\AddAreaController@addArea')->name('addAreaPost');

//Menu add

    Route::get('/dashboard/img', 'Admin\AddImgController@upload')->name('addImgs');
    Route::post('/dashboard/img', 'Admin\AddImgController@adding_img')->name('addImgsPost');
    Route::get('/dashboard/img/{id}', 'Admin\AddImgController@destroy');



//admin adding
    Route::get('/hot-lines', 'Admin\AdminController@hot_lines')->name('hot_lines');



    Route::get('/addTbo', 'Admin\AddTboController@addTbo')->name('addTbo');
    Route::post('/addTbo', 'Admin\AddTboController@addingTbo')->name('addingTbo');


    Route::get('/addMsp', 'Admin\addMspController@addMsp')->name('addMsp');
    Route::post('/addMsp', 'Admin\addMspController@addingMsp')->name('addingMsp');


    Route::get('/addWaste', 'Admin\addwasteController@addWaste')->name('addWaste');
    Route::post('/addWaste', 'Admin\addwasteController@addingWaste')->name('addingWaste');


    Route::get('/addPoints', 'Admin\addPointController@addPoints')->name('addPoints');
    Route::post('/addPoints', 'Admin\addPointController@addingPoints')->name('addingPoints');


//Documents

    Route::get('/dashboard/codes' , 'Admin\DocumentsController@index')->name('codes');
    Route::get('/dashboard/codes/delete/{id}', 'Admin\DocumentsController@destroyCodes')->name('destroyCodes');


    Route::get('/dashboard/laws' , 'Admin\DocumentsController@laws')->name('laws');
    Route::get('/dashboard/laws/{id}', 'Admin\DocumentsController@destroyLaws')->name('destroyLaws');


    Route::get('/dashboard/decisions' , 'Admin\DocumentsController@decisions')->name('decisions');
    Route::get('/dashboard/decisions/{id}', 'Admin\DocumentsController@destroyDecisions')->name('destroyDecisions');



    Route::get('/dashboard/another' , 'Admin\DocumentsController@another')->name('another');
    Route::get('/dashboard/another/{id}', 'Admin\DocumentsController@destroyAnothers')->name('destroyAnothers');


//});




//Route::get('/cabinet', 'CabinetController@index')->name('usercabinet');




Route::get('/chat' , 'ChatController@chat')->name('chat');
//Route::get('/chat/{id}' , 'ChatController@getChat');
Route::get('/message/{id}' , 'ChatController@getMessage')->name('message');
//Route::get('/message/{chat_id}' , 'ChatController@editAnnouncement')->name('announcement');
Route::post('message' , 'ChatController@sentMessage');


Route::get('/articles' , 'ArticlesController@index')->name('articles');


Route::get('/cabinetLogin' , 'CabinetController@cabinetLogin')->name('cabinetLogin');
Route::post('/cabinetLogin' , 'CabinetController@cabinetPost')->name('cabinetPost');

Route::get('/admin-news', 'Admin\AddNewsController@admin_news')->name('admin_news');
Route::get('/admin-news/delete/{id}', 'Admin\AddNewsController@admin_newsDel');
Route::post('/admin-news', 'Admin\AddNewsController@addingNews')->name('addingNews');


Route::get('/admin-articles', 'Admin\ArticlesController@index')->name('admin_articlea');
Route::get('/admin-articles/delete/{id}', 'Admin\ArticlesController@indexDel');
Route::post('/admin-articles', 'Admin\ArticlesController@indexpost')->name('addingArticles');

Route::get('/gallery' , 'Admin\GalleryController@index')->name('gallery');


Route::get('/about-us' , 'AboutController@index')->name('about');
Route::get('/open-data' , 'AboutController@openData')->name('opendata');


Route::get('/info' , 'Admin\AboutController@info')->name('info');

Route::post('/informations', 'Admin\AboutController@informa')->name('informationPosts');





